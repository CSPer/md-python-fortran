#######################################
#
#  This is the rdfmd python module.
#
#  This module provides the foundation
#  that you may use to build rdfmd 
#  applications.
#
#  (C) Christopher Woods, May 2005
#
#######################################


print "Importing RDFMD module..."

from python_source import *
