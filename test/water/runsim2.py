#!/bin/env python

import sys
import os

#set the path to the 'rdfmd' directory that contains the 
#rdfmd python module
sys.path.append("/home/csp1g13/irp/rdfmd/tags/rdfmd-1_0/")

#import the rdfmd python module
from python_source import *

#write our own digifilter command file...
class MyNamdDFConf(NamdDFConf):
      def __init__(self):
            NamdDFConf.__init__(self)
        
      def commandFileContents(self,namdsim):
            """Return a string containing the contents of the command file. Reimplement
               this function to gain complete control over the digital filtering"""
            
            m = self.nframes()/2 + 1
            d = self.delay()
               
            cmdfile = """
#turn on debugging output
param debug

#no shake
#param shake

#load up the system
read system %s

#load up the filter
read filter %s

#set the index of the central configuration
param centindex %d

#read in the starting coordinates and velocities - ascii output files from
#namd use velocity units of A ps-1
read pdb type=coord file=%s start=%d end=%d
read pdb type=vel file=%s start=%d end=%d

#now read in the coordinate and velocity buffers from the backwards simulation
#note that binary files in namd write velocities in amber velocity units
read dcd type=coord file=%s start=%d end=%d
read dcd type=vel file=%s start=%d end=%d velscl=amber

#now read in the coordinate and velocity buffers from the forwards simulation
#note that binary files in namd write velocities in amber velocity units
read dcd type=coord file=%s start=%d end=%d
read dcd type=vel file=%s start=%d end=%d velscl=amber

#calculate the kinetic energy before filtering
kineticnrg state=old

#apply the filter
filter vscale=%f fscale=%f tempcap=%f

#calculate the kinetic energy after filtering
kineticnrg state=new

#write filter velocity and mass 
filtvel

#write the output coordinate and velocity files
write pdb type=coord file=%s
write pdb type=vel file=%s

#remove the backwards buffer files...
remove %s
remove %s

#strip the forwards coordinate file down so that only the frames between the start
#of the trajectory and the end of the 'delay' region remain
strip dcd in=%s out=%s start=%d end=%d delta=%d

#finally remove the forwards buffer files...
remove %s
remove %s

                     """ % (self.system(), self.filter(), m, \
                            namdsim.inputCoords(), m-d, m-d, \
                            namdsim.inputVels(), m-d, m-d, \
                            namdsim.coordinates().toString() + "_reverse", m-d-1, 1, \
                            namdsim.velocities().toString() + "_reverse", m-d-1, 1, \
                            namdsim.coordinates(), m-d+1, self.nframes(), \
                            namdsim.velocities(), m-d+1, self.nframes(), \
                            self.vscale(), self.fscale(), self.tempcap(), \
                            namdsim.outputCoords(), namdsim.outputVels(), \
                            namdsim.coordinates().toString() + "_reverse", \
                            namdsim.velocities().toString() + "_reverse", \
                            namdsim.coordinates(), namdsim.coordinates().toString() + "_strip", \
                            int(1), int(d), int(namdsim.coordFreq()), \
                            namdsim.coordinates(), namdsim.velocities() )
            
            return cmdfile

### ALL DONE!    

#set the base directory for the simulation and change into it
basedir = File("./")
os.chdir( basedir.toString() )

#create the output directory - start counting at 0
simdir = SimDir("output","equil",0)
simdir.createRoot()

#now link the input files to the current output directory
linkFile("input/equilinput", simdir.current())

#create a digital filtering configuration for my customised namd rdfmd simulations
dfconf = MyNamdDFConf()

#set the digital filtering parameters
dfconf.setFilter("input/filter", 2001)   # load a 2001 coeff filter
dfconf.setSystem("input/water-marked.pdb")   # this file sets the atom types and
                                             # which atoms will be filtered

dfconf.setFiltercap(5)        # set the filter cap
dfconf.setTempcap(1200.0)     # set the temperature cap
dfconf.setDelay(5)            # set the filter delay
dfconf.setFScale(5.0)         # set the filter scale
dfconf.setVScale(1.0)         # set the velocity scale to 0

#now create a digital filtering driver - pass it the location of the 'digifilter'
#program that will actually be performing the filtering
dfdriver = DFDriver("bindir/digifilter")

#now create a simulation configuration for namd - load it up from the template
#file 'alanin.namd'
simconf = NamdSim("input/water.namd")

simconf.setCoordFreq(1)  # print out coordinates every 1 step
simconf.setVelFreq(0)    # never print out velocities

#create a driver to run NAMD simulations - pass it the location of the namd2
#binary that will be used to perform the MD
simdriver = NamdDriver("bindir/namd2")
#simdriver.setNodeFile("input/nodefile",2)

#create hmc configuration for hmc simulation
hmcconf = NamdHMCConf()
#create a hmc driver - pass it the location of the 'hmc' executable
hmcdriver = HMCDriver("bindir/hmc")

#finally, create the rdfmd simulation controller object - this controls the whole simulation
sim = DFSim()

#run some nvt equilibration
simdir = sim.nveSim(simdir, simdriver, simconf, 500)  # 500 steps of NVE

#perform some digital filtering
simdir = sim.rdfmdSim(simdir, simdriver, simconf, dfdriver, dfconf)

#perform hmc simulation
simdir = sim.hmcSim(simdir, simdriver, simconf, hmcdriver, hmcconf, 100)

simdir = sim.rdfmdSim(simdir, simdriver, simconf, dfdriver, dfconf)

simdir = sim.hmcSim(simdir, simdriver, simconf, hmcdriver, hmcconf, 100)
