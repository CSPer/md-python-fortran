#Read in the filter
read filter ../input/filter0_2500

#Now calculate the frequency response, using a time step of 0.1fs
viewfilter 0.1 viewfilter.data
