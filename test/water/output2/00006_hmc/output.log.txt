Charm++: standalone mode (not using charmrun)
Converse/Charm++ Commit ID: v6.4.0-beta1-0-g5776d21
CharmLB> Load balancer assumes all CPUs are same.
Charm++> Running on 1 unique compute nodes (8-way SMP).
Charm++> cpu topology info is gathered in 0.000 seconds.
Info: NAMD 2.9 for Linux-x86_64-multicore
Info: 
Info: Please visit http://www.ks.uiuc.edu/Research/namd/
Info: for updates, documentation, and support information.
Info: 
Info: Please cite Phillips et al., J. Comp. Chem. 26:1781-1802 (2005)
Info: in all publications reporting results obtained with NAMD.
Info: 
Info: Based on Charm++/Converse 60400 for multicore-linux64-iccstatic
Info: Built Mon Apr 30 14:00:48 CDT 2012 by jim on naiad.ks.uiuc.edu
Info: 1 NAMD  2.9  Linux-x86_64-multicore  1    uos-201662  csp1g13
Info: Running on 1 processors, 1 nodes, 1 physical nodes.
Info: CPU topology information available.
Info: Charm++/Converse parallel runtime startup completed at 0.00263309 s
Info: 41.4023 MB of memory in use based on /proc/self/stat
Info: Configuration file is cmdfile_forwards.namd
Info: Working in the current directory /home/csp1g13/irp/rdfmd/tags/rdfmd-1_0/test/water/output/00006_hmc
TCL: Suspending until startup complete.
Warning: The following variables were set in the
Warning: configuration file but will be ignored:
Warning:    langevinDamping (Langevin)
Info: SIMULATION PARAMETERS:
Info: TIMESTEP               1
Info: NUMBER OF STEPS        100
Info: STEPS PER CYCLE        1
Info: LOAD BALANCER  Centralized
Info: LOAD BALANCING STRATEGY  New Load Balancers -- DEFAULT
Info: LDB PERIOD             200 steps
Info: FIRST LDB TIMESTEP     5
Info: LAST LDB TIMESTEP     -1
Info: LDB BACKGROUND SCALING 1
Info: HOM BACKGROUND SCALING 1
Info: MIN ATOMS PER PATCH    40
Info: VELOCITY FILE          input.vel
Info: CENTER OF MASS MOVING INITIALLY? NO
Info: DIELECTRIC             1
Info: EXCLUDE                SCALED ONE-FOUR
Info: 1-4 ELECTROSTATICS SCALED BY 1
Info: MODIFIED 1-4 VDW PARAMETERS WILL BE USED
Info: DCD FILENAME           coordinates.dcd
Info: DCD FREQUENCY          100
Info: DCD FIRST STEP         100
Info: NO EXTENDED SYSTEM TRAJECTORY OUTPUT
Info: VELOCITY DCD FILENAME    velocities
Info: VELOCITY DCD FREQUENCY   100
Info: VELOCITY DCD FIRST STEP  100
Info: NO FORCE DCD OUTPUT
Info: OUTPUT FILENAME        output
Info: NO RESTART FILE
Info: SWITCHING ACTIVE
Info: SWITCHING ON           10
Info: SWITCHING OFF          12
Info: PAIRLIST DISTANCE      15
Info: PAIRLIST SHRINK RATE   0.01
Info: PAIRLIST GROW RATE     0.01
Info: PAIRLIST TRIGGER       0.3
Info: PAIRLISTS PER CYCLE    2
Info: PAIRLISTS ENABLED
Info: MARGIN                 0
Info: HYDROGEN GROUP CUTOFF  2.5
Info: PATCH DIMENSION        17.5
Info: ENERGY OUTPUT STEPS    100
Info: CROSSTERM ENERGY INCLUDED IN DIHEDRAL
Info: TIMING OUTPUT STEPS    100
Info: USING VERLET I (r-RESPA) MTS SCHEME.
Info: C1 SPLITTING OF LONG RANGE ELECTROSTATICS
Info: PLACING ATOMS IN PATCHES BY HYDROGEN GROUPS
Info: RANDOM NUMBER SEED     47389
Info: USE HYDROGEN BONDS?    NO
Info: COORDINATE PDB         input.coor
Info: STRUCTURE FILE         water.psf
Info: PARAMETER file: CHARMM format! 
Info: PARAMETERS             charmm27.prm
Info: USING ARITHMETIC MEAN TO COMBINE L-J SIGMA PARAMETERS
Info: SUMMARY OF PARAMETERS:
Info: 250 BONDS
Info: 622 ANGLES
Info: 1049 DIHEDRAL
Info: 73 IMPROPER
Info: 0 CROSSTERM
Info: 130 VDW
Info: 0 VDW_PAIRS
Info: 0 NBTHOLE_PAIRS
Info: TIME FOR READING PSF FILE: 7.60555e-05
Info: TIME FOR READING PDB FILE: 4.41074e-05
Info: 
Info: ****************************
Info: STRUCTURE SUMMARY:
Info: 3 ATOMS
Info: 2 BONDS
Info: 1 ANGLES
Info: 0 DIHEDRALS
Info: 0 IMPROPERS
Info: 0 CROSSTERMS
Info: 0 EXCLUSIONS
Info: 6 DEGREES OF FREEDOM
Info: 1 HYDROGEN GROUPS
Info: 3 ATOMS IN LARGEST HYDROGEN GROUP
Info: 1 MIGRATION GROUPS
Info: 3 ATOMS IN LARGEST MIGRATION GROUP
Info: TOTAL MASS = 18.0154 amu
Info: TOTAL CHARGE = 0 e
Info: *****************************
Info: 
Info: Entering startup at 0.0298991 s, 146.637 MB of memory in use
Info: Startup phase 0 took 5.19753e-05 s, 146.637 MB of memory in use
Info: Startup phase 1 took 0.000442028 s, 146.637 MB of memory in use
Info: Startup phase 2 took 6.19888e-05 s, 146.637 MB of memory in use
Info: Startup phase 3 took 4.00543e-05 s, 146.637 MB of memory in use
Info: PATCH GRID IS 1 BY 1 BY 1
Info: PATCH GRID IS 1-AWAY BY 1-AWAY BY 1-AWAY
Info: REMOVING COM VELOCITY -5.51476e-05 -0.000341159 0.000351246
Info: LARGEST PATCH (0) HAS 3 ATOMS
Info: Startup phase 4 took 0.000135899 s, 146.637 MB of memory in use
Info: Startup phase 5 took 4.19617e-05 s, 146.637 MB of memory in use
Info: Startup phase 6 took 3.8147e-05 s, 146.637 MB of memory in use
LDB: Central LB being created...
Info: Startup phase 7 took 4.98295e-05 s, 146.637 MB of memory in use
Info: CREATING 11 COMPUTE OBJECTS
Info: NONBONDED TABLE R-SQUARED SPACING: 0.0625
Info: NONBONDED TABLE SIZE: 769 POINTS
Info: ABSOLUTE IMPRECISION IN FAST TABLE ENERGY: 1.69407e-21 AT 11.9974
Info: RELATIVE IMPRECISION IN FAST TABLE ENERGY: 1.13046e-16 AT 11.9974
Info: Startup phase 8 took 0.00135708 s, 147.617 MB of memory in use
Info: Startup phase 9 took 5.29289e-05 s, 147.617 MB of memory in use
Info: Startup phase 10 took 4.91142e-05 s, 147.793 MB of memory in use
Info: Finished startup at 0.0322201 s, 147.793 MB of memory in use

ETITLE:      TS           BOND          ANGLE          DIHED          IMPRP               ELECT            VDW       BOUNDARY           MISC        KINETIC               TOTAL           TEMP      POTENTIAL         TOTAL3        TEMPAVG

ENERGY:       0         0.8913         0.0290         0.0000         0.0000              0.0000         0.0000         0.0000         0.0000         1.7777              2.6980       298.1969         0.9203         2.7271       298.1969

LDB: ============= START OF LOAD BALANCING ============== 0.032444
LDB: ============== END OF LOAD BALANCING =============== 0.0324662
LDB: =============== DONE WITH MIGRATION ================ 0.0325942
LDB: ============= START OF LOAD BALANCING ============== 0.0327051
LDB: ============== END OF LOAD BALANCING =============== 0.032722
LDB: =============== DONE WITH MIGRATION ================ 0.0328801
Info: Initial time: 1 CPUs 3.09467e-05 s/step 0.00035818 days/ns 147.922 MB memory
LDB: ============= START OF LOAD BALANCING ============== 0.032964
LDB: ============== END OF LOAD BALANCING =============== 0.0329752
LDB: =============== DONE WITH MIGRATION ================ 0.0331631
LDB: ============= START OF LOAD BALANCING ============== 0.0332551
LDB: ============== END OF LOAD BALANCING =============== 0.0332701
LDB: =============== DONE WITH MIGRATION ================ 0.033411
Info: Initial time: 1 CPUs 2.16007e-05 s/step 0.000250008 days/ns 147.922 MB memory
LDB: ============= START OF LOAD BALANCING ============== 0.0335681
LDB: ============== END OF LOAD BALANCING =============== 0.0335841
LDB: =============== DONE WITH MIGRATION ================ 0.0337262
Info: Initial time: 1 CPUs 2.05994e-05 s/step 0.000238419 days/ns 147.922 MB memory
LDB: ============= START OF LOAD BALANCING ============== 0.0338871
LDB: ============== END OF LOAD BALANCING =============== 0.0339041
LDB: =============== DONE WITH MIGRATION ================ 0.034106
Info: Benchmark time: 1 CPUs 2.16007e-05 s/step 0.000250008 days/ns 147.922 MB memory
Info: Benchmark time: 1 CPUs 1.84059e-05 s/step 0.000213031 days/ns 147.922 MB memory
Info: Benchmark time: 1 CPUs 1.84059e-05 s/step 0.000213031 days/ns 147.922 MB memory
TIMING: 100  CPU: 0.00374317, 3.23701e-05/step  Wall: 0.00374317, 3.23701e-05/step, 0 hours remaining, 147.921875 MB of memory in use.
ENERGY:     100         1.0482         0.7237         0.0000         0.0000              0.0000         0.0000         0.0000         0.0000         0.9800              2.7519       164.3817         1.7719         2.7522       258.2467

OPENING COORDINATE DCD FILE
WRITING COORDINATES TO DCD FILE AT STEP 100
The last position output (seq=100) takes 0.000 seconds, 147.922 MB of memory in use
OPENING VELOCITY DCD FILE
WRITING VELOCITIES TO DCD FILE AT STEP 100
The last velocity output (seq=100) takes 0.000 seconds, 147.922 MB of memory in use
WRITING EXTENDED SYSTEM TO OUTPUT FILE AT STEP 100
WRITING COORDINATES TO OUTPUT FILE AT STEP 100
CLOSING COORDINATE DCD FILE
The last position output (seq=-2) takes 0.027 seconds, 147.922 MB of memory in use
WRITING VELOCITIES TO OUTPUT FILE AT STEP 100
CLOSING VELOCITY DCD FILE
The last velocity output (seq=-2) takes 0.025 seconds, 147.922 MB of memory in use
====================================================

WallClock: 0.088292  CPUTime: 0.088292  Memory: 147.921875 MB
Program finished.
