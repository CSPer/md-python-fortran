
#turn on debugging output
param debug

#no shake
#param shake

#load up the system
read system /home/csp1g13/irp/rdfmd/tags/rdfmd-1_0/test/water/input/water-marked.pdb

#load up the filter
read filter /home/csp1g13/irp/rdfmd/tags/rdfmd-1_0/test/water/input/filter

#set the index of the central configuration
param centindex 1001

#read in the starting coordinates and velocities - ascii output files from
#namd use velocity units of A ps-1
read pdb type=coord file=input.coor start=996 end=996
read pdb type=vel file=input.vel start=996 end=996

#now read in the coordinate and velocity buffers from the backwards simulation
#note that binary files in namd write velocities in amber velocity units
read dcd type=coord file=coordinates.dcd_reverse start=995 end=1
read dcd type=vel file=velocities_reverse start=995 end=1 velscl=amber

#now read in the coordinate and velocity buffers from the forwards simulation
#note that binary files in namd write velocities in amber velocity units
read dcd type=coord file=coordinates.dcd start=997 end=2001
read dcd type=vel file=velocities start=997 end=2001 velscl=amber

#calculate the kinetic energy before filtering
kineticnrg state=old

#apply the filter
filter vscale=1.000000 fscale=5.000000 tempcap=1200.000000

#calculate the kinetic energy after filtering
kineticnrg state=new

#write filter velocity and mass 
filtvel

#write the output coordinate and velocity files
write pdb type=coord file=output.coor
write pdb type=vel file=output.vel

#remove the backwards buffer files...
remove coordinates.dcd_reverse
remove velocities_reverse

#strip the forwards coordinate file down so that only the frames between the start
#of the trajectory and the end of the 'delay' region remain
strip dcd in=coordinates.dcd out=coordinates.dcd_strip start=1 end=5 delta=1

#finally remove the forwards buffer files...
remove coordinates.dcd
remove velocities

                     