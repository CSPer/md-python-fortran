#!/bin/env python

import sys
import os

#set the path to the 'rdfmd' directory that contains the 
#rdfmd python module
sys.path.append("/home/csp1g13/irp/rdfmd/tags/rdfmd-1_0/")

#import the rdfmd python module
from python_source import *

#set the base directory for the simulation and change into it
basedir = File("~/irp/rdfmd/tags/rdfmd-1_0/test/aladip")
os.chdir( basedir.toString() )

#create the output directory - start counting at 0
simdir = SimDir("output","equil",0)
simdir.createRoot()

#now link the input files to the current output directory
linkFile("input/equilinput", simdir.current())

#create a digital filtering configuration for namd simulations
dfconf = NamdDFConf()

#create a HMC configuration for HMC simulation
hmcconf = NamdHMCConf()

#set the digital filtering parameters
dfconf.setFilter("input/filter0_300", 2001)  # load a 2001 coeff filter
dfconf.setSystem("input/aladip-marked.pdb")  # this file sets the atom
                                # types and which atoms will be filtered

dfconf.setFiltercap(2)        # set the filter cap
dfconf.setTempcap(2000.0)     # set the temperature cap
dfconf.setTempcapFile("input/aladip-cap.pdb") #set the temp cap file
dfconf.setDelay(10)          # set the filter delay
dfconf.setFScale(2.0)         # set the filter scale
dfconf.setVScale(1)         # set the velocity scale
dfconf.setHMCmode()         # set the hmc mode so that DF will write the filter velocities        
                          

#now create a digital filtering driver - pass it the location of the
#'digifilter' program that will actually be performing the filtering
dfdriver = DFDriver("bindir/digifilter")

# create a HMCDriver
hmcdriver = HMCDriver("bindir/hmc")

#now create a simulation configuration for namd - load it up from the 
#template file 'alanin.namd'
simconf = NamdSim("input/aladip.namd")

simconf.setCoordFreq(10) # print out coordinates every 10 steps
simconf.setVelFreq(0)    # never print out velocities

#create a driver to run NAMD simulations - pass it the location of the
#namd2 binary that will be used to perform the MD
simdriver = NamdDriver("bindir/namd2", "bindir/charmrun")

#create a monitor to monitor the temperature during filtering
monitor = NamdMonitor("bindir/digifilter", "output/monitor.log")
monitor.addSystemPDB("filtered","input/aladip-marked.pdb")
monitor.addSystemPDB("backbone","input/aladip-cap.pdb")

#finally, create the rdfmd simulation controller object
sim = DFSim()

#run some nvt equilibration at 298K
simdir = sim.nvtSim(simdir, simdriver, simconf, 500, 300)

simdir = sim.dfhmcSim(simdir, simdriver, simconf, hmcdriver, hmcconf, dfdriver, dfconf, 50, 25, 10)
#perform some digital filtering
#simdir = sim.rdfmdSim(simdir, simdriver, simconf, dfdriver, dfconf, monitor)

#simdir = sim.hmcSim(simdir, simdriver, simconf, hmcdriver, hmcconf, 50)
#do some more nvt
#simdir = sim.nvtSim(simdir, simdriver, simconf, 1000, 298)

#some more digital filtering
#simdir = sim.rdfmdSim(simdir, simdriver, simconf, dfdriver, dfconf, monitor)

#do some more nvt
#simdir = sim.nvtSim(simdir, simdriver, simconf, 1000, 298)

#and on, and on, and on,  ...
