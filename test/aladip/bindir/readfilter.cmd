#Read in the filter
read filter ../input/filter0_300

#Now calculate the frequency response, using a time step of 1fs
viewfilter 1.0 viewfilter.data
