#!/bin/env python

import sys
import os

#set the path to the 'rdfmd' directory that contains the 
#rdfmd python module
sys.path.append("/home/chris/Work/WorkSoftware")

#import the rdfmd python module
from rdfmd import *

#set the base directory for the simulation and change into it
basedir = File("./")
os.chdir( basedir.toString() )

#create the output directory - start counting at 0
simdir = SimDir("output","equil",0)
simdir.createRoot()

#now link the input files to the current output directory
linkFile("input/equilinput", simdir.current())

#create a digital filtering configuration for namd simulations
dfconf = NamdDFConf()

#set the digital filtering parameters
dfconf.setFilter("input/filter0_25", 2001)   # load a 2001 coeff filter
dfconf.setSystem("input/alanin-marked.pdb")  # this file sets the atom types and
                                             # which atoms will be filtered

dfconf.setFiltercap(5)        # set the filter cap
dfconf.setTempcap(600.0)      # set the temperature cap
dfconf.setDelay(250)          # set the filter delay
dfconf.setFScale(2.0)         # set the filter scale

#now create a digital filtering driver - pass it the location of the 'digifilter'
#program that will actually be performing the filtering
dfdriver = DFDriver("bindir/digifilter")

#now create a simulation configuration for namd - load it up from the template
#file 'alanin.namd'
simconf = NamdSim("input/alanin.namd")

simconf.setCoordFreq(10) # print out coordinates every 10 steps
simconf.setVelFreq(0)    # never print out velocities

#create a driver to run NAMD simulations - pass it the location of the namd2
#binary that will be used to perform the MD
simdriver = NamdDriver("bindir/namd2")

#finally, create the rdfmd simulation controller object - this controls the whole simulation
sim = DFSim()

#run some nvt equilibration
simdir = sim.nvtSim(simdir, simdriver, simconf, 5000, 300.0)  # 5000 steps at 300K

#perform some digital filtering
simdir = sim.rdfmdSim(simdir, simdriver, simconf, dfdriver, dfconf)

#do some more nvt
simdir = sim.nvtSim(simdir, simdriver, simconf, 5000, 300.0)

#some more digital filtering
simdir = sim.rdfmdSim(simdir, simdriver, simconf, dfdriver, dfconf)

#and on, and on, and on,  ...
