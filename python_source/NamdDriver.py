############################################
#
#   This class provides an interface to
#   NAMD 2.5. This driver is used to 
#   run a NAMD simulation (a NamdSim)
#
#   (C) Christopher Woods, May 2005

import os
import sys
from File import *
from NamdSim import *
from RunCmds import *

class NamdDriver:
    
    def __init__(self, namd2="/usr/local/bin/namd2", \
                       charmrun="/usr/local/bin/charmrun"):
        
        self.namd2 = File(namd2).fullPath()
        self.charmrun = File(charmrun).fullPath()
        self._code = "NAMD_25"
        self._nodefile = None
        self._nnodes = 1
          
    def code(self):
        """Return the code for which this class has been written - in this case 
           namd 2.5"""
        return self._code
 
    def setNodeFile(self,nodefile,nnodes=1):
          """Set the nodefile to use - if this is set then the simulations will be
             run in parallel using these nodes... Also set the number of nodes
             to use!"""
          self._nodefile = File(nodefile).fullPath()
          self._nnodes = int(nnodes)
    
    def run(self, simdir, namdsim):
        """Run the simulation described in namdsim in the current directory of simdir"""
                     
        #check that we have really been passed a 'NamdSim'
        if (namdsim.code() != "NAMD_25"):
            print "WARNING! NamdDriver.run() with a non-NAMD_25 NamdSim (%s)" \
                          % namdsim.mdcode
            sys.exit(-1)
           
        #change to the simulation directory
        simdir.change()
        
        #write the command file to disk - this returns a 'File'
        #to the command file
        if (namdsim.hasForwards()):
             print "    Running %d steps of forwards dynamics..." % namdsim.forwards()
              
             cmdfile_forwards = namdsim.writeForwardsCommandFile()
        
             #get the name of the file to be used for stdout/stderr
             stdout = namdsim.stdout()
        
             if (stdout.isNone()):
                   stdout = File("/dev/null")
        
             if (self._nodefile is None):
                  #command line should be "namd2 file.namd > stdout-file 2>&1"
                  cmd = "%s %s > output.log.txt" % (self.namd2,  \
                                              cmdfile_forwards)
             else:
                  #command is "charmrun ++verbose ++remote-shell ssh ++p nnodes namd2 file.namd ...
                  cmd = "%s ++verbose ++remote-shell ssh ++nodelist %s ++p %d %s %s > %s 2>&1" % \
                             (self.charmrun, self._nodefile, self._nnodes, self.namd2, cmdfile_forwards, stdout)
        
             #actually run the simulation
             runCmd(cmd)
             
             #compress the stdout file
             #stdout.compress()
        
        #now perform the backwards simulation
        if (namdsim.hasBackwards()):
             print "    Running %d steps of backwards dynamics..." % namdsim.backwards()
              
             #because we are running backwards, we need to reverse the input
             #velocities...
             namdsim.reverseInputVelocities(simdir.current())

             #now write the backwards command file
             cmdfile_backwards = namdsim.writeBackwardsCommandFile()
             
             stdout = namdsim.stdout()
             if (stdout.isNone()):
                   stdout = File("/dev/null")
             else:
                   stdout.append("_reverse")
                    
             if (self._nodefile is None):
                  #command line should be "namd2 file.namd > stdout-file 2>&1"
                  cmd = "%s %s > %s 2>&1" % (self.namd2,  \
                                              cmdfile_backwards, stdout)
             else:
                  #command is "charmrun ++verbose ++remote-shell ssh ++p nnodes namd2 file.namd ...
                  cmd = "%s ++verbose ++remote-shell ssh ++nodelist %s ++p %d %s %s > %s 2>&1" % \
                             (self.charmrun, self._nodefile, self._nnodes, self.namd2, cmdfile_backwards, stdout)
        
             #actually run the simulation
             runCmd(cmd)
             
             #compress the stdout file
             #stdout.compress()
        
        #return the the original directory
        simdir.revert()
                
