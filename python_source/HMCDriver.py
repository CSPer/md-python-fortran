######################################
#
#     This is the driver that 
#     actually calls HMC
#     to perform the HMC 
#
#     (C) Can Pervane, May 2014
#
#
#

from DFSim import *

class HMCDriver:
      def __init__(self,hmc):
            self._hmc = File(hmc).fullPath()
            
      def perform(self,simdir,sim,hmcconf):
            """Perform HMC using the simulation in directory 'simdir',
               using the simulation described in 'sim', using the hmc configuration
               'hmcconf'"""
               
            #change into the current directory of the simulation
            simdir.change()
            
            #now tell the dfconf to write out a command file for digifilter...
            cmdfile = hmcconf.write("hmc.cmd",sim)
            
            #run HMC
            lines = pipeRun("%s %s 2>&1" % (self._hmc, cmdfile))
            
            #write these lines to a tmp file
            hmclog = File("hmc.log")
            hmclog.write(string.join(lines,"\n"))
             
	    #compress the output file
            hmclog.compress()
                        
            #revert back to the original directory
            simdir.revert()
            
    
            
