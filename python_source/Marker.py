#############################################
#
#   This class is used to mark atoms in 
#   a pdb file and to add the element
#   type.
#
#   (C) Christopher Woods, June 2005
#
#
#

from File import *

class Marker:
        def __init__(self,pdbfile=None):
            if (pdbfile is not None):
                self.readPDB(pdbfile)
                
        def clear(self):
            """Clear this marker of all information"""
            self._lines = []
            self._marked = {}
            self._atoms = {}
            self._ids = {}
                
        def readPDB(self,pdbfile):
            """Read in the pdb file, replacing any held
               atoms with the contents of this file"""
            self.clear()

            pdbfile = File(pdbfile)
            
            self._lines = pdbfile.readlines()
            self.scan()

        def scan(self):
            """Scan the loaded pdb and automatically assign the
               element types of the atoms"""
            
            i = 0
            for line in self._lines:
                line2 = line.lower()
                if (line2[0:4] == "atom" or line2[0:6] == "hetatm"):
                    self._marked[i] = 1
                    self._atoms[i] = self.guessAtom(line2[12:16])

                    #save the atom number of each line
                    self._ids[i] = int(line[6:11])
                    
                i = i + 1

        def guessAtom(self,name):
            """Try to guess the element type from the atom name"""
            
            name = name.lower().lstrip().rstrip()
            
            if (name[0:1] == "c"):
                return "C"
            elif (name[0:1] == "h"):
                return "H"
            elif (name[0:1] == "p"):
                return "P"
            elif (name[0:1] == "o"):
                return "O"
            elif (name[0:1] == "n"):
                return "N"
            elif (name[0:1] == "s"):
                return "S"
            elif (name[0:1] == "f"):
                return "F"
            else:
                return "X"

        def write(self,pdbfile):
            """Write the marked pdb to 'pdbfile'"""
            
            pdbfile = File(pdbfile)
            pdbfile.remove()
            
            i = 0
            for line in self._lines:
                if (self._atoms.has_key(i)):
                    outline = "%s   %s   %d\n" % (line[0:54],self._atoms[i],self._marked[i])
                    pdbfile.write(outline)
                else:
                    pdbfile.write("%s\n" % line)
                    
                i = i + 1

        def markRange(self,strt,end,step=1):
              for i in range(strt,end+1,step):
                    self.mark(i)
                    
        def unmarkRange(self,strt,end,step=1):
              for i in range(strt,end+1,step):
                    self.unmark(i)
                
        def mark(self,idnum):
            """Mark atom number 'idnum' (same number as 
               given in the pdb file). If multiple atoms
               have the same number then all of them are marked"""

            for num in self._ids:
                atmid = self._ids[num]
                if (atmid == idnum):
                    self._marked[num] = 1
            
        def unmark(self,idnum):
            """Unmark all atoms with atom number 'idnum'"""

            for num in self._ids:
                atmid = self._ids[num]
                if (atmid == idnum):
                    self._marked[num] = 0

        def markAll(self):
            """Mark all atoms"""
            
            for num in self._marked:
                self._marked[num] = 1
                
        def unmarkAll(self):
            """Unmark all atoms"""
            
            for num in self._marked:
                self._marked[num] = 0
                
        def setAtom(self,idnum,typ):
            """Sets the element type of the atom with atom number
               idnum to 'typ'"""
            
            for num in self._ids:
                atmid = self._ids[num]
                if (atmid == idnum):
                    self._atoms[num] = typ
            
