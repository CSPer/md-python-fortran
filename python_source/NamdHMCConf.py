#################################################################
#
#     This class provides the digital filtering configuration
#     for a namd based digital filtering simulation.
#
#     This class is solely responsible for writing the 
#     digifilter command file. I recommend that if you 
#     want more control over the digital filtering, that
#     you create your own class that writes the command
#     file and that you use that class in preference
#     over this.
#
#     (C) Christopher Woods, June 2005
#
#

from File import *
from HMCConf import *
from DFSim import *

class NamdHMCConf(HMCConf):
      def __init__(self):
            HMCConf.__init__(self)
            self._code = "NAMD_25"
            
      def code(self):
            """Return the type of md code for which this class was designed"""
            return self._code
      
      def write(self,cmdfile,namdsim):
            """Write a digital filtering command file to 'cmdfile'"""
            
            #check that namdsim is compatible with this
            checkCompatibility( [self,namdsim] )
            
            #create the command file, forcing any existing version to be replaced
            cmdfile = File(cmdfile)
            cmdfile.create(replace=True)
            
            #construct the contents of the command file...
            contents = self.commandFileContents(namdsim)
            
            #write the contents to the file
            cmdfile.write(contents)
            
            #we are all done!
            return cmdfile
            
      def commandFileContents(self,namdsim):
            """Return a string containing the contents of the command file. Reimplement
               this function to gain complete control over the digital filtering"""
                      

            cmdfile = """
readfiltvel
genvel
writegenvel
"""         
            if (self.mode()):
                  cmdfile = """
readfiltvel
test
writeaccept
"""   
                  if (self.writecoords()):
                        cmdfile = """
%s
writecoords frames=%d
                  """ % (cmdfile, self.frame())

                  if (self.remove()):
                        cmdfile = """
%s
remove %s
remove %s
remove %s
remove %s
remove %s
remove %s
remove %s
remove %s
                  """ % (cmdfile, \
                         namdsim.inputCoords(), namdsim.inputVels(), \
                         namdsim.coordinates(), namdsim.velocities(), \
                         "output.log.txt", namdsim.inputXSC(), "hmc.cmd", \
                         "cmdfile_forwards.namd")
              
            return cmdfile
                  
