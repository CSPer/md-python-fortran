#############################################
#
#     NamdMonitor provides the a monitor
#     for NAMD simulations that can be used
#     to monitor properties throughout
#     the RDFMD simulation (e.g. KE or temperature)
#
#     (C) Christopher Woods, June 2005
#
#

from NamdSim import *
from SimDir import *
from File import *

class NamdMonitor:
      
      def __init__(self,digifilter="/usr/local/bin/digifilter",logfile="monitor.log"):
            """Create a monitor that uses the passed 'digifilter' executable to do its work"""
            self._digifilter = File(digifilter).fullPath()
            self._logfile = File(logfile).fullPath()
            self._logfile.remove()
            
            self._syspdbs = []
            self._nam = []
            
      def addSystemPDB(self,nam,syspdb):
            """Add a system PDB containing the set of marked atoms to be monitored"""
            self._nam.append(nam)
            self._syspdbs.append(File(syspdb).fullPath())
            
      def monitor(self,simdir,sim):
            """Monitor the output files in simdir, from the simulation described in 'sim'"""
            
            #get the name and format of the output coordinates file...
            if (sim.outputCoordsFormat() != "PDB" or sim.outputVelsFormat() != "PDB"):
                  print "Monitor can only monitor PDB file output!"
                  return
            
            outcoords = sim.outputCoords(simdir.current())
            outvels = sim.outputVels(simdir.current())

            self._logfile.write("******* Info for %s ********\n" % File(simdir.current()).shortName())
            
            #now write a command file to load this configuration for each of the system files,
            #and calculate its temperature and kinetic energy
            for i in range(0,len(self._syspdbs)):
                  pdb = self._syspdbs[i]
                  info = self.getInfo(pdb, outcoords, outvels)

                  strng = "%s: %s\n" % (self._nam[i], info)
                  print "    %s" % strng.rstrip("\n")                  
                  
                  self._logfile.write(strng)
                  
      def getInfo(self, syspdb, coords, vels):
            """Get the information from the passed coordinate and velocity files 
               using the system pdb 'syspdb'"""
               
            cmd = """
#turn off debugging
param debug

#turn on shake
param shake

#read the system pdb
read system %s

#read the coordinates and velocities (no velocity scale for NAMD pdbs)
read pdb type=coord file=%s
read pdb type=vel file=%s

#now of just the marked region
kineticnrg markedonly
"""                     % (syspdb, coords, vels)

            #write this command to the file
            cmdfile = File("temp-cmdfile")
            cmdfile.write(cmd)
            
            #now run digifilter and get the output
            lines = pipeRun("%s %s 2>&1" % (self._digifilter, cmdfile))
            
            #remove the command file
            cmdfile.remove()
            
            #parse the output
            for line in lines:
                  if (line.find("kinetic energy") != -1):
                        return line
                        
                  
            return "No kinetic energy calculated"
            