#############################
#
#  This file contains functions
#  that are used to run commands
#
#  (C) Christopher Woods, May 2005
#
#

import os

def runCmd(cmd):
	"""Run the command 'cmd'"""

        exitval = os.system(cmd)
        
        if (exitval != 0):
            raise "Command '%s' exited badly!" % cmd

def pipeRun(cmd):
        """Run the command 'cmd' and return the standard output as 
           a list of lines"""
           
        pipe = os.popen(cmd,"r")
        lines = pipe.readlines()
        pipe.close()
        
        lines2 = []
        
        for line in lines:
            lines2.append(line.rstrip("\n"))
        
        return lines2
        
