######################################
#
#     This is the driver that 
#     actually calls digifilter
#     to perform the digital filtering
#
#     (C) Christopher Woods, June 2005
#
#
#

from DFSim import *

class DFDriver:
      def __init__(self,digifilter):
            self._digifilter = File(digifilter).fullPath()
            
      def perform(self,simdir,sim,dfconf):
            """Perform digital filtering using the simulation in directory 'simdir',
               using the simulation described in 'sim', using the df configuration
               'dfconf'"""
               
            #change into the current directory of the simulation
            simdir.change()
            
            #now tell the dfconf to write out a command file for digifilter...
            cmdfile = dfconf.write("dfmd.cmd",sim)
            
            #run digifilter
            lines = pipeRun("%s %s 2>&1" % (self._digifilter, cmdfile))
            
            #write these lines to a tmp file
            dflog = File("dfmd.log")
            dflog.write(string.join(lines,"\n"))
           
            os.system("mv output.coor input.coor")
            os.system("mv output.vel input.vel")
            os.system("mv output.xsc input.xsc")
                       
            #check the output to see if the temperature cap was breached...
            self.checkTempBreach(lines)

            #compress the output file
            dflog.compress()
                        
            #revert back to the original directory
            simdir.revert()
            
      def checkTempBreach(self,lines):
            """Runs through the digifilter output in 'lines' and sees if the temperature
               cap was breached - if so then it raises a TempCapBreached exception"""
            
            i = 0
            for line in lines:
                  i = i + 1
                  if (line.find("Exceeded temperature cap") != -1):
                        raise TempCapBreached(line + " " + lines[i])
            
