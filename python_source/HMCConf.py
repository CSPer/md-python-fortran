###############################################
#
#     This class contains the configuration
#     for an HMC simulation
#
#     You will need to use this class to 
#     write custom HMC command files...
#
#     (C) Can Pervane, May 2014
#
#
#

from File import *

class HMCConf:
    def __init__(self):
        """Create a HMCConf onject"""
        self._mode = 0
        self._remove = False
        self._writecoords = False
        self._frame = 1

    def setFrame(self, value):
        """ sets the removing file option to true """
        self._frame= int(value)

    def frame(self):
        """ Return the option of removing files"""
        return self._frame
    
    def setWritecoords(self,value):
        """ sets the removing file option to true """
        if (value == 1):
            self._writecoords = True
        if (value == 0):
            self._writecoords = False
            
    def writecoords(self):
        """ Return the option of removing files"""
        return self._writecoords


    def setRemove(self, value):
        """ sets the removing file option to true """
        if (value == 1):
	    self._remove = True
        if (value == 0):
            self._remove = False

    def remove(self):
        """ Return the option of removing files"""
        return self._remove

    def setMode(self, val):
        if (val == "test"):
            self._mode = 1
        else:
            self._mode = 0
        
    def mode(self):
        """ Returns the mode of HMC run"""
        return self._mode

    def output(self, rundir=None):
        """Return the path and name of the output file"""
        return makeFile(rundir, "filtvel.dat")

    def input(self, rundir=None):
        """Return the path and name of the input file"""
        return makeFile(rundir, "filtvel.dat")      

    def linkFiles(self, olddir, newdir):
        """Link the output to the input file"""  
	self.output(olddir).linkTo(self.input(newdir))
            

