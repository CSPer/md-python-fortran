############################################
#
#   This class represents an incrementing
#   simulation directory.
#
#   (C) Christopher Woods
#
#
#

from RunCmds import *
from File import *
import sys
import os

class SimDir:
        def __init__(self,basedir,nam,strt):
            self._base = File(basedir).fullPath()
            self._nam = nam
            self._curnt = int(strt)
            self._dirstack = []
            
        def change(self):
              """Change to the 'current()' directory. The previous directory is stored
                 in a stack"""
              
              olddir = os.getcwd()
              os.chdir(self.current())
              
              self._dirstack.append(olddir)
            
        def revert(self):
              """Revert back to the previous directory. Does nothing if there is no 
                 previous directory"""
              
              if (len(self._dirstack) > 0):
                    olddir = self._dirstack.pop()
                    
                    os.chdir(olddir)
              
        def current(self, n=None):
            """Return the current directory"""
            
            if (n is None):
                n = self._curnt
            
            return "%s/%00005d_%s" % (self._base.toString(),n,self._nam)

        def last(self):
            """Return the last existing directory, or throw an
               exception if it does not exist"""
            lines = pipeRun("ls -d %s/%00005d*" % (self._base.toString(),self._curnt-1))
            
            if (len(lines) > 0):
                return lines[0]
            else:
                raise "No last directory in SimDir.last()"

        def create(self, n=None):
            """Create directory 'n'. If n is none, then create
               the current directory"""
            try:
                  simdir = self.current(n)
                  os.makedirs(simdir)        
                  return    
            except:
                  print "Directory '%s' already exists! " \
                        "You must remove it before we can continue!" % self.current(n)
                  sys.exit(-1)

        def createRoot(self):
            """This creates the root output directory"""
            try:
                  os.makedirs(self._base.fullPath().toString())
            except:
                  print "WARNING! Base output directory '%s' already exists or could not be created!" % \
                                   self._base
                  
        def increment(self):
            """Increment the current directory"""
            self._curnt = self._curnt + 1            

        def setCurrent(self,n):
            """Set the current directory to 'n'"""
            self._curnt = n
            
        def setName(self,nam):
            """Set the name of the directory"""
            self._nam = nam
            

if (__name__ == "__main__"):
    
    s = SimDir("output","npt","0")

    s.create()

    print os.getcwd()
    s.change()
    print os.getcwd()
    
    s.increment()
    s.create()
    s.change()
    print os.getcwd()
    s.revert()
    print os.getcwd()
    s.revert()
    print os.getcwd()
    s.revert()
    print os.getcwd()    
    