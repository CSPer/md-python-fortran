###############################################
#
#     This class contains the configuration
#     for an rdfmd simulation
#
#     You will need to use this class to 
#     write custom digifilter command files...
#
#     (C) Christopher Woods, June 2005
#
#
#

from File import *

class DFConf:
      def __init__(self):
            self._filtercap = 0
            self._tempcap = 0
            self._delay = 0
            self._nframes = 0
            self._filter = File()
            self._system = File()
            self._tempcapfile = File()
            self._fscale = float(2.0)
            self._vscale = float(1.0)
            self._remove = False
            self._hmcMode = False
          
      def setHMCmode(self):
            """ sets the removing file option to true """
            self._hmcMode = True

      def hmcMode(self):
            """ Return the option of removing files"""
            return self._hmcMode

      def setRemove(self):
            """ sets the removing file option to true """
            self._remove = True 
             
      def remove(self):
            """ Return the option of removing files"""
            return self._remove

      def filtercap(self):
            """Return the filter cap"""
            return self._filtercap

      def setFiltercap(self,cap):
            """Set the filter cap to 'cap' - needs to be an integer"""
            self._filtercap = int(cap)
            
      def setTempcap(self,tempk):
            """Set the temperature cap to tempk kelvin"""
            self._tempcap = tempk
      
      def tempcap(self):
            """Return the temperature cap (in kelvin)"""
            return self._tempcap
      
      def delay(self):
            """Return the filter delay"""
            return self._delay

      def setDelay(self,delay):
            """Set the filter delay to 'delay' - needs to be an integer"""
            self._delay = int(delay)
            
      def nframes(self):
            """Return the number of frames in the buffer (also the number of
               coefficients in the filter"""
            return self._nframes
      
      def fscale(self):
            """Return the filter scale"""
            return self._fscale
      
      def vscale(self):
            """Return the velocity scale"""
            return self._vscale
      
      def setFScale(self,val):
            """Set the filter scale"""
            self._fscale = float(val)
            
      def setVScale(self,val):
            """Set the velocity scale"""
            self._vscale = float(val)
      
      def setFilter(self, filterfile, nframes):
            """Set the file containing the filter and the number of frames in the buffer"""
            self._filter = File(filterfile).fullPath()
            self._nframes = nframes
            
      def filter(self):
            """Return a File representing the filter file"""
            return self._filter
      
      def system(self):
            """Return a File representing the system file"""
            return self._system
      
      def setSystem(self,systm):
            """Set the file containing the system"""
            self._system = File(systm).fullPath()
            
      def setTempcapFile(self,tempcapfile):
            """Set the file containing the marked atoms for the temperature cap"""
            self._tempcapfile = File(tempcapfile).fullPath()
            
      def tempcapFile(self):
            """Return the file containing the marked atoms for the temperature cap"""
            return self._tempcapfile
