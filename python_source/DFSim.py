#########################################################
#
#     This class represents a digitial filtering
#     simulation. This is the top level class that
#     you will use to run your digital filtering 
#     simulations.
#
#     (C) Christopher Woods, June 2005
#
#
#

###################
#
#  This class makes extensive use of 'ducktypes'. These are classes that
#  act, look and behave in a particular way (if it walks like a duck and
#  sounds like a duck, then it must be a duck!)
#
#  The ducktypes used are;
#
#  sim     :  This class describes an MD simulation. It contains all of the parameters
#             needed to describe the simulation. It must have the following member functions;
#
#               (1) sim.setNPT(temp,press)      : set parameters for an NPT sim at 'temp' and 'press'
#               (2) sim.setNVT(temp)            : set parameters for an NVT sim at 'temp'
#               (3) sim.setNVE()                : set parameters for an NVE sim
#               (4) sim.setForwards(nsteps)     : set parameters to run 'nsteps' of forwards dynamics
#               (5) sim.setBackwards(nsteps)    : set parameters to run 'nsteps' of backwards dynamics
#               (6) sim.linkFiles(old,new)      : link output files from 'old' dir to input files of 'new' dir
#               

#  driver  :  This class drives the MD simulation described by 'sim'. Both driver and sim must
#             be compatible with one another...
#
#               (1) driver.run(simdir,sim)      : run the simulation described by 'sim' in the current
#                                                 directory of 'simdir' (simdir.current()). Note that
#                                                 this should run both the forwards and backwards
#                                                 dynamics, if requested to.

#  simdir  :  This class represents and keeps track of the output simulation directory. The 
#             output directories are numbered sequentially, and this class provides functions to
#             go to the next directory and to find the previous directories...
#
#              (1) simdir.current()             : return the full path to the current output directory
#              (2) simdir.increment()           : increment to the next output directory
#              (3) simdir.last()                : return the full path to the last output directory
#              (4) simdir.setName()             : set the id name of the output directory
#              (5) simdir.create()              : create the current output directory

#  dfconf  :  This class represents the configuration for the actual digital filtering. It is
#             used extensively by the dfdriver. dfconf must thus be
#             compatible with dfdriver
#
#             (1) dfconf.filtercap()            : return the filter cap (maximum number of filter applications)
#             (2) dfconf.nframes()              : return the total number of frames in the filter buffer
#             (3) dfconf.delay()                : return the filter delay
#             (4) dfconf.tempcap()              : return the temperature cap (maximum temperature of 
#                                                                        filtered velocities in kelvin)

#  dfdriver : This class is used to drive the actual digital filtering. This class must be compatible
#             with dfconf, and with sim, as it uses the sim class to find out what the output
#             files are, and what their formats are. 
#
#             (1) dfdriver.perform(simdir,sim,dfconf)  : perform digital filtering on the output files
#                                           described by sim, using digital filtering parameters described
#                                           by dfconf, in the current directory of simdir (simdir.current())

#  This, DFSim class provides a few, key functions;
#
#             (1) DFSim.nptSim(...)   : Perform a standard NPT simulation
#             (2) DFSim.nvtSim(...)   : Perform a standard NVT simulation
#             (3) DFSim.nveSim(...)   : Perform a standard NVE simulation
#             (4) DFSim.rdfmdSim(...) : Perform an rdfmd simulation
#
#        These functions are fully commented and documented in the source code below.
#
#
import time
from File import *
from os import *

def checkCompatibility( objs ):
     """Check the 'obj.code()' is the same for all objects in the list"""

     if (len(objs) <= 1):
            return True
     else:
           code = objs[0].code()
           for obj in objs:
                 if (code != obj.code()):
                       raise "Incompatible code! (%s vs. %s)" % (code,obj.code())

class TempCapBreached:
      """This exception is raised whenever the temperature cap is breached"""
      def __init__(self,errstrng):
            self._error = errstrng
            
      def error(self):
            """Return the error string"""
            return self._error

def linkFile(oldfile, newfile):
      """Link oldfile to newfile. This resolves the links so that if oldfile is a link,
         the newfile is linked to whatever oldfile is linked to. This also ensures that
         the linked file is not compressed"""
        
      oldfile = File(oldfile)
      newfile = File(newfile)
        
      oldfile.linkTo(newfile)
              
class DFSim:
      def __init__(self):
            self._version = 0.1
      
      def nptSim(self, simdir, driver, sim, nsteps, temperature=298, pressure=1.0):
            """Perform an npt simulation from the current directory of 'simdir', using 'driver' to
               run the simulation described in 'sim'"""
        
            print "Performing an NPT block..."
            
            simdir.increment()
            sim.setNPT(temperature,pressure)
            sim.setForwards(nsteps)
            sim.setBackwards(0)
        
            simdir.setName("npt")
            simdir.create()
            sim.linkFiles(simdir.last(), simdir.current())
        
            driver.run(simdir,sim)
        
            return simdir

      def nvtSim(self, simdir, driver, sim, nsteps, temperature=298):
            """Perform an nvt simulation in the current directory of 'simdir', using 'driver'
               to perform the simulation that is described in 'sim'"""

            print "Performing an NVT block..."
                           
            simdir.increment()
            sim.setNVT(temperature)
            sim.setForwards(nsteps)
            sim.setBackwards(0)
            
            simdir.setName("nvt")
            simdir.create()
            sim.linkFiles(simdir.last(), simdir.current())
            
            driver.run(simdir,sim)
            
            return simdir
      
      def nveSim(self, simdir, driver, sim, nsteps):
            """Perform an nve simulation in the current directory of 'simdir', using 'driver'
               to perform the simulation that is described in 'sim'"""
            
            print "Performing an NVE block..."
            simdir.increment()
            sim.setNVE()
            sim.setForwards(nsteps)
            sim.setBackwards(0)
                    
            simdir.setName("nve")
            simdir.create()
            sim.linkFiles(simdir.last(), simdir.current())
            
            driver.run(simdir,sim)
            
            return simdir
        
      def rdfmdSim(self, simdir, driver, sim, dfdriver, dfconf, monitor=None):
            """Perform an rdfmd simulation in simdir, using driver to perform the 
               simulation described in sim, using dfdriver to perform the digital 
               filtering described in dfconf"""
            try:
                  print "Performing an rdfmd block..."
                  
                  #get the main parameters that describe digital filtering...
                  filtercap = dfconf.filtercap()  # the filter cap
                  nsteps = dfconf.nframes()       # the number of frames in the filter buffer
                  delay = dfconf.delay()          # the filter delay
                  dfconf.setRemove()
                  #create a deep copy of the 'sim' object so that we can change it at will
                  sim = sim.deepCopy()
                  
                  sim.setNVE()  # filtering requires an NVE simulation
                  sim.setFilterConf() # change the configuration for filtering
                                      # This must ensure that shake is switched off, and that
                                      # anything which changes the velocites, e.g. COMmotion is also off!
                  
                  for i in range(1,filtercap+1):
                        print "  Running rdfmd %d out of %d..." % (i,filtercap)
                        
                        #simdir.increment()               # go to the next output directory
                        #simdir.setName("rdfmd_%s" % i)

                        #print out the coordinates and velocities every frame - save the old
                        #frequency
                        oldcoordfreq = sim.coordFreq()
                        oldvelfreq = sim.velFreq()
                        
                        sim.setCoordFreq(1)
                        sim.setVelFreq(1)
                
                        #set the number of forwards and backwards steps that are to
                        #be simulated
                        m = (nsteps / 2) + 1
                        sim.setForwards(m + delay - 1)
                        sim.setBackwards(m - delay - 1)
                        sim.replace("coordinates","input.coor")
                        #create the new output directory and link in the input files
                        #simdir.create()
                        #sim.linkFiles(simdir.last(), simdir.current())
                
                        #run the forwards and backwards simulations
                        driver.run(simdir,sim)

                        #restore the frequency of printing out coordinates and velocities
                        sim.setCoordFreq(oldcoordfreq)
                        sim.setVelFreq(oldvelfreq)
                                   
                        #actually perform the digital filtering
                        dfdriver.perform(simdir,sim,dfconf)
                                  
                        #monitor the result
                        if (monitor is not None):
                              monitor.monitor(simdir,sim)
            
            except TempCapBreached:
                   print "Exceeded temperature cap (%s K)!" % dfconf.tempcap()
                 
            return simdir
 
      def hmcSim(self, simdir, driver, sim, hmcdriver, hmcconf, MDsteps):
            """Perform a hmc simulation in simdir, using driver to perform the 
               simulation described in sim, using hmcdriver to perform the hmc
	       described in hmcconf"""
           
            print "Performing a HMC block..."

            #create a deep copy of the 'sim' object so that we can change it at will
            sim = sim.deepCopy()
                
            sim.setNVE()  # dfhmc requires a NVE simulation  

	    #simdir.increment()
            #simdir.setName("hmc")
            sim.setForwards(MDsteps)
            sim.setBackwards(0)

	    #print out the coordinates and velocities only at the first and last frame - save the old
            #frequency
            oldcoordfreq = sim.coordFreq()
            oldvelfreq = sim.velFreq()
            sim.setCoordFreq(MDsteps)
            sim.setVelFreq(MDsteps)
            
            #make the input coordinates file for MD sim input.coor
            sim.replace("coordinates","input.coor")
            
            #save the energies every nsteps to the log file
            sim.replace("outputenergies", MDsteps)
 	    
            #create the new output directory and link in the input files           
            #simdir.create()
	    #sim.linkFiles(simdir.last(), simdir.current())
            #hmcconf.linkFiles(simdir.last(), simdir.current()) 
                 
            hmcconf.setMode("GenVel")         
            # run hmc to write the input velocity file for the MD steps
            hmcdriver.perform(simdir, sim, hmcconf)
                 
            #run the forward MD steps 
            driver.run(simdir, sim)
                       
            hmcconf.setMode("test")
            # to the acceptance test
            hmcdriver.perform(simdir, sim, hmcconf)      
            
            #restore the frequency of printing out coordinates and velocities
            sim.setCoordFreq(oldcoordfreq)
            sim.setVelFreq(oldvelfreq)     
            
            return simdir 

      def dfhmcSim(self, simdir, driver, sim, hmcdriver, hmcconf, dfdriver, dfconf, MDsteps, HMCsteps, update):
         
            #create a deep copy of the 'sim' object so that we can change it at will
            sim = sim.deepCopy()
    
            simdf = sim   # create a simdf object to configure the NVE sim of digital filter         
            simdf.replace("coordinates","input.coor")            

            sim.setNVE()  # filtering and hmc requires an NVE simulation
            simdir.increment()    # go to the nest output directory
            simdir.setName("dfhmc") # name the directory as dfhmc
            simdir.create()

            simdf.linkFiles(simdir.last(), simdir.current())          
            # do forward MDsteps NVE simulation for HMC sim
            sim.setForwards(MDsteps) 
            sim.setBackwards(0)  
            hmcconf.setFrame(HMCsteps/update)
            hmcconf.setRemove(1)
            counter = 1
            simdir = self.rdfmdSim(simdir, driver, simdf, dfdriver, dfconf)
            for i in range(HMCsteps):
                  hmcconf.setWritecoords(0)
                  print "HMC Step %d Starting... " % (i+1)
                  # write coordinates every update step
                  if ((i + 1) % update == 0):
                        print "Writing coordinates at %d to hmccoords.dcd" % (i+1)        
                        hmcconf.setWritecoords(1)
                
                  start = time.time()
		  simdir = self.hmcSim(simdir, driver, sim, hmcdriver, hmcconf, MDsteps)
                  end = time.time()
                  print "hmcsim step is executed in %.3f seconds" % (end - start)
                  simdir.change()
                  print "The current directory is: %s" % os.getcwd()                  
                  # renames the output coordinates as input coordinates
                  os.system("mv output.coor input.coor")
                  os.system("mv output.vel input.vel")
                  os.system("mv output.xsc input.xsc")
                  simdir.revert()
              
                  # when HMC step is equal to rdfmd step. do digital filtering
                  if ((i + 1) % update == 0 and (i + 1) < HMCsteps):
                        start= time.time()
                        simdir = self.rdfmdSim(simdir, driver, simdf, dfdriver, dfconf)
                        end = time.time()
                        print "rdfmdSim step is executed in %.3f seconds " % (end-start)

            return simdir
