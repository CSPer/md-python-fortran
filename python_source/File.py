############
#
# This class provides a very simple
# wrapper over various file operations
#
# (C) Christopher Woods, May 2005
#
#

from RunCmds import *

import os
import string

compress = "bzip2 -f"
uncompress = "bunzip2"
compress_ext = ".bz2"

def makeFile(path, filename):
      if (filename is None):
            return File()
      elif (path is None):
            return File(filename)
      else:
            return File("%s/%s" % (path,filename))

class File:
    """A very simple wrapper around file operations"""
    
    def __init__(self,filename=None,format=None):
        """Create a File that works on the file called 'filename'"""

        if (filename is None):
            self.__origname = None
        elif (filename.__class__ == File):
            self.__origname = filename.__origname
        else:
            self.__origname = os.path.normpath( \
                                  os.path.expanduser( \
                                     os.path.expandvars(filename) ) )

        self.__format = format

    def __str__(self):
        return self.__origname
        
    def isNone(self):
        return self.__origname is None        
        
    def format(self):
        """Return the format of the file (if it was specified, None if not)"""
        return self.__format        

    def toString(self):
        """Return a python string representation of the file"""
        return self.name()
        
    def fullPath(self):
        """Return a file representing the full path to the file - returns None if the
           file does not exist"""
        if (self.__origname is None):
              return File(None)
        else:
            fullpath = os.path.abspath(self.__origname)
            return File(fullpath)
        
    def directory(self):
        """Returns the directory of this file"""
        direc = os.path.dirname(self.__origname)
        if (direc == ''):
            return "./"
        else:
            return direc
        
    def name(self):
        """Return the name of the file as passed to the constructor"""
        return self.__origname

    def shortName(self):
        """Return the short name (base name, e.g. name without the directory)
           of the File"""
        return os.path.basename(self.__origname)

    def exists(self):
        """Return whether or not this file exists"""
        if (self.__origname is None):
              return False
        else:
              return os.path.exists(self.__origname)

    def append(self,extra):
        """Append 'extra' onto the end of the filename"""
        self.__origname = "%s%s" % (self.__origname, extra)

    def resolvePath(self):
        """This resolves any symbolic links or compression to 
           return a File representing the original file. Returns
           None if the file does not exist."""
        
        fullfile = File(os.path.realpath(self.__origname))
        
        if (fullfile.exists()):
            return fullfile
        else:
            #try the compressed version
            fullfile.append(compress_ext)
            if (fullfile.exists()):
                return fullfile
            else:
                print "WARNING! The file '%s' does not exist!" % self.__origname
                return File()
                
    def uncompress(self):
        """Uncompress the file. Does nothing if the filename does not
           end with the compress_ext"""
        if (self.isNull()):
              print "Warning! Cannot uncompress a null file!"
              return
        
        (root,ext) = os.path.splitext(self.__origname)

        if (ext == compress_ext):
            runCmd("%s %s" % (uncompress,self.__origname))
            self.__origname = root
            
    def compress(self):
        """Compress the file. Does nothing if the filename ends with 
           the compress_ext"""
           
        fullfile = self.resolvePath()
        
        if (fullfile.isNull()):
              print "WARNING! Cannot compress a null file!"
              return
           
        (root,ext) = os.path.splitext(fullfile.__origname)
        
        if (ext != compress_ext):
            runCmd("%s %s" % (compress,fullfile.__origname))
            self.__origname = "%s%s" % (fullfile.__origname,compress_ext)
            
    def create(self, replace=False):
        """Make sure that this file exists"""
        
        if (replace):
             if (self.exists()):
                   self.remove()
        
        runCmd("touch %s" % self.__origname)
        
    def write(self,text):
        """Append 'text' to the file."""
        f = open(self.__origname,"a") 
        f.write(text)
        f.flush()
        f.close()
        
    def readlines(self):
        """Read all lines of text and return them as a list"""
        lines = open(self.__origname,"r").readlines()
        
        lines2 = []
        for line in lines:
            lines2.append(line.rstrip("\n"))
        
        return lines2
        
    def remove(self):
        """Remove this file if it exists"""
        if (self.exists()):
            runCmd("rm -f %s" % self.__origname)

    def copyTo(self,newfile):
        """Copy this file to 'newfile'. This will overwrite newfile 
           if it exists"""
        newfile = File(newfile)
        newfile.remove()
        runCmd("cp %s %s" % (self.__origname,newfile.__origname))

        return newfile

    def isNull(self):
          return self.__origname is None
           
    def linkTo(self,newfile):
        """Links this file to 'newfile', ensuring that the link is made
           to the original, uncompressed file"""
        
        newfile = File(newfile)
        
        if (self.isNull() or newfile.isNull()):
              print "Could not link '%s' to '%s'" % (self.toString(),newfile.toString())
              return None
        
        #resolve this file to the original link - also ensures that this
        #file is not compressed!
        fullfile = self.resolvePath()
        fullfile.uncompress()

        newfile.remove()

        #work out how much of the path they share...
        oldpath = fullfile.toString()
        newpath = newfile.fullPath().toString()

        splitold = oldpath.split("/")
        splitnew = newpath.split("/")
        
        share = 0
        maxlen = len(splitnew)
        
        for i in range(0,len(splitold)):
              share = i
              if (i < maxlen):
                    if (splitold[i] != splitnew[i]):
                          break
        
        #count the number of '/' in the different path
        newpath = string.join(splitnew[share:],"/")
        
        slash = ""
               
        for i in range(0,len(newpath)):
              if (newpath[i:i+1] == "/"):
                    slash = "../%s" % slash
                    
        runCmd("ln -s %s%s %s" % (slash, string.join(splitold[share:],"/"), newfile.name()) )
        
        #running this command may have broken this file - try to fix it!
        #because of this, I will copy the fullfile to this file
        self.__origname = fullfile.__origname
        
        return newfile
        
    def moveTo(self,newfile):
        """Moves this file to newfile - overwrites newfile!"""
        
        if (self.isNone()):
            return None
        
        newfile = File(newfile)
        newfile.remove()
        
        runCmd("mv %s %s" % (self.name(), newfile.name()) )

        self.__origname = newfile.__origname
