#################################
#
# A NamdSim object contains the 
# complete simulation configuration
# for a NAMD 2.5 run.
#
# (C) Christopher Woods, May 2005
#
#

from File import *
import string
import copy

class NamdSim:

    def __init__(self,namdfile=None):
        """Create a NamdSim object. If namdfile is not None, then the configuration
           will be read from the supplied namd command file"""

        self._remove = False

        self._code = "NAMD_25"

        #the name of the file into which to pipe the stdout (relative to the basedir)
        self._stdout = "output.txt"

        #a hash of all of the simulation parameters
        self._params = {}

        #how to represent 'true'
        self._true = "on"
        #how to represent 'false'
        self._false = "off"

        #the list of namd parameters to ignore
        self._ignore = {}
        
        #the number of forwards steps
        self._nforwards = 0
        self._nbackwards = 0
        
        #enums for charmm, amber and gromacs
        self._charmm = "charmm"
        self._amber = "amber"
        self._gromacs = "gromacs"
        
        #default forcefield is charmm
        self._forcefield = "charmm"
        
        #assume a linux fftw file
        self._fftwfile = "FFTW_NAMD_2.5_Linux-i686.txt"
        
        #ignore all other temperature control methods other than langevin
        self.ignore("tcouple")
        self.ignore("tcoupletemp")
        self.ignore("tcouplefile")
        self.ignore("tcouplecol")
        self.ignore("rescalefreq")
        self.ignore("rescaletemp")
        self.ignore("reassignincr")
        self.ignore("reassignhold")
        self.ignore("temperature")
        
        if (namdfile is not None):
            self.readConfig(namdfile)

    def setRemove(self):
            """ sets the removing file option to true """
            self._remove = True

    def remove(self):
            """ Return the option of removing files"""
            return self._remove

    def deepCopy(self):
          """Return a deep copy of this NamdSim"""
          return copy.deepcopy(self)
            
    def ignore(self,param):
          """Ignore this namd parameter"""
          self._ignore[param.lower()] = 1
                        
    def code(self):
          """Return the code for which this class has been written - in this case 
             namd 2.5"""
          return self._code

    def ignoreParams(self,param):
          """Whether or not to ignore this namd parameter"""
          return self._ignore.has_key(param.lower())
                
    def readConfig(self,cmdfile):
        """Read the config file 'cmdfile' and set all parameters from the contained values.
           This also clears all of the parameters already set"""

        cmdfile = File(cmdfile)
           
        self.params = {}
        
        lines = cmdfile.readlines()
        for line in lines:
            words = line.split()
            if len(words) <= 1:
                continue
            elif words[0].find("#") == 0:
                continue

            if (not self.ignoreParams(words[0])):                
                  self.input(words[0],string.join(words[1:]," "))
           
    def isTrue(self,val):
        val = val.lower()
        
        return (val == "yes" or val == "on" or val == "true")
         
    def isFalse(self,val):
        val = val.lower()
         
        return (val == "no" or val == "off" or val == "false")

    def nvals(self,key):
          """Return the number of values associated with key 'key'"""
          if (self._params.has_key(key)):
                return len(self._params[key])
          else:
                return 0
    
    def has_key(self,key):
          """Return whether or not this sim has key 'key'"""
          return self.nvals(key) > 0
          
    def value(self,key,i=0):
        """Returns the 'ith' value which has the key 'key', or None if it doesn't exist"""
        key = key.lower().lstrip().rstrip()
              
        if (i < self.nvals(key)):
              val = self._params[key][i]
        
              if (val is True):
                    return self.true()
              elif (val is False):
                    return self.false()
              else:
                    return val
        else:
              return None

    def true(self):
          return self._true
    
    def false(self):
          return self._false

    def replace(self,key,value):
          """Replace the current value of 'key' with 'value'"""
          key = key.lower().lstrip().rstrip()
          
          while (self._params.has_key(key)):
                del self._params[key]
                
          self.input(key,value)
            
    def input(self,key,value):
        """Input a new key/value pair""" 
        key = key.lower().lstrip().rstrip()
          
        if (value is None):
              if (self._params.has_key(key)):
                  del self._params[key]
              
              return
          
        value = str(value)
        
        if (self.isTrue(value)):
            value = True
        elif (self.isTrue(value)):
            value = False

        if (value == True):
            if (key == "paraTypeCharmm"):
                  self._forcefield = self._charmm
            elif (key == "amber"):
                  self._forcefield = self._amber
            elif (key == "gromacs"):
                  self._forcefield = self._gromacs
            
        self._params.setdefault(key,[]).append(value)

    def hasForwards(self):
          """Return whether this simulation has any 'forwards' dynamics"""
          return self._nforwards > 0
    
    def hasBackwards(self):
          """Return whether this simulation has any 'backwards' dynamics"""
          return self._nbackwards > 0

    def useCharmm(self):
          """Whether or not the charmm forcefield is being used"""
          return self._forcefield == self._charmm
    
    def useAmber(self):
          """Whether or not the amber forcefield is being used"""
          return self._forcefield == self._amber
    
    def useGromacs(self):
          """Return whether or not the gromacs forcefield is being used"""
          return self._forcefield == self._gromacs
    
    def reverseInputVelocities(self,rundir):
          """Reverse the velocities contained in the input file"""
          if (self.binaryOutput()):
                self.ensureValue("binvelocities","input.vel")
                velfile = makeFile(rundir, self.value("binvelocities"))
                self.reverseNamdBin(velfile)
          else:
                self.ensureValue("velocities","input.vel")
                velfile = makeFile(rundir, self.value("velocities"))
                self.reversePDB(velfile)

    def reversePDB(self,pdbfile):
          """Reverses the coordinates contained in the passed pdb file"""
          pdbfile = File(pdbfile)
          lines = pdbfile.readlines()
          pdbfile.append("_reverse")
          pdbfile.create()
          
          newlines = []
          
          for line in lines:
                tag = line[0:6].lower().lstrip().rstrip()
                if ( tag == "atom" or tag == "hetatm" ):
                      line = self.reversePDBLine(line)
                      
                newlines.append(line)
                
          pdbfile.write( string.join(newlines,"\n") )
          
    def reversePDBLine(self,line):
          """Reverse the pdb atom/hetatm line, and return the result"""
          strt = line[0:30]
          x = float(line[30:38])
          y = float(line[38:46])
          z = float(line[46:54])
          end = line[54:]
          
          return "%s%8.3f%8.3f%8.3f%s" % (strt,-x,-y,-z,end)
          
    def reverseNamdBin(self,namdfile):
          """Reverses the coordinates contained in the passed namdbin file"""
          raise "This feature is not yet implemented!!!"
                            
    def writeForwardsCommandFile(self):
        """Write the NAMD command file to disk and return a 'File' to 
           represent it"""
        cmdfile = File("cmdfile_forwards.namd")
        cmdfile.remove()
        cmdfile.create()
        
        self.replace("numsteps",self._nforwards)
        
        keys = self._params.keys()
        keys.sort()
        
        for key in keys:
              nval = self.nvals(key)
              for i in range(0,nval):
                    cmdfile.write("%s %s\n" % (key, self.value(key,i)) )
               
        return cmdfile

    def writeBackwardsCommandFile(self):
        """Write the NAMD command file for the backwards simulation and return
           'File' to represent it"""
        cmdfile = File("cmdfile_backwards.namd")
        cmdfile.remove()
        cmdfile.create()
        
        #set the number of backwards steps
        self.replace("numsteps",self._nbackwards)

        #switch parameters to 'backwards' mode
        self.switchBackwards()
                
        keys = self._params.keys()
        keys.sort()
        
        for key in keys:
              nval = self.nvals(key)
              for i in range(0,nval):
                    cmdfile.write("%s %s\n" % (key, self.value(key,i)) )
               
        #switch back to 'forwards' mode
        self.switchForwards()                  
                  
        return cmdfile

    def switchBackwards(self):
          """Switch the filenames over for the 'backwards' simulation"""
          self.paramAppend("_reverse", "outputname")
          
          if (self.binaryOutput()):
                self.paramAppend("_reverse","binvelocities")
          else:
                self.paramAppend("_reverse","velocities")

          self.paramAppend("_reverse","dcdfile")
          self.paramAppend("_reverse","veldcdfile")

    def switchForwards(self):
          """Switch the filenames back for the 'forwards' simulation"""
          self.paramRemove("_reverse", "outputname")
          
          if (self.binaryOutput()):
                self.paramRemove("_reverse","binvelocities")
          else:
                self.paramRemove("_reverse","velocities")

          self.paramRemove("_reverse","dcdfile")
          self.paramRemove("_reverse","veldcdfile")

    def paramAppend(self,strng,param):
          """Append 'strng' to the end of value 'param'"""
          self.replace(param, self.value(param) + strng)

    def paramRemove(self,strng,param):
          """Remove 'strng' from the end of value 'param'"""
          val = self.value(param)
          if (val.endswith(strng)):
                val = val[0: len(val)-len(strng)]
                self.replace(param,val)

    def usingCharmm(self):
          """Return whether or not the charmm forcefield is being used"""
          return self.value("paratypecharmm") is True
                                                                          
    def ensureValue(self,key,value):
          """If there is no parameter for 'key', then set the default value 'value'"""
          key = key.lower().lstrip().rstrip()
          
          if (not self._params.has_key(key)):
                self.input(key,value)
                
    def checkValue(self,key):
          """Ensure that there is a value for 'key' - raise an exception if there is not"""
          key = key.lower().lstrip().rstrip()
          if (not self._params.has_key(key)):
                print "WARNING! There is no value for parameter '%s'" % key
                raise "WARNING! There is no value for parameter '%s'" % key
    
    def binaryOutput(self):
          """Return whether or not we are using binary ouput (we should not!)"""
          return (self.value("binaryoutput") == self.true() or \
                  self.value("binaryrestart") == self.true())
    
    def inputCoords(self, rundir=None):
          """Return the name of the input coordinate file"""
          if (self.binaryOutput()):
                self.ensureValue("bincoordinates","input.coor")
                return makeFile(rundir, self.value("bincoordinates"))
          else:
                self.ensureValue("coordinates","input.coor")
                return makeFile(rundir, self.value("coordinates"))
          
    def inputVels(self, rundir=None):
          """Return the name of the input velocity file"""
          if (self.binaryOutput()):
                self.ensureValue("binvelocities","input.vel")
                return makeFile(rundir, self.value("binvelocities"))
          else:
                self.ensureValue("velocities","input.vel")
                return makeFile(rundir, self.value("velocities"))
          
    def inputXSC(self, rundir=None):
          """Return the name of the input xsc file"""
          self.checkValue("extendedSystem")
          return makeFile(rundir, self.value("extendedSystem"))
          
    def structureFile(self, rundir=None, i=0):
          """Return the name of the structure (topology) file"""
          if (self.useCharmm()):
                self.checkValue("structure")
                return makeFile(rundir, self.value("structure",i))
          else:
                return File()
          
    def pdbFile(self, rundir=None, i=0):
          """Return the name of the pdb system file"""
          self.checkValue("coordinates")
          return makeFile(rundir, self.value("coordinates",i))          
          
    def paramFile(self, rundir=None, i=0):
          """Return the name of the parameter file"""
          if (self.useCharmm()):
                self.checkValue("parameters")
                return makeFile(rundir, self.value("parameters",i))
          elif (self.useAmber()):
                self.checkValue("parmfile")
                return makeFile(rundir, self.value("parmfile",i))
          elif (self.useGromacs()):
                self.checkValue("grotopfile")
                return makeFile(rundir, self.value("grotopfile",i))
          else:
                return File()
          
    def outputCoords(self, rundir=None):
          """Return the name of the output coordinate file"""
          self.ensureValue("outputname","output")
          return makeFile(rundir, self.value("outputname") + ".coor")

    def outputCoordsFormat(self):
          """Return the format of the output coordinates file"""
          return "PDB"
              
    def outputVels(self, rundir=None):
          """Return the name of the output velocity file"""
          self.ensureValue("outputname","output")
          return makeFile(rundir, self.value("outputname") + ".vel")

    def outputVelsFormat(self):
          """Return the format of the output velocity file"""
          return "PDB"
    
    def outputXSC(self, rundir=None):
          """Return the name of the output xsc file"""
          self.ensureValue("outputname","output")
          return makeFile(rundir, self.value("outputname") + ".xsc")          
          
    def coordinates(self, rundir=None):
          """Return the name of the coordinate trajectory file"""
          return makeFile(rundir, self.value("dcdfile"))
          
    def velocities(self, rundir=None):
          """Return the name of the velocity trajectory file"""
          return makeFile(rundir, self.value("veldcdfile"))     

    def fftwFile(self, rundir=None):
          """Return the name of the FFTW file"""
          return makeFile(rundir, self._fftwfile)
              
    def stdout(self):
        """Where should the stdout from this simulation go? This is the file!
           Return 'None' if you are not interested in keeping the stdout"""
        return File(self._stdout)
           
    def setStdout(self,fle):
        """Set the standard out to go to 'fle'"""
        self.stdout = fle
           
    def outputFiles(self):
        """Return the list of output files that are expected to be created
           by this simulation. Only files on this list will be saved."""
        return None           

    def setFilterConf(self):
          """Change any configuration options to ensure that the configuration
             is correct for digital filtering. In the case of NAMD, this means that
             shake must be switched off, and COMmotion must be set to 'yes'"""
             
          self.replace("COMmotion","yes")
          self.replace("rigidbonds","none")
    
    def compress(self,simdir):
          """Compress the input files and stdout for the simulation run
             in directory 'simdir'"""
          self.inputCoords(simdir.current()).compress()
          self.inputVels(simdir.current()).compress()
          self.inputXSC(simdir.current()).compress()
          self.stdout().compress()
    
    def linkFiles(self,olddir,newdir):
          """Link the output restart files from outdir to the input files of
             newdir"""
          
          if (not(self.inputVels(newdir).exists())):
                self.outputVels(olddir).linkTo(self.inputVels(newdir))
          
          if (self.has_key("extendedsystem")):
                self.outputXSC(olddir).linkTo(self.inputXSC(newdir))
      
          if (not(self.inputCoords(newdir).exists())):
                if (self.value("coordinates") == "input.coor") :
                      self.outputCoords(olddir).linkTo(self.inputCoords(newdir))
                else:
                      for i in range(0,self.nvals("coordinates")):
                            self.pdbFile(olddir,i).linkTo(self.pdbFile(newdir,i))
          
          if (self.useCharmm()):
                self.structureFile(olddir).linkTo(self.structureFile(newdir))
          
          for i in range(0,self.nvals("parameters")):
                self.paramFile(olddir,i).linkTo(self.paramFile(newdir,i))
          
          if (self.fftwFile(olddir).exists()):
                self.fftwFile(olddir).linkTo(self.fftwFile(newdir))
          
    
    def setNVT(self,temperature=298.0):
          """Set the simulation to run in the NVT ensemble at 'temperature' kelvin"""
          self.setTemperature(temperature)
          self.setPressure(None)
          
    def setNPT(self, temperature=298.0, pressure=1.0):
          """Set the simulation to run in the NPT ensemble at 'temperature' kelvin and
             'pressure' atmospheres"""
          self.setTemperature(temperature)
          self.setPressure(pressure)
    
    def setNVE(self):
          """Set the simulation to run in the NVE ensemble"""
          self.setTemperature(None)
          self.setPressure(None)

    def setCoordFreq(self,nsteps):
          """Set the frequency of recording coordinate frames"""
          if (nsteps is None or nsteps <= 0):
                self.replace("dcdfile",None)
                self.replace("dcdfreq",0)
          else:
                self.replace("dcdfreq",nsteps)
                if (self.value("dcdfile") is None):
                      self.replace("dcdfile","coordinates")

    def setVelFreq(self,nsteps):
          """Set the frequency of recording velocity frames"""
          if (nsteps is None or nsteps <= 0):
                self.replace("veldcdfile",None)
                self.replace("veldcdfreq",0)
          else:
                self.replace("veldcdfreq",nsteps)
                if (self.value("veldcdfile") is None):
                      self.replace("veldcdfile","velocities")

    def coordFreq(self):
          """Return the frequency of outputing the coordinate frames"""
          return self.value("dcdfreq")
                                    
    def velFreq(self):
          """Return the frequency of outputing the velocity frames"""
          return self.value("veldcdfreq")
    
    def temperature(self):
        """Return the set temperature, or None if running NVE or no temperature
           has been set"""
        try:
            return self.value("langevintemp")
        except:
            return None
            
    def setTemperature(self,temp):
        """Set the temperature in kelvin"""
        if (temp is None):
              self.replace("langevinTemp",None)
              self.replace("langevin",False)
              self.replace("LangevinPistonTemp",None)
        else:
              self.replace("langevinTemp",temp)
              self.replace("langevin",True)    
              if (self.value("LangevinPiston")):
                    self.replace("LangevinPistonTemp",self.temperature())

    def setPressure(self,press):
          """Set the pressure to press atmospheres"""
          if (press is None):
                self.replace("LangevinPiston",False)
                self.replace("LangevinPistonTemp",None)
                self.replace("LangevinPistonTarget",None)
          else:
                self.replace("LangevinPiston",True)
                self.replace("LangevinPistonTemp",self.temperature())
                self.replace("LangevinPistonTarget", float(press) * 1.01325)
              
    def forwards(self):
        """Return the number of steps of dynamics this will perform"""
        return self._nforwards
        
    def setForwards(self,nsteps):
        """Set the number of steps of dynamics that will be performed"""
        if (nsteps is None or nsteps < 0):
            self._nforwards = 0
        else:
            self._nforwards = nsteps
        
    def backwards(self):
          """Return the number of steps of backwards dynamics that will be performed"""
          return self._nbackwards
    
    def setBackwards(self,nsteps):
          """Set the number of backwards steps that will be performed"""
          if (nsteps is None or nsteps < 0):
             self._nbackwards = 0
          else:
             self._nbackwards = nsteps

    def timestep(self):
        """Return the timestep in femtoseconds"""
        return self.value("timestep")
        
    def setTimestep(self,ts):
        """Set the timestep in femtoseconds"""
        self.replace("timestep", ts)
