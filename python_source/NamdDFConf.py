#################################################################
#
#     This class provides the digital filtering configuration
#     for a namd based digital filtering simulation.
#
#     This class is solely responsible for writing the 
#     digifilter command file. I recommend that if you 
#     want more control over the digital filtering, that
#     you create your own class that writes the command
#     file and that you use that class in preference
#     over this.
#
#     (C) Christopher Woods, June 2005
#
#

from File import *
from DFConf import *
from DFSim import *

class NamdDFConf(DFConf):
      def __init__(self):
            DFConf.__init__(self)
            self._code = "NAMD_25"
            
      def code(self):
            """Return the type of md code for which this class was designed"""
            return self._code
      
      def write(self,cmdfile,namdsim):
            """Write a digital filtering command file to 'cmdfile'"""
            
            #check that namdsim is compatible with this
            checkCompatibility( [self,namdsim] )
            
            #create the command file, forcing any existing version to be replaced
            cmdfile = File(cmdfile)
            cmdfile.create(replace=True)
            
            #construct the contents of the command file...
            contents = self.commandFileContents(namdsim)
            
            #write the contents to the file
            cmdfile.write(contents)
            
            #we are all done!
            return cmdfile
            
      def commandFileContents(self,namdsim):
            """Return a string containing the contents of the command file. Reimplement
               this function to gain complete control over the digital filtering"""
            
            m = self.nframes()/2 + 1
            d = self.delay()

            cmdfile = """
#turn on debugging output
param debug

#load up the system
read system %s

#load up the filter
read filter %s

#set the index of the central configuration
param centindex %d

#read in the starting coordinates and velocities - ascii output files from
#namd use velocity units of A ps-1
read pdb type=coord file=%s start=%d end=%d
read pdb type=vel file=%s start=%d end=%d
"""         % ( self.system(), self.filter(), m, \
                namdsim.inputCoords(), m-d, m-d, \
                namdsim.inputVels(), m-d, m-d)

            if (m-d-1 > 0):
                  cmdfile = """
%s
                  
#now read in the coordinate and velocity buffers from the backwards simulation
#note that binary files in namd write velocities in amber velocity units
read dcd type=coord file=%s start=%d end=%d
read dcd type=vel file=%s start=%d end=%d velscl=amber
"""         % (cmdfile, \
               namdsim.coordinates().toString() + "_reverse", m-d-1, 1, \
               namdsim.velocities().toString() + "_reverse", m-d-1, 1)

            cmdfile = """
%s

#now read in the coordinate and velocity buffers from the forwards simulation
#note that binary files in namd write velocities in amber velocity units
read dcd type=coord file=%s start=%d end=%d
read dcd type=vel file=%s start=%d end=%d velscl=amber

#calculate the kinetic energy before filtering
kineticnrg state=old markedonly

#apply the filter
"""                    % (cmdfile, \
                          namdsim.coordinates(), m-d+1, self.nframes(), \
                          namdsim.velocities(), m-d+1, self.nframes())

            if (self.tempcapFile().isNull()):
                  cmdfile += "filter vscale=%f fscale=%f tempcap=%f markedonly" % \
                              (self.vscale(), self.fscale(), self.tempcap())
            else:
                  cmdfile += "filter vscale=%f fscale=%f tempcap=%f markfile=%s" % \
                              (self.vscale(), self.fscale(), self.tempcap(),self.tempcapFile())

            if (self.hmcMode()):
                  cmdfile = """
%s

filtvel
            """ % (cmdfile)
            
            cmdfile = """
%s                         

#calculate the kinetic energy after filtering
kineticnrg state=new markedonly

#write the output coordinate and velocity files
write pdb type=coord file=%s
write pdb type=vel file=%s

#strip the forwards coordinate file down so that only the frames between the start
#of the trajectory and the end of the 'delay' region remain
strip dcd in=%s out=%s start=%d end=%d delta=%d

#finally remove the forwards buffer files...
remove %s
remove %s
                     """ % (cmdfile, \
                            namdsim.outputCoords(), namdsim.outputVels(), \
                            namdsim.coordinates(), namdsim.coordinates().toString() + "_strip", \
                            int(1), int(d), int(namdsim.coordFreq()), \
                            namdsim.coordinates(), namdsim.velocities() )

            if (m-d-1 > 0):
                  cmdfile = """
%s 
                  
remove %s
remove %s
            """ % (cmdfile, \
                   namdsim.coordinates().toString() + "_reverse", \
                   namdsim.velocities().toString() + "_reverse")
            if (self.remove()):
                  cmdfile = """
%s

remove %s
remove %s
remove %s
remove %s
remove %s
remove %s
remove %s
remove %s
remove %s
remove %s
          """ % (cmdfile, \
                  namdsim.coordinates().toString() + "_strip", "dfmd.cmd", \
                  "dfmd.log", "output.log.txt", namdsim.inputCoords(), \
                  namdsim.inputVels(), namdsim.inputXSC(), \
                  namdsim.inputVels().toString() + "_reverse", \
                  "cmdfile_backwards.namd", "output.txt_reverse")
                  
                  if (m-d-1 > 0):
                          cmdfile = """
%s

remove %s
                  """ % (cmdfile, "cmdfile_forwards.namd") 
                                     
            namdsim.switchBackwards()
            cmdfile = """
%s

remove %s
remove %s
remove %s    
            """ %(cmdfile, \
                  namdsim.outputVels(), namdsim.outputCoords(), \
                  namdsim.outputXSC())
         
            namdsim.switchForwards()
            return cmdfile
                  
