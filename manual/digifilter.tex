
\chapter{digifilter}\label{chp:digifilter}

\dftr is a program that can perform digital filtering. This manual will assume that you know what digital filtering is; if not, then there are many descriptions of digital filtering available.\cite{rdfmd1,apwthesis,dfmd1,scpthesis}

To perform digital filtering you need to supply;
\begin{enumerate}
\item A pdb containing all of the atoms in the system, identifying the atom types and which atoms are to be filtered. This is known as the \tpg{system} file, and its creation is described in appendix \ref{chp:marker}.
\item A filter file, containing all of the coefficients for the digital filter. The format and instructions for creating this file are given in appendix \ref{chp:filter}.
\item Trajectory files, containing the coordinates and velocities of all of the atoms in the system over a trajectory. Multiple file formats are supported, including DCD, CRD and PDB.
\item A \dftr command file. \dftr has a basic scripting interface, and this file contains the instructions to control the actual process of digital filtering. The bulk of this chapter describes the format of this command file.
\end{enumerate}

\section{Concepts}
\dftr is built around a few, simple concepts;
\begin{description}
\item[system] The system is the collection of atoms that comprise the system that is being simulated. The atoms in the system are specified using a specially modified PDB file (see appendix \ref{chp:marker}) that contains information as to what the element type of each atom is, and which atoms are marked.
\item[marked atoms] There are two types of atoms in \dftr. Marked atoms and unmarked atoms. Marked atoms are those whose coordinates and velocities may be stored in the coordinate and velocity buffers, and hence are the only atoms to which digital filtering may be applied. There are many ways to mark and unmark atoms, and you can change the atom marking throughout an application of \dftr.
\item[buffer] \dftr has two buffers; a coordinate buffer, that stores multiple coordinate frames of a trajectory, and a velocity buffer, that stores multiple velocity frames. Normally both the coordinate and velocity frames should be in sync (e.g. frame 4 of the coordinate buffer should have the corresponding velocities in frame 4 of the velocity buffer). Note that only the coordinates and velocities of \emph{marked} atoms are stored in the coordinate or velocity buffers.
\item[system frame] The buffers only contain the coordinates and velocities of the marked atoms. The system frame contains the coordinates and velocities of all of the system atoms. The application of digital filtering will overwrite the velocities of the marked atoms in the system frame with the output, filtered velocities. The resulting system frame can then be written out to disk.
\item[filter] The filter is the collection of coefficients that make up the digital filter. The coordinate and velocity buffers must contain the same number of frames as there are coefficients in the filter, or the program will exit with a warning.
\end{description}

\section{Command File}
\dftr is a powerful program with a simple scripting interface. This interface allows \dftr to support many different molecular dynamics programs and file formats, and to give you complete control over how the digital filtering is performed. The command file format is straightforward, with only a few simple rules;
\begin{enumerate}
\item Each command is on a separate line.
\item You can use as much whitespace (spaces and returns) as you like.
\item Comments can appear anywhere on a line, and start with a `\#'.
\item The capitalisation of commands is not significant. Only the capitalisation of filenames is important.
\end{enumerate}

\subsection{Commands}
The command file is based on a few core commands;
\begin{enumerate}
\item \tpg{param}: Sets the values of parameters or flags (section \ref{sec:param}).
\item \tpg{read}: Controls the reading of input files and trajectory files (section \ref{sec:read}).
\item \tpg{write}: Controls the writing of output files and trajectory files (section \ref{sec:write}).
\item \tpg{stitch}: Used to stitch (join) together two trajectory files to make a single file (section \ref{sec:stitch}). A more powerful program to stitch together multiple trajectory files is described in appendix \ref{chp:stitch}.
\item \tpg{strip}: Used to control the stripping down of trajectory files to make them smaller (section \ref{sec:strip}).
\item \tpg{remove}: Used to remove files (section \ref{sec:remove}).
\item \tpg{mark}: Used to mark atoms (e.g. for filtering) (section \ref{sec:mark}).
\item \tpg{clearbuffer}: Used to clear the buffers (section \ref{sec:clearbuffer}).
\item \tpg{avgbuffer}: Used to average the velocity buffer, e.g. if leapfrog was used (section \ref{sec:avgbuffer}).
\item \tpg{kineticnrg}: Used to calculate the kinetic energy and temperature of the system (section \ref{sec:kineticnrg}).
\item \tpg{removetr}: Used to remove the translational and rotational components of the velocity from the system (section \ref{sec:removetr}).
\item \tpg{removenontr}: Used to remove everything except the translational and rotational components of the velocity from the system (section \ref{sec:removenontr}).
\item \tpg{filter}: Actually perform the digital filtering (section \ref{sec:dfilter}).
\item \tpg{viewfilter}: This command is used to view the frequency response of the currently loaded filter, and is described in appendix \ref{chp:filter}, section \ref{sec:viewfilter}.
\end{enumerate}

\subsection{param}\label{sec:param}
The \tpg{param} command is used to set the value of parameters.

\cmd{param debug}
This turns on the debugging flag. This means that more information will be printed so that it is easier to see what is happening (and what is going wrong!)

\cmd{param debugoff}
This turns off the debugging flag.

\cmd{param centindex \emph{integer}}
This sets the frame in the buffer that is considered as the `central' configuration. If the buffer is filled with 2m+1 frames, then this normally should be the mth frame. If this parameter is not set, then the mth frame is used by default. When reading in the buffers, the coordinates and velocites of all of the atoms in the `central' frame will be automatically copied to the system frame.

\cmd{param ndof \emph{sysndof} \emph{markedndof}}
This sets the number of degrees of freedom in the system to \emph{sysndof}, and in the marked atom region to \emph{markedndof}. The number of degrees of freedom are used when estimating the temperature of the system. If this parameter is not set then the number of degrees of freedom is estimated.

\cmd{param shake}
This sets the flag to assume SHAKE on hydrogens was used. This information is only used when the code estimates the number of degrees of freedom in the system. This option has no effect if the \tpg{ndof} option has been set.

\subsection{read}\label{sec:read}
The \tpg{read} command is used to read in input files, including the system file, filter file, and coordinate and velocity trajectories.

\cmd{read dcd type=\emph{TYPE} start=\emph{strt} end=\emph{end} velscl=\emph{scl} file=\emph{filename}}
This command is used to read in binary DCD files. There are several options that control how this file is read in;
\begin{description}
\item[type] This specifies the type of data that is held in the trajectory file. The possible types are \tpg{coord} for coordinate files and \tpg{vel} for velocity files.
\item[start] This specifies the starting frame that this trajectory will be read into.
\item[end] This specifies the end frame that this trajectory will be read into. The trajectory will fill the buffer from frame \emph{strt} to frame \emph{end}. If \emph{strt} is greater than \emph{end} then the trajectory will be read in backwards, and if it is a velocity trajectory, the velocities will be reversed. Only the first \begin{tt}abs(\emph{end}-\emph{strt})\end{tt} frames of the trajectory will be read from the file, and there will be an error if the file does not contain sufficient frames.
\item[velscl] If a velocity file is read in, then this option may be used to set a scaling factor to scale the velocities from the units of the file to the internal units of \dftr (\angstrom ps$^{-1}$). This option also recognises two special keywords, \tpg{velscl=amber} or \tpg{velscl=charmm}, which will set the scale factor to the appropriate value to scale from AMBER or CHARMM velocity units to \dftr units. Both AMBER and CHARMM use the same scale factor as they both output velocities using AKMA units. NAMD outputs velocities in AKMA units in binary output files, but uses \angstrom ps$^{-1}$ for text files.
\item[file] This sets the filename of the file containing the trajectory.
\end{description}
As an example, the command;

\tpg{read dcd type=vel start=50 end=100 velscl=amber file=vels.dcd} 

will read in a binary DCD velocity trajectory file, vels.dcd, and use the information within it to fill frames 50 to 100 of the buffer (inclusive). The velocities are read using AMBER AKMA units.

\cmd{read crd type=\emph{TYPE} start=\emph{strt} end=\emph{end} velscl=\emph{scl} file=\emph{filename}}
This command is used to read in an AMBER CRD file trajectory. The options have the same meaning as for the \tpg{read dcd} command.

\cmd{read amber type=\emph{TYPE} start=\emph{strt} end=\emph{end} velscl=\emph{scl} file=\emph{filename}}
This command is used to read in the system frame from an AMBER restart file. The options for this command have slightly different meanings compared to the previous \tpg{read} commands;
\begin{description}
\item[type] As well as \tpg{coord} and \tpg{vel}, the type can also be \tpg{both}, which reads in both the coordinates and velocities from the restart file, and \tpg{avgvels}, which reads in the velocities in the file, and averages them with the velocities that are already present in the system frame. This is useful if the leap frog integrator was used, where the velocities for time step t are the average of the velocities at t-0.5 and t+0.5.
\item[start and end] The AMBER restart file is read into the system frame. However, the optional \tpg{start} and \tpg{end} options may be used to specify that the coordinates and/or velocities for the marked atoms should be copied into frames \emph{strt} to \emph{end} inclusive of the buffer. This is useful to copy the initial coordinates and velocities of the system into the buffer.
\end{description}
The other options to this command behave in the same way as for the other \tpg{read} commands.

\cmd{read pdb type=\emph{TYPE} start=\emph{strt} end=\emph{end} velscl=\emph{scl} file=\emph{filename}}
This command reads in the system frame from the supplied PDB file. \tpg{type} can take values \tpg{coord}, \tpg{vel} or \tpg{avgvels}. The other options are identical to those for \tpg{read amber}.

\cmd{read system \emph{filename}}
This command tells \dftr to read in the system from the system PDB file \emph{filename}. This is a specially modified PDB file that contains all of the atoms in the system (in the same order as the atoms in the trajectory files), and that identifies the element type of each atom, and whether each atom is marked. The format and method of creating this file is described in appendix \ref{chp:marker}.

\cmd{read filter \emph{filename}}
This command tells \dftr to read in the filter file containing all of the filter coefficients. The number of coefficients in the filter should be equal to the number of frames in the coordinate and velocity buffers. The format and method of creating the filter file is described in appendix \ref{chp:filter}.

\cmd{read nuke \emph{filename}}
This command tells \dftr to read in a \tpg{nuke} command file. This allows \dftr to act in \tpg{nuke} compatibility mode. This is useful to test that \dftr is working and gives the same results as \tpg{nuke}. This option should not be used by new users, as it is really only here for use by the developers to debug \dftr.

\subsection{write}\label{sec:write}
The \tpg{write} command is used to tell \dftr to write out either the system frame coordinates or velocities, or to write out the buffer coordinates or velocities.

\cmd{write pdb type=\emph{TYPE} velscl=\emph{scl} file=\emph{filename} markedonly}
This command is used to tell \dftr to write out the system frame coordinates or velocities to a file. There are several options that control how the information is written;
\begin{description}
\item[type] The \tpg{type} can either be \tpg{coord}, to tell \dftr to write out the system frame coordinates, or \tpg{vel} to write out the system frame velocities.
\item[velscl] This sets the scaling factor with which to scale the velocities when they are written out. This is to convert the velocities from the units used by \dftr (\angstrom ps$^{-1}$) to the units used by the molecular dynamics program. Two special options, \tpg{velscl=amber} and \tpg{velscl=charmm} both convert the velocities into AKMA units, used by the AMBER and CHARMM programs, and by NAMD for its binary files.
\item[file] This sets the filename of the file to which the coordinates or velocities will be written. If this file already exists then it will be overwritten.
\item[markedonly] If \tpg{markedonly} is present on this line, then only the coordinates or velocities of marked atoms will be written to the file. Otherwise the coordinates or velocities of all system atoms will be written to the file.
\end{description}

\cmd{write pdb file=\emph{filename} start=\emph{strt} end=\emph{end} delta=\emph{d}}
This is another version of the \tpg{write pdb} command, which will instruct \dftr to write out the coordinate or velocity buffers to a set of files, which all start with the name \emph{filename}. This command can also have the same options as the normal \tpg{write pdb} command.

\cmd{write raw type=\emph{TYPE} velscl=\emph{scl} file=\emph{filename} markedonly}
This command is identical to \tpg{write pdb} except that it writes a PDB style file with the coordinates and velocites reported to the highest precision available to the program. Use this option if you want velocities or coordinates to more than three decimal places.

\cmd{write amber velscl=\emph{scl} file=\emph{filename} markedonly}
This command tells \dftr to write the system frame coordinates and velocities to an AMBER restart file. The options to this command are identical to those of \tpg{write pdb} except that this command ignores the \tpg{type} option (as both the velocties and coordinates are always written to the AMBER restart file.

\cmd{write dcd type=\emph{TYPE} velscl=\emph{scl} file=\emph{fle} markedonly start=\emph{s} end=\emph{e} delta=\emph{d}}
This command tells \dftr to write out some or all of the velocity or coordinate buffers. The options \tpg{type}, \tpg{velscl}, \tpg{file} and \tpg{markedonly} have the same meanings as for \tpg{write pdb}. The additional options, \tpg{start}, \tpg{end} and \tpg{delta} control how much of the buffer to write out, as every \emph{d} frames from \emph{s} to \emph{e} will be written.

\cmd{write crd type=\emph{TYPE} velscl=\emph{scl} file=\emph{fle} markedonly start=\emph{s} end=\emph{e} delta=\emph{d}}
This command is identical to \tpg{write dcd}, except that the buffer is written to a CRD format file.

\subsection{stitch}\label{sec:stitch}
The \tpg{stitch} command is used to join two trajectory files together to form a single file. This is useful to join together the forwards and reverse trajectory files when filtering, or to join together the trajectories from multiple filter applications, or multiple phases of the simulation. The \tpg{stitch} command works by writing the output trajectory files while it is reading in the input files, so it is very fast, and has a low memory requirement regardless of the size of the input trajectories. 

\cmd{stitch dcd file1=\emph{file1} file2=\emph{file2} out=\emph{outfile} delta=\emph{d}}
This command joins together the DCD trajectory files \emph{file1} and \emph{file2}, writing the output to the newly created output file \emph{outfile}. \emph{file1} will appear first in the output file, and every \emph{d} frames of the joined trajectories will be written. If \emph{outfile} already exists then it will be overwritten. Note that this command does nothing to \emph{file1} or \emph{file2} so you will need to remove them yourself (using the \tpg{remove} command) if you wish to conserve disk space. Also note that it is possible for \emph{file1} and \emph{file2} to be the same file, thus creating an output that is \emph{file1} doubled! Finally, note that \emph{file1} and \emph{file2} must contain the same number of atoms, and really should be trajectories from the same system!

\cmd{stitch crd file1=\emph{file1} file2=\emph{file2} out=\emph{outfile} delta=\emph{d}}
This command is identical to \tpg{stitch dcd}, except that it is used to stitch together CRD files.

The stitch command is only able to stitch together pairs of trajectory files. Appendix \ref{chp:stitch} describes a program that comes with \dftr that is able to stitch together multiple trajectory files.

\subsection{strip}\label{sec:strip}
The \tpg{strip} command is used to strip a trajectory file down to make it smaller. While the same procedure could be obtained by reading a trajectory file into a buffer, then writing only parts of the buffer to an output file, this would require large amounts of memory and could be slow. The strip command works by stripping and writing the output trajectory while it is reading the input trajectory. This results in a faster procedure that does not have a large memory requirement. In addition, the \tpg{strip} command can work with arbitrarily large trajectory files without running out of memory.

The strip command is very useful, as digital filtering requires that the coordinates and velocities of all atoms of the system are recorded for every frame of the trajectory. This wastes a lot of disk space, which can be recovered by using the \tpg{strip} command to prune down these large input trajectory files into something more manageable.

\cmd{strip dcd in=\emph{infile} out=\emph{outfile} start=\emph{s} end=\emph{e} delta=\emph{d}}
This command writes every \emph{d} frames from frames \emph{s} to \emph{e} of the trajectory contained in \emph{infile} to the output file \emph{outfile}. Note that \emph{outfile} will be overwritten if it already exists. Note also that it is a mistake for \emph{infile} and \emph{outfile} to be the same. You will need to use the \tpg{remove} command to remove \emph{infile} once you have finished stripping it.

\cmd{strip crd in=\emph{infile} out=\emph{outfile} start=\emph{s} end=\emph{e} delta=\emph{d}}
This command is identical to \tpg{strip dcd} except this works on CRD files. The options behave in an identical manner to \tpg{strip dcd}.

\subsection{remove}\label{sec:remove}
The \tpg{remove} command is used to remove files. This is useful if you are stripping trajectory files and wish to remove the originals, or if you wish to clean up after the application of the filter.

\cmd{remove \emph{filename}}
This command removes the file \emph{filename}. Take care as once the file has gone, you will not be able to get it back!

\subsection{mark}\label{sec:mark}
This command is used to change the marking of atoms while \dftr is running. Note that this only changes the record of which atoms are marked - it does not change the contents of the coordinate or velocity buffers. You should clear the coordinate and velocity buffers before you change the marking of atoms. 

Changing the marking of atoms is useful for many reasons;
\begin{enumerate}
\item You can select different parts of the system to digitally filter. You could filter one part with one filter / filter-scale, then clear the buffers, change the marking, then filter the new part with a different filter or filter-scale.
\item You can select different parts of the system of which to calculate the kinetic energy or temperature.
\item You can select different parts of the system to output to PDB or trajectory files.
\end{enumerate}

The command to mark atoms is very flexible, with atoms identified from the system PDB file based on their PDB atom number.

\cmd{mark all}
Mark all atoms in the system.

\cmd{mark none}
Unmark all of the atoms in the system (thus no atoms are marked).

\cmd{mark 1-3 6 7-15 5 18}
Mark atoms with PDB atom numbers 1 to 3, 6, 7 to 15, 5 and 18. Note that if multiple atoms have the same PDB atom number, then all atoms with that number are marked. This could be useful to distinguish between solute and solvent, e.g. all oxygens in a water solvent could have atom number 50. Thus marking atom number 50 would mark all solvent oxygen atoms.

\subsection{clearbuffer}\label{sec:clearbuffer}
This command is used to clear the coordinate and velocity buffers. This is necessary if the list of marked atoms is changed using the \tpg{mark} command.

\cmd{clearbuffer}
This command clears the coordinate and velocity buffers. There are no options.

\subsection{avgbuffer}\label{sec:avgbuffer}
This command is used to average the velocities within the velocity buffer. This is useful if the leapfrog integrator was used, as averaging the velocities is required to bring them into synch with the coordinates.

\cmd{avgbuffer}
This command averages the velocity buffer. The velocities at frame i are set equal to the average of the velocities at frame i and i+1. The number of velocity frames in the buffer is thus reduced by one.

\subsection{kineticnrg}\label{sec:kineticnrg}
This command is used to calculate the kinetic energy and temperature of the system. This is useful to provide output that may be used by a calling script or program, e.g. so that the script can implement a variable filter scale to prevent the breaching of the temperature cap.

\cmd{kineticnrg state=\emph{TYPE} markedonly}
This calculates the kinetic energy and temperature of the system. If \tpg{markedonly} is on the line, then the kinetic energy and temperature of only the marked atoms is calculated. If \emph{TYPE} is \tpg{old}, then the calculated kinetic energy and temperature are saved as the `old' values. If \emph{TYPE} is \tpg{new}, then the difference between the calculated values and the `old' values is calculated and also printed. The kinetic energies are output in both \kcalmol and \kjmol. The temperatures are output in kelvin.

\subsection{removetr}\label{sec:removetr}
This command is used to remove the translational and rotational components of the velocity from the system configuration. The translational and rotational components are automatically removed from the buffer velocities, but this command provides the option of doing the same to the system velocities.

\cmd{removetr markedonly}
This command removes the translational and rotational components of the velocity from the system configuration. If \tpg{markedonly} is on the line, then only the velocities of the marked atoms are affected - otherwise the velocities of all of the atoms in the system are affected.

\subsection{removenontr}\label{sec:removenontr}
This command is very similar to \tpg{removetr}, except that is removes all components from the system configuration velocities, except for the translational and rotational components.

\cmd{removenontr markedonly}
This command removes everything except for the translational and rotational velocities from the system configuration velocities. If \tpg{markedonly} is on this line, then only the marked atoms are affected - otherwise all atoms are affected.

\subsection{filter}\label{sec:dfilter}
This is the most important command in the entire program as it is the one that initiates and controls the actual digital filtering! To run this command you must have loaded up the system PDB, loaded a filter file, and have loaded up the coordinate and velocity buffers with the same number of frames as their are coefficients in the filter. If you try to use this command without first doing these things, then an error will be printed and \dftr will exit.

The way that digital filtering works is that the coordinates in the coordinate buffer are used with the atomic masses in the system frame to calculate and remove the translational and rotational components of the velocity from each frame (frame 1 to $n_{frame}$) of the velocity buffer. The velocity in each frame in the velocity buffer, $v_{buf}$, is then multiplied by its associated filter coefficient, $c$, and then summed to yield the filtered velocity, $v_{filt}$,
\begin{equation}
v_{filt} = \sum_{i=1}^{n_{frame}} c(i) \times v_{buf}(i).
\end{equation}
The resulting filtered velocities are then added onto the system frame velocities,$v_{sys}$, to yield new system frame velocities, $v_{sys}\prime$,
\begin{equation}
v_{sys}\prime = (vscl \times v_{sys}) + (fscl \times v_{filt}).
\end{equation}
There are two scaling factors in this equation; the filter scale, $fscl$ is used to scale the filtered velocities to enhance or reduce the impact of the digital filtering. The velocity scale, $vscl$, is used to scale the original velocities, affecting how much of the original velocities are present in the output. In \tpg{nuke}, the velocity scale was fixed at $1.0$, with the only control being over the filter scale. The application of the filter used a terminology such that if the filter scale was equal to $3.0$, then the filter was said to be a $3+1$ filter ($1$ for the value of the velocity scale). Continuing this terminology, if in \dftr the velocity scale is set to $0.5$, and the filter scale is set to $2.0$, then we will say that this corresponds to a $2+0.5$ filter. Note that you can set the velocity scale to zero, thus the output will only consist of the filtered velocities (and will thus be a `+0' filter). 

Note that only the velocities of marked atoms will be affected by the digital filtering, even if the velocity scale is set to zero. The velocities of unmarked atoms in the system frame are not changed at all by digital filtering.

\cmd{filter vscale=\emph{vscl} fscale=\emph{fscl} tempcap=\emph{tempk} markedonly}
This command initiates digital filtering of the marked atoms, using a velocity scale of \emph{vscl} (default $1.0$), and a filter scale of \emph{fscl}. If the \tpg{tempcap} option is supplied, then a temperature cap of \emph{tempk} kelvin is applied. This means that if the temperature of the system exceeds this temperature, then an error will be printed, and the velocities reset to their pre-filter values. If \tpg{markedonly} is on the line, then the temperature cap test is applied only to the marked atoms - otherwise the temperature cap test is applied to the entire system.

\cmd{filter vscale=\emph{vscl} fscale=\emph{fscl} tempcap=\emph{tempk} markfile=\emph{markfile}}
This command initiates digital filtering of the marked atoms, using the same parameters as the previous command. However, in this version, a temperature cap is applied to the atoms that are marked in the file \emph{markfile}. This file has the same format as the system PDB file, and is useful if you wish to apply the temperature cap to a subset of atoms that is different to the filtered atoms, e.g. you wish the temperature cap to be applied to the solvent only, or to the protein backbone atoms only.

\section{Example}
Figure \ref{fig:dfter-example} shows an example \dftr command file. This command file uses \dftr to filter the velocities from an RDFMD simulation, filling part of the buffer from a `forwards' simulation, part of the buffer from a `backwards' simulation, and one frame of the buffer from the starting configuration for both of these simulations.
\begin{figure}
\begin{boxedverbatim}
#turn on debugging output
param debug


#assume that shake on hydrogens was used when calculating the system 
#temperature
param shake


#load up the system
read system ../input/aladip-system.pdb


#load up the filter - contains 2001 coefficients
read filter ../input/filter.coeffs



#set the index of the central configuration to 1001
param centindex 1001



#read in the starting coordinates and velocities - ascii output
#files from namd use velocity units of A ps-1
read pdb type=coord file=inpcoords.pdb start=800 end=800
read pdb type=vel file=inpvels.pdb start=800 end=800



#now read in the coordinate and velocity buffers from the backwards sim
#note that binary files in namd write velocities in amber velocity units
read dcd type=coord file=back_coords.dcd start=1 end=799
read dcd type=vel file=back_vels.dcd start=1 end=799 velscl=amber



#now read in the coordinate and velocity buffers from the forwards sim
#note that binary files in namd write velocities in amber velocity units
read dcd type=coord file=forwd_coords.dcd start=801 end=2001
read dcd type=vel file=forwd_vels.dcd start=801 end=2001 velscl=amber



#calculate the kinetic energy before filtering
kineticnrg state=old markedonly



#apply the filter using a velocity scale of 0.5, a filter scale of 3.0
#and a temperature cap of 1200 K on the marked atom region only
filter vscale=0.5 fscale=3.0 tempcap=1200 markedonly



#calculate the kinetic energy after filtering
kineticnrg state=new markedonly



#write the output coordinate and velocity files
write pdb type=coord file=outcoords.pdb
write pdb type=vel file=outvels.pdb



#remove the backwards buffer files...
remove back_coords.dcd
remove back_vels.dcd



#strip the forwards coordinate file down so that only the frames between
#the start of the trajectory and the end of the delay region remain
strip dcd in=forwd_coords.dcd out=outcoords.dcd start=1 end=200 delta=10



#finally remove the forwards buffer files...
remove forwd_coords.dcd
remove forwd_vels.dcd
\end{boxedverbatim}
\caption
{
      An example command file to perform RDFMD.
}\label{fig:dfter-example}
\end{figure}

\section{Conclusion}
\dftr provides a powerful and capable program that can perform digital filtering on a wide range of different supplied input files. While this allows \dftr to be adapted and used in a wide range of different environments, it does not, on its own, allow \dftr to actually perform a digital filtering simulation. \rdfmd, discussed in the next chapter, is a python framework that uses \dftr as one part of a system that is designed to perform full, digital filtering simulations.
