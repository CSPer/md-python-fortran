
\chapter{rdfmd}\label{chp:rdfmd}

\rdfmd is a python module that provides a complete system for performing digital filtering simulations. \rdfmd uses \dftr, described in the previous chapter, to actually perform the digital filtering, and a specialisation of \rdfmd for NAMD is supplied so that NAMD may be used to perform the molecular dynamics. The design of \rdfmd is such that is should be straightforward to write specialised code to support other molecular dynamics programs.

\section{Concepts}
Before using \rdfmd, you should already be familiar with digital filtering and the ideas behind how it works. You should also be familiar with the \dftr program, and the format of its command file. \rdfmd stands for ``reversible digitally filtered molecular dynamics''. The name is a slight misnomer, because the process is not reversible. Rather, the digital filter is applied to a trajectory that is run both forwards from a starting point, and in reverse (backwards) from the same starting point. This is shown in figure \ref{fig:rdfmd}. 
%%
%%
\begin{figure}\centering
\includegraphics[width=5in]{filter}
\caption
{
A diagrammatic representation of digital filtering. 
}\label{fig:rdfmd}
\end{figure}
%%
%%
A filter consists of a set of coefficients (represented in figure \ref{fig:rdfmd} in yellow). This filter is associated with a corresponding filter buffer, which must be the same length as the filter (e.g. the number of frames in the buffer must equal the number of coefficients in the filter). The filter buffer is filled with coordinates and velocities from one or many molecular dynamics simulations. In RDFMD, the buffer is filled with a reverse simulation (shown in blue), and a forwards simulation (shown in purple). The starting configuration for both of these simulations must also be placed in the buffer, between these two simulations (shown in figure \ref{fig:rdfmd} in green). If the forwards and backwards simulations are of the same length, then the starting configuration will be directly in the centre of the buffer, otherwise the starting configuration will be shifted to the left or right of the centre. The difference between the frame number at the middle of the buffer and the frame number of the starting configuration is known as the \emph{filter delay}, $f_{delay}$, with a positive delay indicating a shift to the left, and a longer forwards simulation than a reverse simulation.

The coordinates of the atoms in each frame, together with the atomic masses, are then used to remove the translational and rotational components of the velocities in each frame of the buffer. The resulting velocites in each frame, $v_{buffer}$ are then multiplied by the corresponding filter coefficient, $c$, and summed to yield the filtered velocities, $v_{filt}$,
\begin{equation}
v_{filt} = \sum_{i=1}^{n_{frame}} c(i) \times v_{buf}(i).
\end{equation}
The filtered velocities are then added onto the velocities at the centre of the buffer, $v_{sys}$, to yield the output velocities,
\begin{equation}
v_{output} = (vscl \times v_{sys}) + (fscl \times v_{filt}).
\end{equation}
Here $vscl$ and $fscl$ are the velocity and filter scales, which are described in section \ref{sec:dfilter}. The coordinates from the central frame are taken as the output coordinates.

If the filter delay, $f_{delay}$, was zero, then the output coordinates would be equal to the input coordinates, as the input coordinates would be at the centre of the buffer. However, if the filter delay was positive, then the simulation will have progressed by $f_{delay}$ steps. This allows for multiple applications of the filter to be used, with $f_{delay}$ steps between each application. This is shown in figure \ref{fig:fdelay}.
%%
%%
\begin{figure}\centering
\includegraphics[width=5in]{fdelay}
\caption
{
A diagrammatic representation of a complete RDFMD filter application.
}\label{fig:fdelay}
\end{figure}
%%
%%
This multiple application of the filter is controlled by two parameters;
\begin{description}
\item[filter cap] The filter cap is the maximum number of applications of the filter.
\item[temperature cap] The application of the filter is likely to increase the system temperature. The temperature cap is the temperature at which the filter application cycle is aborted, e.g. the filter is applied multiple times until the system temperature exceeds 1200~K.
\end{description}
It is normal for an RDFMD simulation to apply both a temperature cap and a filter cap, e.g. the filter is repeatedly applied until either it has been applied 5 times, or the temperature has exceeded 800~K.

It is a requirement of RDFMD that the dynamics used to fill the buffers samples from the NVE ensemble. However, this can mean that the application of the filter can take the system energy far away from what would be expected at the desired simulation temperature. To correct for this, it is necessary to intersperse applications of the filter by periods of NVT or NPT normal molecular dynamics. This is shown in figure \ref{fig:fullsim}.
%%
%%
\begin{figure}\centering
\includegraphics[width=5in]{fullsim}
\caption
{
A diagrammatic representation of a complete RDFMD simulation, interspersing normal NVT or NPT molecular dynamics with applications of the digital filter.
}\label{fig:fullsim}
\end{figure}
%%
%%

\subsection{Requirements of an RDFMD software system}
The form of an RDFMD simulation places many requirements on the RDFMD software;
\begin{enumerate}
\item The software must provide controls to switch between NVE sampling required by the filter application, and NVT or NPT sampling required during the normal MD parts of the simulation.
\item The software needs to provide controls to allow both forwards and backwards trajectories to be generated, and to specify the lengths of both the forwards and backwards trajectories (and thus, implicitly, the value of the filter delay).
\item The software needs to control the actual digital filtering, including writing the \dftr command file, running the \dftr program and parsing the output to ensure that everything worked, and that the temperature cap was not breached.
\item The software needs to manage the files produced, with ideally each phase of the simulation occupying its own directory, and with good file control to prevent the running simulation from consuming too much disk space.
\item The software needs to control the MD package used to perform the molecular dynamics simulation, and such control should be sufficiently encapsulated to allow for multiple different MD packages to be used.
\item The software should be easy to understand and extensible, thus allowing continuing research into the best practice for RDFMD simulations.
\end{enumerate}

\section{The rdfmd module, an overview}
\rdfmd is a python module written to perform complete RDFMD simulations, and to satisfy the aforementioned requirements. The module is written to use the \dftr program to perform the digital filtering, and most of the module is written to be independent of the molecular dynamics program used to generate the trajectories. A specific implementation of the \rdfmd module for NAMD has been written, with the names of all NAMD specific classes starting with `NAMD'. All of the python files for \rdfmd and the NAMD specific implementation can be found in the \tpg{python\_source} directory of the distribution.

\subsection{Key classes}
\rdfmd is an object orientated module, built on six key classes;
\begin{description}
\item[DFSim] This class is the master that controls the RDFMD simulation. DFSim provides member functions that request either MD sampling (NVE, NVT or NPT), or a complete filter application. DFSim is a high-level controller class that does nothing itself, but rather coordinates and controls the following classes that actually do the work!
\item[NamdDriver] This class performs a molecular dynamics simulation. In this case this class is specialised to use NAMD, though you can write a new class modelled on NamdDriver that uses another MD package, e.g. thus creating SanderDriver or CharmmDriver.
\item[NamdSim] This class contains all of the information necessary to configure an MD simulation, and contains all of the simulation parameters and information necessary to link the output restart files from the previous iteration to the input files for the current iteration. An MD simulation is performed by passing a NamdSim to a NamdDriver. Like NamdDriver, this class is specialised for NAMD. You can use NamdSim as a model to write your own class that provides the same information and functionality for another MD package, e.g. creating SanderSim or CharmmSim.
\item[SimDir] This class represents the current simulation run directory. This class provides functionality to get the current directory, previous directory or next directory, and provides the code necessary to advance from one output directory to the next for each phase of the RDFMD simulation.
\item[DFDriver] Just as NAMDDriver drives the MD simulation, so DFDriver drives the \dftr program to actually perform the digital filtering. This class provides code to run the \dftr program, and then parse the output to check to see if the temperature cap had been breached.
\item[NamdDFConf] This class provides all of the information needed to control and configure an application of digital filtering. This class is responsible for writing the \dftr command file and for storing information such as the temperature cap and filter cap. Note that this class is specialised for use by the other NAMD classes - the reason for this is that the actual configuration of \dftr will depend on the names and formats of the output trajectory files, and thus must be specialised for the particular MD package used to generate them. You can use this class as a model to write your own DFConf classes, specialised for other MD packages, e.g. creating SanderDFConf or CharmmDFConf.
\item[NamdMonitor] This class is used to calculate the temperature of different regions of the system during filtering, so that the temperature can be monitored to detect any problems. This class is specialised for use with the other NAMD classes, though you could use it as a model to write your own Monitor classes, e.g. SanderMonitor or CharmmMonitor.
\end{description}

\subsection{Example}\label{sec:rdfmd-example}
Now that the basics of the module have been introduced, lets now look at an example using the module. The example we use is a simple simulation of gas-phase alanine dipeptide, located in the \tpg{test/aladip} directory. There are other examples in this directory, which are described in chapter \ref{chp:example}.

The input files for this example are in the \tpg{input} directory. This directory contains the system PDB file, \tpg{aladip-marked.pdb}, a digital filter file, \tpg{filter0\_300}, the NAMD command file containing the base configuration of this system, \tpg{aladip.namd}, and a directory containing all of the restart files, \tpg{equilinput}. This directory contains all of the files necessary to run a NAMD simulation on this system, e.g. parameter files, topology and structure files. The output restart files (\tpg{output.coor}, \tpg{output.vel} and \tpg{output.xsc}) are the ones that will be linked to the input files for the first stage of the simulation.

With all of the input files in place, there is also a directory containing symbolic links to all of the executables (\tpg{bindir}). Within this directory are symbolic links to the \tpg{namd2}, \tpg{charmrun} and \tpg{digifilter} exectuables. Such a directory is not necessary, but I find that it makes things easier to understand.

The only other file in this example is \tpg{runsim.py}. This is the python script that is used to actually perform the simulation. This file is shown in figure \ref{fig:wat-cmd}, with line numbers identifying each line.
%%
%%
\begin{figure}
\begin{boxedverbatim}
00: #!/bin/env python
01: 
02: import sys
03: import os
04: 
05: #set the path to the 'rdfmd' directory that contains the 
06: #rdfmd python module
07: sys.path.append("/home/chris/Work/WorkSoftware")
08: 
09: #import the rdfmd python module
10: from rdfmd import *
11: 
12: #set the base directory for the simulation and change into it
13: basedir = File("~/Work/WorkSoftware/rdfmd/test/aladip")
14: os.chdir( basedir.toString() )
15: 
16: #create the output directory - start counting at 0
17: simdir = SimDir("output","equil",0)
18: simdir.createRoot()
19: 
20: #now link the input files to the current output directory
21: linkFile("input/equilinput", simdir.current())
22: 
23: #create a digital filtering configuration for namd simulations
24: dfconf = NamdDFConf()
25: 
26: #set the digital filtering parameters
27: dfconf.setFilter("input/filter0_300", 2001)  # load a 2001 coeff filter
28: dfconf.setSystem("input/aladip-marked.pdb")  # this file sets the atom
29:                                 # types and which atoms will be filtered
30: 
31: dfconf.setFiltercap(5)        # set the filter cap
32: dfconf.setTempcap(2000.0)     # set the temperature cap
33: dfconf.setTempcapFile("input/aladip-cap.pdb")
34: dfconf.setDelay(250)          # set the filter delay
35: dfconf.setFScale(2.0)         # set the filter scale
36: dfconf.setVScale(0.2)         # set the velocity scale
37: 
38: #now create a digital filtering driver - pass it the location of the
39: #'digifilter' program that will actually be performing the filtering
40: dfdriver = DFDriver("bindir/digifilter")
41: 
42: #now create a simulation configuration for namd - load it up from the 
43: #template file 'alanin.namd'
44: simconf = NamdSim("input/aladip.namd")
45: 
46: simconf.setCoordFreq(10) # print out coordinates every 10 steps
47: simconf.setVelFreq(0)    # never print out velocities
48: 
49: #create a driver to run NAMD simulations - pass it the location of the
50: #namd2 binary that will be used to perform the MD
51: simdriver = NamdDriver("bindir/namd2", "bindir/charmrun")
52: 
53: #create a monitor to get the temperature during filtering
54: monitor = NamdMonitor("bindir/digifilter", "output/monitor.log")
55: monitor.addSystemPDB("filtered", "input/aladip-marked.pdb")
56: monitor.addSystemPDB("backbone", "input/aladip-cap.pdb")
57:
58: #finally, create the rdfmd simulation controller object
59: sim = DFSim()
60: 
61: #run some nvt equilibration at 298K
62: simdir = sim.nvtSim(simdir, simdriver, simconf, 1000, 298)
63: 
64: #perform some digital filtering
65: simdir = sim.rdfmdSim(simdir, simdriver, simconf, dfdriver, dfconf, monitor)
66: 
67: #do some more nvt
68: simdir = sim.nvtSim(simdir, simdriver, simconf, 1000, 298)
69: 
70: #some more digital filtering
71: simdir = sim.rdfmdSim(simdir, simdriver, simconf, dfdriver, dfconf, monitor)
72: 
73: #do some more nvt
74: simdir = sim.nvtSim(simdir, simdriver, simconf, 1000, 298)
75: 
76: #and on, and on, and on,  ...
\end{boxedverbatim}
\caption
{
The python script used to perform an RDFMD simulation on an alanine dipeptide example system.
}\label{fig:wat-cmd}
\end{figure}
%%
%%
Lines 00 to 10 should be the same in all \rdfmd scripts, with the exception that on line 07, you need to substitute the path to the \rdfmd source (e.g. if your \rdfmd package is in \tpg{/home/xan/software/rdfmd} then you need to write the path \tpg{/home/xan/software} on line 07).

Line 13 sets the base directory for the simulation, which contains the \tpg{input} and \tpg{bindir} directories. This will also need to be changed depending on where you have installed this example. 

Line 14 changes into the base directory. Line 17 is used to create the SimDir object that keeps track of the simulation. The output will be placed in the \tpg{output} directory, with the tag name \tpg{equil} and starting at directory 0. 

Line 18 uses the SimDir object to create the root output directory. Line 21 is then used to link the directory containing the equilibrated restart files (\tpg{indir/equilinput}) to the `current' directory of the SimDir (\tpg{output/00000\_equil}). 

Line 24 is used to create a NamdDFConf object. Lines 27 to 35 are used to set the parameters for the digital filtering. Line 27 sets the filter file to be used (a 2001 coefficient file that filters the 0 to 300~cm$-1$ region). Line 28 sets the system PDB file (in which all of the alanine dipeptide atoms are marked). Line 31 sets the filter cap to 5, line 32 sets the temperature cap to 2000~K. Line 33 sets the temperature cap file. This is a file that has the same format as the system PDB file, and marks the atoms that the temperature cap will be applied to (in this case, the non-hydrogen atoms). Line 34 sets the filter delay to 250, line 35 sets the filter scale to 2.0 and line 36 sets the velocity scale to 0.2.

Line 40 is used to create a DFDriver object, passing the location of the \dftr executable.

Line 44 sees the creation of a NamdSim object, based on the NAMD command file \tpg{input/aladip.namd}. Lines 46 and 47 see the frequency of recording coordinates to every 10 frames, and of recording velocities to 0 (e.g. velocities are not saved).

Line 51 is used to create a NamdDriver, which is passed the location of the \tpg{namd2} and \tpg{charmrun} executables. Lines 54 to 56 create a NamdMonitor, used to calculate the temperature during the digital filtering applications, and give the monitor two system PDB files; \tpg{aladip-marked.pdb} and \tpg{aladip-cap.pdb}. The temperature of the atoms marked in these two files will be calculated and printed out during the application of the filter, and will also be recorded in the log file, \tpg{output/monitor.log}, which is specified on line 54.

Line 59 sees the final bit of setup, the creation of the DFSim object that controls the whole simulation.

On line 62, 1000 steps of NVT equilibration is requested at a temperature of 298~K. This is followed on line 65 by an application of the filter specified in the previous lines. Line 68 sees some more NVT equilibration, followed by line 71 with another filter application, then line 74 with some more NVT. Note that this could have been placed in a loop, e.g.

\begin{verbatim}
for i in range(0,5):
    simdir = sim.nvtSim(simdir, simdriver, simconf, 1000, 298)
    simdir = sim.rdfmdSim(simdir, simdriver, simconf, dfdriver, dfconf, monitor)
\end{verbatim}

It should be clear from this example that the \rdfmd module is very flexible, and that many different types of filtering simulations may be performed.

Running this example should result in the creation of the top-level output directory, \tpg{output}. Within this should be the link back to the input files, \tpg{00000\_equil}, the directory containing the first NVT simulation, \tpg{00001\_nvt}, and the individual stages of the filter (\tpg{00002\_rdfmd\_1}, \tpg{00003\_rdfmd\_2} etc.). Depending on whether the temperature cap was breached you may have many or a few filter directories. These will be followed by another NVT output directory (in my case \tpg{00007\_nvt}), then more filter applications, then another NVT output directory.

Within the output directories you should find the output files from NAMD, and also the log files from \tpg{namd2} (\tpg{output.txt.bz2}), and in the filter directories, the log file from \dftr (\tpg{dfmd.log.bz2}). It is important that you check these files to make sure that there were no problems. Also, these log files should be your first port of call when investigating what happened if something goes wrong with the running of this script.

Play with this example, and try to learn how the script and \rdfmd module works. Then play with the other examples in the \tpg{test} directory, which are described in chapter \ref{chp:example}. The rest of this chapter will go on to describe the \rdfmd module in more detail. This detail is not necessary for the casual user of this software, but will provide you with more information about how to extend or modify the software, and to better diagnose problems if things go wrong.

\section{The rdfmd module, the specifics}
Now that you are familiar with the overview of how the \rdfmd module works, it is now time to get down and dirty looking at its internals. Each class will now be presented, together with its member functions, and a description of how it works.

\subsection{DFSim}
DFSim is the main controller of the RDFMD simulation, and is an excellent place to start if you want to understand how the \rdfmd module works. DFSim does no work itself, rather it is a controller class that instructs all of the other classes to do their work. DFSim is a very simple class with only a few member functions;

The DFSim constructor takes no arguments. A new DFSim is thus constructed via;

\cmd{dfsim = DFSim()}

DFSim contains member functions to perform simulations in the NVE, NVT and NPT ensembles, as well as to perform an application of digital filtering.

\cmd{simdir = dfsim.nptSim(simdir, driver, sim, nsteps, temperature=298, pressure=1)}
This function performs \tpg{nsteps} of NPT molecular dynamics, using the MD driver \tpg{driver} and MD configuration \tpg{sim}, at \tpg{temperature} kelvin, and \tpg{pressure} atmospheres pressure. The trajectory is generated in the next directory of \tpg{simdir}, which is incremented and returned by this function. The input files come from the `current' directory of \tpg{simdir}, and exceptions will be thrown if any of the input files are missing. Note that \tpg{driver} and \tpg{sim} need to be compatible, e.g. NamdDriver and NamdSim, or if you write them, say SanderDriver and SanderSim.

\cmd{simdir = dfsim.nvtSim(simdir, driver, sim, nsteps, temperature=298)}
This function performs \tpg{nsteps} of NVT molecular dynamics at \tpg{temperature} kelvin. This function is otherwise identical to \tpg{dfsim.nptSim()}.

\cmd{simdir = dfsim.nveSim(simdir, driver, sim, nsteps)}
This function performs \tpg{nsteps} of NVE molecular dynamics. This function is otherwise identical to \tpg{dfsim.nptSim()}.

The above functions provide a convienient interface to run and control normal MD simulations. DFSim provides just one, powerful function to perform a digital filter application.

\cmd{simdir = dfsim.rdfmdSim(simdir, driver, sim, dfdriver, dfconf, monitor=None)}
This class performs a digital filter application using the MD driver \tpg{driver}, MD simulation configuration \tpg{sim}, using the DFDriver \tpg{dfdriver} and digital filter configuration \tpg{dfconf}. Depending on the filter cap and temperature cap specified in \tpg{dfconf}, multiple filter buffers could be filled, and \dftr could be run multiple times. Each buffer and application of \dftr occurs in its own output directory, with \tpg{simdir} incremented each time. The value of \tpg{simdir} at the end of the filter application is returned. Note that \tpg{driver}, \tpg{sim} and \tpg{dfdriver} need to be compatible with one another, e.g. use NamdDriver, NamdSim and NamdDFConf together, or if you have written them, SanderDriver, SanderSim and SanderDFConf.

The optional parameter, \tpg{monitor}, can be used to provide a monitor object that will calculate the temperature and kinetic energy of the system during filtering. \tpg{monitor} must be compatible with \tpg{sim}, e.g. you must use NamdMonitor with NamdSim, or, if you have written them, SanderMonitor with SanderSim.

One thing to note is that DFSim will modify the simulation parameters so that options such as SHAKE or options that remove translational or rotational momentum are switched off. The reason for this is because the velocities that are loaded into the simulation must be the ones that are actually used, and the simulation code must not pre-process them. This means that when using NAMD, the option \tpg{COMmotion} is set to \tpg{yes} and the option \tpg{rigidbonds} is set to \tpg{none}. The original simulation parameters are restored when this function exits, so that the normal NVT or NPT simulations may use SHAKE, and may also remove translational or rotational momentum.

\subsection{NamdDriver}
NamdDriver provides a specialised class that can generate a molecular dynamics trajectory using the \tpg{namd2.5} program. Note that you can write your own class to use any other MD program, and as long as it has the same interface (constructor and member functions) as NamdDriver, then it should work with DFSim and the rest of the \rdfmd module.

\cmd{driver = NamdDriver(namd2="/path/to/namd2", charmrun="/path/to/charmrun")}
Construct a NamdDriver object, passing the locations of the \tpg{namd2} and \tpg{charmrun} executables.

\cmd{driver.code()}
Return the name of the code that this class has been written to use (in this case \tpg{NAMD\_25} is returned). This code is checked to ensure that specialised classes are compatible with one another.

\cmd{driver.setNodeFile("/path/to/nodefile", nnodes=1)}
If you wish to run parallel MD, then use this function to tell the class where the nodefile is located, and how many nodes to use in parallel.

\cmd{driver.run(simdir, namdsim)}
Generate an MD trajectory in the `current' output directory of \tpg{simdir}, using the NAMD configuration specified in \tpg{namdsim}. Note that the \tpg{namdsim} object may request both forwards and backwards dynamics to be performed, and if so requested, this function will perform both the forwards and backwards dynamics.

\subsection{NamdSim}
NamdSim provides a specialised class that contains the configuration for a NAMD molecular dynamics simulation. NamdSim is closely coupled to NamdDriver and NamdDFConf, though as long as you match the interface to this class, you should be able to write specialised classes for other MD packages (e.g. creating SanderSim or CharmmSim).

\cmd{sim = NamdSim(namdfile=None)}
Create a new NamdSim object, optionally passing in a NAMD command file that provides the base configuration of the simulation. Note that the NamdSim object will not read nor understand any TCL commands that are in the NAMD command file, so you will need to limit yourself to using the basic NAMD configuration options.

\cmd{sim.code()}
Identical to \tpg{NamdDriver.code()}, with the output of this used to test compatibility with the driver.

\cmd{sim.readConfig("/path/to/command/file")}
Set the base configuration to that contained in the supplied NAMD command file. As in the constructor, only the basic NAMD command file is understood, and any TCL commands are ignored.

\cmd{sim.setTemperature(temp)}
Set the temperature of the simulation to \tpg{temp} kelvin. If \tpg{temp} is \tpg{None} then the thermostat is switched off.

\cmd{sim.setPressure(press)}
Set the pressure of the simulation to \tpg{press} atmospheres. If \tpg{press} is \tpg{None} then the barostat is switched off.

The above two functions are used by the following three higher functions;

\cmd{sim.setNPT(temperature=298, pressure=1)}
Switches to the NPT ensemble, at the specified temperature and pressure.

\cmd{sim.setNVT(temperature=298)}
Switches to the NVT ensemble, at the specified temperature.

\cmd{sim.setNVE()}
Switches to the NVE ensemble.

\cmd{sim.setCoordFreq(nsteps)}
Set the frequency of writing coordinates to the coordinate trajectory file to every \tpg{nsteps} steps.

\cmd{sim.setVelFreq(nsteps)}
Set the frequency of writing velocities to the velocity trajectory file to every \tpg{nsteps} steps.

\cmd{sim.compress(simdir)}
Compress the input files for the simulation that was run in the `current' directory of \tpg{simdir}.

\cmd{sim.linkFiles("/path/to/olddir", "/path/to/newdir")}
Link or copy the output restart files from the old directory to the input files of the new directory. After running this command the new directory should contain everything necessary to run the molecular dynamics simulation.

\cmd{sim.deepCopy()}
Return a deep copy of this simulation object. The copy can be changed without affecting the original.

\cmd{sim.setFilterConf()}
Change the configuration of the simulation so that it is suitable for digital filtering. This means that \tpg{rigidbonds} is set to \tpg{none} and \tpg{COMmotion} is set to \tpg{yes}. This is necessary so that NAMD does not modify the velocities that are loaded via pre-processing operations (e.g. removing translational momentum).

As can be guessed, this powerful class provides many further functions that are used internally by NamdDriver and NamdDFDriver. For a full description of these please look at the source code, or use the python interactive help to browse the documentation strings that are embedded in the source code with these functions.

\subsection{DFDriver}
DFDriver is the class that provides the driver for the actual digital filtering. This class is responsible for running the \dftr program and parsing the output to see if the temperature cap has been breached.

\cmd{dfdriver = DFDriver("/path/to/digifilter")}
Create a DFDriver, passing the location of the \dftr executable.

\cmd{dfdriver.perform(simdir, sim, dfconf)}
Actually perform digital filtering. The simulation output of \tpg{sim}, in the current directory of \tpg{simdir}, is filtered according to the configuration held in \tpg{dfconf}. Note that \tpg{sim} and \tpg{dfconf} need to be compatible with one another, e.g. use NamdSim with NamdDFConf, or if you have written them, SanderSim with SanderDFConf. If the temperature cap is breached, then this function will raise a \tpg{TempCapBreached} exception.

\subsection{DFConf}
DFConf provides the base class of digital filtering configuration objects. This class provides functions to set the filter scale, velocity scale, temperature cap, temperature cap file, filter cap, filter file and system file. These functions are is pairs, e.g.

\cmd{dfconf.setFiltercap(5)}
sets the filter cap to 5, while,

\cmd{dfconf.filtercap()}
returns the filter cap. 

Similar functions exist for all of the other filter parameters.

\subsection{NamdDFConf}
NamdDFConf provides the specilisation of DFConf for NAMD simulations. This class is designed to work with NamdSim and NamdDriver.

\cmd{dfconf = NamdDFConf()}
The NamdDFConf constructor has no options.

\cmd{cmdfile = dfconf.write("/path/to/cmdfile", namdsim)}
Write a \dftr command file to \tpg{/path/to/cmdfile} (which is also returned), using the parameters stored in this object, and using the output files and fileformats specified in \tpg{namdsim}. Note that NamdDFConf has to be compatible with \tpg{namdsim}.

This function uses another function, \tpg{dfconf.commandFileContents()}, to return a string with the contents of the written command file. To change the \dftr command file, and thus write your own, you need to derive from NamdDFConf and override the \tpg{commandFileContents()} function. It is easiest to do this using the original function in NamdDFConf as a model. 

\subsection{NamdMonitor}
NamdMonitor provides a NAMD specific class to calculate and record the temperatures of different parts of the system while the filter is being applied. This class is designed to work with NamdSim, though it could be used as a base for other types of Monitors, e.g. SanderMonitor or CharmmMonitor.

\cmd{monitor = NamdMonitor("/path/to/digifilter", logfile)}
Create a NamdMonitor object, passing the location of the \dftr executable, and the name of the logfile into which the temperatures and kinetic energies will be recorded.

\cmd{monitor.addSystemPDB("tagname", "/path/to/systempdb")}
Add a system PDB file to the monitor. The monitor will calculate the kinetic energy and velocity of the system using each system PDB file that has been added to it. This allows the monitor to calculate the energies of different subsets of the system, e.g. one of the added system PDB files could only mark the solvent atoms, while another could only mark the backbone atoms. The kinetic energy and temperature of the marked atoms of each system PDB are calculated during filtering, with the output written to \tpg{stdout} and to the logfile. To prevent confusion, each temperature output is preceeded by the \tpg{tagname}, also given in this function. Thus the two regions could be given tagnames \tpg{solvent} and \tpg{backbone} respectively. The example in figure \ref{fig:wat-cmd} uses a NamdMonitor to monitor the temperatures of the filtered region and of the non-hydrogen atoms of the system.

\subsection{SimDir}
SimDir is a class that can keep track of the current output directory, and provides functions to access previous output directories. In addition, each directory can have an associated name, so that you can mark which phase of the simulation you are in.

\cmd{simdir = SimDir("/path/to/basedir", tagname, strt)}
This constructs a new SimDir that has the current output directory as \tpg{/path/to/basedir/strt\_tagname}. The value of \tpg{strt} can be an positive integer (including zero), and this integer is incremented as the simulation progresses. 

\cmd{simdir.change()}
Change into the current directory of the SimDir.

\cmd{simdir.revert()}
Revert to the previous directory. Every directory change is stored in a stack, enabling the SimDir to go back to previous directories. If there is no previous directory then nothing occurs.

\cmd{simdir.current()}
Return the full path to the `current' directory.

\cmd{simdir.last()}
Return the full path to the last directory (e.g. if we are on directory 5, then return the path to directory 4).

\cmd{simdir.createRoot()}
Create the base directory passed in the constructor. An error will be printed if this directory already exists.

\cmd{simdir.create()}
Create the `current' output directory. To prevent accidentally overwriting previous output, this function will raise an exception if this output directory already exists. This means that if you want to repeatedly run a single simulation, you need to manually move or delete the original output directory.

\cmd{simdir.increment()}
Increment the SimDir to the next output directory, e.g. if the `current' directory is 4, then increment the current directory to 5.

\cmd{simdir.setName(newname)}
Set the name, or tag for the current directory to \tpg{newname}. Note that this will only have an affect if it is called before the current directory has been created.

\subsection{File}
File provides a class that represents a file(!) This class simplifies the accessing, linking and locating of files. This class is used extensively by the \rdfmd module, and, given its large size, it will be described only briefly here.

\cmd{fle = File("filename")}
Create a File that represents the file \tpg{filename}. Note that this can be an absolute or relative path, and the file need not already exist.

\cmd{fle.fullPath()}
Return the absolute path to this file. 

\cmd{fle.directory()}
Return the absolute path of the directory that contains this file.

\cmd{fle.shortName()}
Return the name of the file (without the directory).

\cmd{fle.exists()}
Return whether or not the file exists.

\cmd{fle.append(extra)}
Append \tpg{extra} onto the end of the filename.

\cmd{fullfile = fle.resolvePath()}
This function resolves any symbolic links or compression to return the original file that this file is pointing to. This returns a copy of this file if it is not compressed and not a symbolic link.

\cmd{fle.compress() / fle.uncompress()}
Compress or uncompress this file.

\cmd{fle.create()}
Create this file if it does not already exist.

\cmd{fle.write(text)}
Append \tpg{text} to the end of this file.

\cmd{lines = fle.readLines()}
Read all of the lines in the file and return them as a list of strings (with trailing newlines removed).

\cmd{fle.remove()}
Remove this file.

\cmd{fle.copyTo(newfile)}
Copy this file to \tpg{newfile}.

\cmd{fle.linkTo(newfile)}
Create a symbolic link from this file to \tpg{newfile}. Note that the link is actually made to the result of \tpg{fle.resolvePath()}. Note also that the code is clever enough to turn the link into a relative link if that is possible. This is useful as it means that the links between files within the output directories should remain intact even if you move the whole output directory tree.

\cmd{fle.moveTo(newfile)}
Move this file to \tpg{newfile}.

\subsection{Marker}
The only other significant part of the \rdfmd module is Marker. This is a class used to aid in the creation of the system PDB files required by \dftr. This class, and its use, is described in section \ref{sec:marker}.

\section{Diagnosing problems}
Because the \rdfmd module acts as a glue between NAMD (or any other MD package) and \dftr, it has to cope with many more things that could go wrong. Rather than complicate the code with lots of error handling, the design has deliberately been kept simple so that you can go into it and work out what has gone wrong. I strongly encourage you to learn python so that you can understand this code. Python is a very easy scripting language to learn, and will improve your productivity and allow you to take on tasks that you previously couldn't. If you learn python, and work your way through the code of the \rdfmd module, then you will be able to fix most of the problems that you come across, and you will be able to extend \rdfmd to other MD simulation packages, and develop new best practice for RDFMD simulations.

Unfortunately, the error codes from python can be a bit confusing. To help, look at the line numbers in the error output. The last line number is the line of code that actually caused the error. Then look at the output files for \dftr and for the molecular dynamics programs. These will normally tell you if you are missing an input file.

\section{Conclusion}
\rdfmd is a python module that provides an easy to understand and flexible piece of software to perform RDFMD simulations. The software uses \dftr to perform the actual digital filtering, and a specialised set of code (NamdDriver, NamdSim and NamdDFConf) is provided to use NAMD to generate the molecular dynamics trajectories. The code is designed such that if versions of these files for another package were written, which behaved in the same manner as the NAMD versions, then it would be possible to use another MD package to generate the trajectory, e.g. for AMBER/SANDER, a set such as SanderDriver, SanderSim and SanderDFConf could be produced.
