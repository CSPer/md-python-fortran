\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}digifilter}{2}{chapter.2}
\contentsline {section}{\numberline {2.1}Concepts}{2}{section.2.1}
\contentsline {section}{\numberline {2.2}Command File}{3}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Commands}{4}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}param}{5}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}read}{6}{subsection.2.2.3}
\contentsline {subsection}{\numberline {2.2.4}write}{8}{subsection.2.2.4}
\contentsline {subsection}{\numberline {2.2.5}stitch}{10}{subsection.2.2.5}
\contentsline {subsection}{\numberline {2.2.6}strip}{10}{subsection.2.2.6}
\contentsline {subsection}{\numberline {2.2.7}remove}{11}{subsection.2.2.7}
\contentsline {subsection}{\numberline {2.2.8}mark}{11}{subsection.2.2.8}
\contentsline {subsection}{\numberline {2.2.9}clearbuffer}{12}{subsection.2.2.9}
\contentsline {subsection}{\numberline {2.2.10}avgbuffer}{13}{subsection.2.2.10}
\contentsline {subsection}{\numberline {2.2.11}kineticnrg}{13}{subsection.2.2.11}
\contentsline {subsection}{\numberline {2.2.12}removetr}{13}{subsection.2.2.12}
\contentsline {subsection}{\numberline {2.2.13}removenontr}{14}{subsection.2.2.13}
\contentsline {subsection}{\numberline {2.2.14}filter}{14}{subsection.2.2.14}
\contentsline {section}{\numberline {2.3}Example}{16}{section.2.3}
\contentsline {section}{\numberline {2.4}Conclusion}{17}{section.2.4}
\contentsline {chapter}{\numberline {3}rdfmd}{18}{chapter.3}
\contentsline {section}{\numberline {3.1}Concepts}{18}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Requirements of an RDFMD software system}{21}{subsection.3.1.1}
\contentsline {section}{\numberline {3.2}The rdfmd module, an overview}{22}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Key classes}{22}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Example}{23}{subsection.3.2.2}
\contentsline {section}{\numberline {3.3}The rdfmd module, the specifics}{27}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}DFSim}{27}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}NamdDriver}{29}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}NamdSim}{29}{subsection.3.3.3}
\contentsline {subsection}{\numberline {3.3.4}DFDriver}{32}{subsection.3.3.4}
\contentsline {subsection}{\numberline {3.3.5}DFConf}{32}{subsection.3.3.5}
\contentsline {subsection}{\numberline {3.3.6}NamdDFConf}{33}{subsection.3.3.6}
\contentsline {subsection}{\numberline {3.3.7}NamdMonitor}{33}{subsection.3.3.7}
\contentsline {subsection}{\numberline {3.3.8}SimDir}{34}{subsection.3.3.8}
\contentsline {subsection}{\numberline {3.3.9}File}{35}{subsection.3.3.9}
\contentsline {subsection}{\numberline {3.3.10}Marker}{37}{subsection.3.3.10}
\contentsline {section}{\numberline {3.4}Diagnosing problems}{37}{section.3.4}
\contentsline {section}{\numberline {3.5}Conclusion}{38}{section.3.5}
\contentsline {chapter}{\numberline {4}Examples}{39}{chapter.4}
\contentsline {section}{\numberline {4.1}Alanine Dipeptide}{39}{section.4.1}
\contentsline {section}{\numberline {4.2}Alanine Decapeptide}{40}{section.4.2}
\contentsline {section}{\numberline {4.3}Folate}{40}{section.4.3}
\contentsline {section}{\numberline {4.4}Water}{40}{section.4.4}
\contentsline {section}{\numberline {4.5}Conclusion}{41}{section.4.5}
\contentsline {chapter}{\numberline {A}Creating the System PDB File}{42}{appendix.A}
\contentsline {section}{\numberline {A.1}Creating the initial PDB file}{42}{section.A.1}
\contentsline {section}{\numberline {A.2}Using Marker.py}{43}{section.A.2}
\contentsline {section}{\numberline {A.3}Conclusion}{45}{section.A.3}
\contentsline {chapter}{\numberline {B}Creating the Filter File}{46}{appendix.B}
\contentsline {section}{\numberline {B.1}Filter Format}{46}{section.B.1}
\contentsline {subsection}{\numberline {B.1.1}The Averaging Filter}{47}{subsection.B.1.1}
\contentsline {subsection}{\numberline {B.1.2}The Nuke Filter}{47}{subsection.B.1.2}
\contentsline {subsection}{\numberline {B.1.3}The Matlab filter}{47}{subsection.B.1.3}
\contentsline {section}{\numberline {B.2}How to Create a Filter}{47}{section.B.2}
\contentsline {section}{\numberline {B.3}Visualising a Filter}{50}{section.B.3}
\contentsline {chapter}{\numberline {C}Stitching together trajectory files}{52}{appendix.C}
