
\chapter{Creating the System PDB File}\label{chp:marker}

The system PDB file is used to describe all of the atoms in the system, to give the element type of each atom, and to say which atoms are marked (e.g. for filtering). Creating the system PDB consists of three simple stages;
\begin{enumerate}
\item Create a normal PDB that represents the system
\item Use Marker.py to process the PDB and mark atoms
\item Test the resulting PDB with \dftr
\end{enumerate}

\section{Creating the initial PDB file}
To create the system PDB, you first need to generate a PDB file for the system that you are studying. This PDB file must contain all of the atoms that are in the trajectory files that you will be loading, and the atoms must be in the same order as the atoms in the trajectory files. It is a serious error for the system PDB to either contain extra atoms, or have missing atoms compared to the trajectory file, and such an error will likely cause \dftr to complain and exit. It is an even more serious error for the atoms in the PDB to not match the order of those in the trajectory, as it will not be possible for \dftr to detect this error, and the resulting application of the filter will thus be incorrect.

The best way to check that your initial PDB file is correct is to use VMD (or an equivalent program) to load up the PDB, and then to load up the trajectory file using that PDB as a template. You can then check that the resulting movie shows the structure moving correctly.

\section{Using Marker.py}\label{sec:marker}
Once you have obtained the initial PDB file, you can then process it using Marker, part of the \rdfmd module. Marker is a python module that must be included in one of your scripts. An example of a script that uses Marker is shown in figure \ref{fig:marker}.
%%
%%
\begin{figure}
\begin{boxedverbatim}
00: #!/bin/env python
01: 
02: import sys
03: import os
04: 
05: #set the path to the 'rdfmd' directory that contains the 
06: #rdfmd python module
07: sys.path.append("/home/chris/Work/WorkSoftware")
08: 
09: #import the rdfmd python module
10: from rdfmd import *
11:
12: #create the Marker object that works on the PDB file "system.pdb"
13: marker = Marker("./system.pdb")
14: 
15: #marker has now read in the PDB file and guessed all of the atom types.
16: #All of the atoms are initially marked - we only want to mark atoms
17: #10-20...
18:
19: #Start by unmarking all of the atoms
20: marker.unmarkAll()
21:
22: #Now mark the range 10-20
23: marker.markRange(10,20)
24:
25: #Finally, write the resulting system PDB file out to 
26: # "system-marked.pdb"
27: marker.write("./system-marked.pdb")
28:
\end{boxedverbatim}
\caption
{
A simple python script that creates a system PDB file from system.pdb, marking atoms 10 to 20 inclusive, and writing the result to system-marked.pdb.
}\label{fig:marker}
\end{figure}
%%
%%
The script should appear pretty straightforward. It uses a Marker object to read the initial PDB file. When reading the PDB file the Marker object will use the name of the atom to guess the atom type. Most of the time it should guess correctly, though it will be thrown by strange names or biologically rare atoms. It is always best to check that the output is correct before you use the resulting file!

Marker will automatically mark all of the atoms that it reads in. If you want to change the marked range then you must first unmark all of the atoms, and then mark the range that you wish. The marking works in the same manner as the \tpg{mark} command in \dftr (see section \ref{sec:mark}), namely that the PDB atom numbers are used to identify atoms, and that all those with the same PDB atom number are marked equally. This means that in general, you should ensure that each atom has a unique PDB atom number, though you could use repeated numbers to refer to blocks of atoms (as was described in section \ref{sec:mark}).

The member functions of Marker are as follows;

\cmd{marker = Marker(pdbfile=None)}
Create a new Marker object, optionally passing the name of the PDB file to read in.

\cmd{marker.clear()}
Clear the Marker of all loaded data.

\cmd{marker.readPDB(pdbfile)}
Read in a new PDB file. Whatever was previously loaded is cleared.

\cmd{marker.scan()}
Rescan the PDB and reassign all the atom types. Note that this will remark all of the atoms.

\cmd{marker.write(newfile)}
Write out the marked PDB to \tpg{newfile}.

\cmd{marker.markRange(strt, end, step=1)}
Mark atoms with PDB atom numbers from \tpg{strt} to \tpg{end} inclusive, marking every \tpg{step} atom.

\cmd{marker.unmarkRange(strt, end, step=1)}
Same as above, but unmark the atoms.

\cmd{marker.mark(idnum)}
Mark atoms with PDB atom number \tpg{idnum}.

\cmd{marker.unmark(idnum)}
Unmark atoms with PDB atom number \tpg{idnum}.

\cmd{marker.markAll()}
Mark all atoms in the system.

\cmd{marker.unmarkAll()}
Unmark all atoms in the system.

\cmd{marker.setAtom(idnum, typ)}
Set the atom type of atoms with PDB atom number \tpg{idnum} to \tpg{typ}.

\section{Conclusion}
Marker provides a convienient tool to aid in the creation of system PDB files. While it may not be perfect, it is sufficiently powerful to take the drudgery out of creating the system PDB file, and any errors that it may make should be relatively straightforward to correct by hand. In addition, Marker could form the basis of a more powerful code that could mark atoms based on distance from a point, or more complex masks, e.g. protein backbone.
