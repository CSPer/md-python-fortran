
\chapter{Creating the Filter File}\label{chp:filter}

The filter file lies at the heart of digital filtering, as it describes the frequency range that is enhanced or suppressed by the application of the filter. The filter is just a list of coefficients, with the number of coefficients equalling the number of frames in the filter buffer. This appendix will first describe the recognised formats of the filter, and will then detail a procedure that may be used to generate a filter file.

\section{Filter Format}
The history of digital filtering means that there are three different formats of filter file that are supported by \dftr. These formats are the averaging filter, nuke filter, and matlab filter. All three formats allow for comments to be placed in the file, with a `\#' at the start of the line needed to specify a comment line. I strongly recommend that you comment your filter files to record what the frequency response of the filter should be. This will prevent confusion or mistakes later on\ldots

In addition, the underlying code to read in the filters is very flexible, and is able to read in the coefficients in any format, e.g. each coefficient may be on a separate line, or they could all be on the same line separated by spaces, or a mixture of same and different lines may be used. All that matters is the order of numbers that appear in the filter file.

\subsection{The Averaging Filter}
The averaging filter is a simple filter that just forms the mean average of the velocities in the buffer. The only information that needs to be present in the averaging filter is the size of the filter buffer (and thus the number of velocities to average together). The format is correspondingly simple, consisting of a single line;

\cmd{avg \emph{nframes}}
Create an averaging filter containing \emph{nframes} frames in the buffer. Note that if \emph{nframes} is even, then one is added to the value so to make it odd.

\subsection{The Nuke Filter}
The nuke filter format is the one used by the \tpg{nuke} program. The first number in the file is the number of frames in the buffer. This must be an odd number. The number of frames in the buffer equals $2m+1$, where $m$ is a positive integer. The nuke filter file must contain a further $m+1$ numbers, these referring to coefficients $m+1$ to $2m+1$ inclusive. The coefficients from $m+2$ to $2m+1$ inclusive will be reflected to fill the coefficients $m$ to $1$ inclusive, thus forming a symmetrical filter.

\subsection{The Matlab filter}
The matlab filter is the format as output by Matlab, and lists each and every coefficient in the filter. While a filter should be symmetrical, this allows for non-symmetrical filters to be used. The file consists of just the list of coefficients. There should be an odd number of coefficients in the file, but other than that, there is no restriction on how the coefficients are arranged in the file. The coefficients must be ordered, with the nth coefficient in the file corresponding to the nth coefficient in the filter. Examples of the matlab filter can be found in the \tpg{test} direcctory, e.g. \tpg{test/aladip/input/filter0\_300}.

\section{How to Create a Filter}
Creating a filter can be tricky. We have found that the easiest approach is to use the \tpg{fircls} function of \tpg{MATLAB}. This function is quite complex. To use the function, you need to know the range of frequencies that you wish to enhance, and the timestep of the simulation that you will be using. You will need the timestep as you will have to convert the frequency range in wavenumbers (cm$^{-1}$), to a normalised frequency. You normalise the frequency by diving by a function that depends on the timestep,
\begin{equation}
f_{normalised} = \frac{f_{wavenumbers}}{g(\delta t)},
\end{equation}
where,
\begin{equation}
g(\delta t) = \frac{1}{2 \times \delta t \times c},
\end{equation}
where $\delta t$ is the timestep in seconds, and $c$ is the speed of light in cm~s$^{-1}$. For a timestep of 1~fs, this means that the normalised frequency is obtained by dividing the frequency in wavenumbers by 16678~cm$^{-1}$, while for a 2~fs timestep, you must divide by 8339.1~$cm^{-1}$.

Once you have decided on your desired frequency response, and have normalised the frequencies, you can now decide on the number of filter coefficients you wish to use. Using more coefficients will mean that the filter will more accurately reflect your desired frequency response, but at the cost of requiring longer simulations to fill the larger filter buffers. As most of the buffer is discarded, it is best to use as small a filter buffer as possible. Remembering that there must be an odd number of filter coefficients, the best values range from 101 coefficients up to 2001 coefficients.

The \tpg{fircls} command is used as follows;

\cmd{f = fircls( \emph{n}, [0 \emph{a} \emph{b} 1], [s1 s2 s3], [1.1 0.001], [0.9 -0.001], 'text')}
\emph{n} is the number of coefficients in the filter, and 0, \emph{a}, \emph{b} and 1 are the boundaries that partition the frequency range into three regions. The first region extends from 0 to \emph{a}, the second from \emph{a} to \emph{b} and the third from \emph{b} to 1. The end of the regions must necessarily be 0 and 1. The remaining parameters are the values by which to scale the three regions, e.g. is \emph{s1} was zero, \emph{s2} was one, and \emph{s3} was zero, then frequencies between 0 and \emph{a} would be suppressed, between \emph{a} and \emph{b} would be left alone, and then between \emph{b} and 1 would be suppressed.

It is normally the case that we only wish to enhance the low frequency motion, and thus we can use a slightly simpler form of this command;

\cmd{f = fircls( \emph{n}, [0 \emph{a} 1], [1.0 0.0], [1.1 0.001], [0.9 -0.001], 'text')}
In this command, frequencies from 0 to \emph{a} are left alone, while those from \emph{a} upwards are suppressed. If \emph{a} was $0.0180$, then this corresponds to a frequency of 300~cm$^{-1}$ for a timestep of 1~fs. Such a filter would be called a 0-300~cm$^{-1}$ filter, as it would remove all frequencies except those in the 0 to 300~cm$^{-1}$ range.

Once you have created the filter, you need to write it to a text file. A matlab script to do this is shown in figure \ref{fig:matlab}. You can run this script either by starting \tpg{matlab} and typing it into the command window, or by asking \tpg{matlab} to run the file.
%%
%%
\begin{figure}
\begin{small}
\begin{boxedverbatim}
00: 
01: f = fircls( 2001, [0 0.0180 1], [1.0 0.0], [1.1 0.001], [-0.9 -0.001], 'text')
02: b = f'
03: save filter0_300 b -ascii
04:
\end{boxedverbatim}
\end{small}
\caption
{
A small matlab script to 2001 coefficient, 0-300~cm$^{-1}$ filter, that is saved to the file called filter0\_300.
}\label{fig:matlab}
\end{figure}
%%
%%
The resulting output file can be read directly by \dftr, though you may wish to edit the file to add comments that give the \tpg{fircls} command used to create the file, and information about the desired frequency response and required timestep.

\newpage
\section{Visualising a Filter}\label{sec:viewfilter}
You can visualise the frequency response of the filter using \dftr.

\cmd{viewfilter \emph{ts} \emph{filename}}
This command tells \dftr to calculate the frequency response of the currently loaded filter, given the supplied timestep \emph{ts} (in femtoseconds), and will output the result to the file \emph{filename}. A script to calculate the frequency response for the 0-300~cm$^{-1}$ filter in \tpg{test/aladip/input/filter0\_300} is shown in figure \ref{fig:filterscript}.
%%
%%
\begin{figure}
\begin{boxedverbatim}
00:
01: #Read in the filter
02: read filter test/aladip/input/filter0_300
03: 
04: #Now calculate the frequency response, using a timestep of 1 fs,
05: #and writing the result to 'viewfilter.data'
06: viewfilter 1.0 viewfilter.data
07:
\end{boxedverbatim}
\caption
{
Script used to view the frequency response of a filter.
}\label{fig:filterscript}
\end{figure}
%%
%%
The resulting file contains three columns; the first is the frequency in wavenumbers, the second is the amount by which that frequency is scaled, and the third is the phase. This file can be viewed using \tpg{xmgrace} or a similar program. The resulting graph for this filter is shown in figure \ref{fig:xmgrace}.
%%
%%
\begin{figure}\centering
\includegraphics[width=5in]{view}
\caption
{
The graph of the frequency response of a 0-300~cm$^{-1}$ filter.
}\label{fig:xmgrace}
\end{figure}
