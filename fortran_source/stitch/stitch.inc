

      integer DCD,CRD
      parameter(DCD=1)
      parameter(CRD=2)

      integer MAXFILES
      parameter(MAXFILES=1000)

      integer MAXATOMS
      parameter(MAXATOMS=150000)

      integer FILE,OFILE
      parameter(FILE=42)
      parameter(OFILE=43)

      real tmpbuffer(MAXATOMS,3)

      character*300 filename(MAXFILES)

      common /stitchcommon/ tmpbuffer,filename