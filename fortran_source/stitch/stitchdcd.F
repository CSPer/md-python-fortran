
      subroutine stitchDCD(ioutfile, nfiles, istep)
      implicit none
      include 'stitch.inc'
c####################################################################
c
c     This routine stitches together nfiles DCD files to 
c     form a single output DCD file (ioutfile). Every 
c     istep step from the files is written.
c
c     (C) Christopher Woods, June 2005
c
c####################################################################

      character*(*) ioutfile
      integer istep,idelta
      character*4 hdr
      character*32 title(30)
      double precision delta
      integer nset,nsetall,ntitle
      integer nats,natoms,nframes
      integer i,j,k
      integer strt,nsavc,zeroes(14),natnfr
      integer nfiles,lgth,lineLength

c     copy the variable so that we have pass by value...
      idelta = istep
      if (idelta .le. 0) idelta = 1

c     open up the output file for writing
      call unlink(ioutfile)
      open(OFILE,file=ioutfile,form="unformatted",status="new",err=9601)

      nsetall = 0

c     now read in the header from each file so that we can sum up the number of sets...
      do i=1,nfiles
        lgth = lineLength(filename(i))
        open(FILE,file=filename(i),form="unformatted",status="old",err=9600)

        read(FILE,err=9700,end=9700) hdr,nset,strt,nsavc,(zeroes(k),k=1,5),natnfr,delta,(zeroes(k),k=6,14)

        if ((hdr.ne.'CORD' .and. hdr.ne.'VELD')) then
          print *,filename(i)(1:lgth)," does not look like a binary DCD file?"
          goto 9900
        endif

        nsetall = nsetall + nset

c       we have finished with the FILE for now, so we can close it...
        close(FILE)
      enddo

c     work out how many sets will be written to the file...
      print *,"Total number of frames in all trajectory files = ",nsetall
      nsetall = nsetall / idelta
      print *,"Of these, ",nsetall," will now be written... (please be patient)"

      nframes = 0

c     we can now read the files again to actually copy...
      do i=1,nfiles

        lgth = lineLength(filename(i))

c       now read in the header of file1
        open(FILE,file=filename(i),form="unformatted",status="old",err=9600)
        read(FILE,err=9700,end=9700) hdr,nset,strt,nsavc,(zeroes(k),k=1,5),natnfr,delta,(zeroes(k),k=6,14)

        if (i .eq. 1) then
c         write this information out to the header of the output file
          write(OFILE,err=9700) hdr,nsetall,strt,nsavc,(zeroes(k),k=1,5),natnfr,delta,(zeroes(k),k=6,14)
        endif

c       read in the title of file1
        read(FILE,err=9700,end=9700) ntitle,(title(k),k=1,2*ntitle+1)

        if (i .eq. 1) then
c         write it to the output file
          write(OFILE,err=9700) ntitle,(title(k),k=1,2*ntitle+1)
        endif

c       now read in the number of atoms in the file
        read(FILE,err=9700,end=9700) nats
        if (i .eq. 1) then
          natoms = nats
          if (natoms .gt. MAXATOMS) then
            print *,"Exceeded maximum number of atoms ",MAXATOMS
            print *,"Increase MAXATOMS to be at least equal to ",natoms
            goto 9900
          endif

          write(OFILE,err=9700) natoms

        else
          if (nats .ne. natoms) then
            print *,"Disagreement with the number of atoms in the file!"
            goto 9900
          endif
        endif

c       read in all of the DCD file, and write the coordinates if nframes / idelta is 
c       a whole number (e.g. write out every idelta frames)

        do k=1,nset
c         read the x/y/z coordinates into a temporary buffer          
          read(FILE,err=9700,end=9700) (tmpbuffer(j,1),j=1,nats)
          read(FILE,err=9700,end=9700) (tmpbuffer(j,2),j=1,nats)
          read(FILE,err=9700,end=9700) (tmpbuffer(j,3),j=1,nats)
        
c         increment the total number of frames that have been read in
          nframes = nframes + 1

          if (mod(nframes,idelta) .eq. 0) then
c           this is one of the frames to be written out, so get writing!
            write(OFILE,err=9700) (tmpbuffer(j,1),j=1,nats)
            write(OFILE,err=9700) (tmpbuffer(j,2),j=1,nats)
            write(OFILE,err=9700) (tmpbuffer(j,3),j=1,nats)
          endif
        enddo

c       we have finished with FILE, so we can close it
        close(FILE)
      enddo

c     everything has finished ok!
      print *,"Everything is finished!"
      close(OFILE)

      return

9600  continue
      print *,"Problem in stitchDCD opening DCD file, ",filename(1)(1:lgth)
      goto 9900

9601  continue
      lgth = lineLength(ioutfile)
      print *,"Problem in stitchDCD opening output DCD file ",ioutfile(1:lgth)
      goto 9900

9700  continue
      print *,"There was an error reading one of the DCD files!"
      goto 9900

9900  continue
      print *,"Failed to stitch the files together!"

      close(FILE)
      close(OFILE)

      return
      end
