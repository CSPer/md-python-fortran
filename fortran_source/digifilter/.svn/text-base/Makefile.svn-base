#-----------------------------------------------
#  Makefile for digifilter
#     Christopher Woods
#-----------------------------------------------

#*******************************************************
#  This is the list of all source files used to
#  build digifilter. If you want to add a new file
#  to digifilter then just add it to this list.
#*******************************************************
OBJS = addvelocity.o \
       applyfilter.o \
       averagevelbuffer.o \
       checksanity.o \
       copyfrombuffer.o \
       copytobuffer.o \
       initialise.o \
       getbufferoptions.o \
       getfilteroptions.o \
       getkineticnrgoptions.o \
       getstitchoptions.o \
       getstripoptions.o \
       getwriteoptions.o \
       loadnukeatomweights.o \
       loadnukefiles.o \
       loadnukefilteratoms.o \
       parseerror.o \
       parsefilterline.o \
       parsekineticline.o \
       parsemarkline.o \
       parseparamline.o \
       parsereadline.o \
       parsestitchline.o \
       parsestripline.o \
       parsewriteline.o \
       readcrd.o \
       readdcd.o \
       readfilter.o \
       readnukebuffer.o \
       removetranslationrotation.o \
       viewfilter.o \
       writecrd.o \
       writedcd.o \
       writepdbtrajectory.o

#*******************************************************
# This is the list of all of the include files that
# contain common blocks
#*******************************************************

INC = digifilter.inc

#
#*********************************************************************
#  You shouldn't need to modify anything below. These lines compile
#  DigiFilter based on the compiler names and flags, the object list
#*********************************************************************
#

#escape sequence for a newline
nl = \033[\n

#colours...
# F/B light fg/bg; f/b dim fg/bg; Brgybpcw cols; 
# n return to normal; D dim; L light
n = \033[0m
D = \033[0m
L = \033[1m
FB = \033[1;30m
Fr = \033[1;31m
Fg = \033[1;32m
Fy = \033[1;33m
Fb = \033[1;34m
Fp = \033[1;35m
Fc = \033[1;36m
Fw = \033[1;37m
BB = \033[1;40m
Br = \033[1;41m
Bg = \033[1;42m
By = \033[1;43m
Bb = \033[1;44m
Bp = \033[1;45m
Bc = \033[1;46m
Bw = \033[1;47m
fB = \033[0;30m
fr = \033[0;31m
fg = \033[0;32m
fy = \033[0;33m
fb = \033[0;34m
fp = \033[0;35m
fc = \033[0;36m
fw = \033[0;37m
bB = \033[0;40m
br = \033[0;41m
bg = \033[0;42m
by = \033[0;43m
bb = \033[0;44m
bp = \033[0;45m 
bc = \033[0;46m 
bw = \033[0;47m

# The list of objects is assumed to be in variable OBJS.
# Sources corresponding to the objects:
SRCS = $(OBJS:%.o=%.F)

%.o : %.F $(INC)
	@echo -e "$(Fp)Compiling $(Fb)$<...$(Fr)"
	@-$(FC) -c $(FFLAGS) $< -o $@

# Build DigiFilter
digifilter: $(OBJS) $(INC) digifilter.F
	@echo -e "$(Fp)Linking digifilter...$(Fr)"
	@$(FC) $(LFLAGS) $(FFLAGS) digifilter.F -o ../exe/digifilter $(OBJS) ../exe/libcore.a

clean:
	@-rm *.o
