
      subroutine parseKineticLine()
      implicit none
      include 'digifilter.inc'
c######################################################
c
c     Parses the 'kineticnrg' line. This is
c     used to calculate the kinetic energy
c     before ('old') and after ('new') 
c     digital filtering.
c
c     (C) Christopher Woods, May 2005
c
c######################################################

      double precision kineticEnergy
      double precision inrg,itemp
      integer state
      logical markedonly

      call getKineticNrgOptions(state,markedonly)

      if (state .eq. OLD) then
        oldnrg = kineticEnergy(markedonly,oldtemp)
      
        write(printstring,*) "The kinetic energy is ",oldnrg," J mol-1, or ",
     .         oldnrg*J2KCAL," kcal mol-1. ",
     .         "This corresponds to a temperature of ",oldtemp," K."
        call printLine(STDOUT,printstring)
      else if (state .eq. NEW) then
        newnrg = kineticEnergy(markedonly,newtemp)
        
        write(printstring,*) "The kinetic energy is ",newnrg," J mol-1, or ",
     .         newnrg*J2KCAL," kcal mol-1. ",
     .         "This corresponds to a temperature of ",newtemp," K."
        call printLine(STDOUT,printstring)

        write(printstring,*) "This is a change in kinetic energy of ",newnrg-oldnrg,
     .       " J mol-1, or ",(newnrg-oldnrg)*J2KCAL," kcal mol-1, ",newtemp-oldtemp," K."
        call printLine(STDOUT,printstring)
      else
        inrg = kineticEnergy(markedonly,itemp)
      
        write(printstring,*) "The kinetic energy is ",inrg," J mol-1, or ",
     .         inrg*J2KCAL," kcal mol-1. ",
     .         "This corresponds to a temperature of ",itemp," K."
        call printLine(STDOUT,printstring)
      endif

      return
      end
