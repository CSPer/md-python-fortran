
      include '../core/core.inc'
      include '../core/splitter.inc'

c     the maximum number of atoms that may be filtered
      integer MAXFILTERATOMS
      parameter(MAXFILTERATOMS=300)

c     the maximum number of filter coefficients/frames
      integer MAXCOEFFS
      parameter(MAXCOEFFS=4001)

c     the actual filter coefficients
      double precision filter(MAXCOEFFS)

c     whether or not any data has been loaded into buffer position 'i' 
      logical loadedvelbuffer(MAXCOEFFS)
      logical loadedcoordbuffer(MAXCOEFFS)

c     the velocity filter buffer - this can take up a lot of memory!
      double precision velbuffer(MAXCOEFFS,MAXFILTERATOMS,3)
c     the coordinate buffer - again more memory!
      double precision coordbuffer(MAXCOEFFS,MAXFILTERATOMS,3)

c     a temporary buffer to hold the filtered velocities
      double precision filtervels(MAXFILTERATOMS,3)

c     the index of the central frame of the configuration
      integer centindex
c     the number of filter coefficients - this equals 2m+1
      integer ncoeffs
c     the value of 'm' (ncoeffs = 2m+1)
      integer m
c     the number of frames in the buffer
      integer nframes

c     the default value of fscale
      double precision defaultfscale

c     saved energies for 'kineticEnergy'...
      double precision oldnrg,oldtemp
      double precision newnrg,newtemp

      common /DIGICOMMON/ velbuffer,coordbuffer,filtervels,filter,
     .                    oldnrg,oldtemp,newnrg,newtemp,defaultfscale,
     .                    centindex,ncoeffs,m,nframes,
     .                    loadedvelbuffer,loadedcoordbuffer
