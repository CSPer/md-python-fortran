
      subroutine stitchDCD(ifile1,ifile2,ioutfile,istep)
      implicit none
      include "core.inc"
c####################################################################
c
c     This routine stitches together two DCD files,
c     ifile1 and ifile2, writing the output to a new DCD
c     file, ioutfile. Every istep frames are written
c     to the output file.
c
c     (C) Christopher Woods, June 2005
c
c####################################################################

      character*(*) ifile1,ifile2,ioutfile
      integer istep,idelta
      integer lgth1,lgth2,olgth,lineLength
      character*4 hdr
      character*32 title(30)
      double precision delta
      integer nset1,nset2,ntitle
      integer nsetout,nats,nats2,nframes
      integer i,j
      integer strt,nsavc,zeroes(14),natnfr

      lgth1 = lineLength(ifile1)
      lgth2 = lineLength(ifile2)
      olgth = lineLength(ioutfile)

c     copy the variable so that we have pass by value...
      idelta = istep
      if (idelta .le. 0) idelta = 1

      if (debug) then
        write(printstring,*) "Stitching together DCD files ",ifile1(1:lgth1)," and ",
     .                       ifile2(1:lgth2)," into the new DCD file ",ioutfile(1:olgth)
        call printLine(STDOUT,printstring)
        
        if (idelta .eq. 1) then
          call printLine(STDOUT,"Including every frame of both DCD files")
        else
          write(printstring,*) "Including every ",idelta," frames"
          call printLine(STDOUT,printstring)
        endif
      endif

c     open up the output file for writing
      call unlink(ioutfile)
      open(OFILE,file=ioutfile,form="unformatted",status="new",err=9602)

c     now read in the header for the input files to get the number of frames...
c     Read file2 first, as we will write the header of file1 to outfile...
      open(FILE2,file=ifile2,form="unformatted",status="old",err=9601)

      read(FILE2,err=9700,end=9700) hdr,nset2,strt,nsavc,(zeroes(i),i=1,5),natnfr,delta,(zeroes(i),i=6,14)

      if ((hdr.ne.'CORD' .and. hdr.ne.'VELD')) then
        write(printstring,*) ifile2(1:lgth2)," does not look like a binary DCD file?"
        call printLine(STDOUT,printstring)
        goto 9900
      endif

c     we have finished with FILE2 for now, so we can close it... (this is to prevent problems if
c     FILE1 == FILE2
      close(FILE2)

c     now read in the header of file1
      open(FILE1,file=ifile1,form="unformatted",status="old",err=9600)
      read(FILE1,err=9700,end=9700) hdr,nset1,strt,nsavc,(zeroes(i),i=1,5),natnfr,delta,(zeroes(i),i=6,14)

      if ((hdr.ne.'CORD' .and. hdr.ne.'VELD')) then
        write(printstring,*) ifile1(1:lgth1)," does not look like a binary DCD file?"
        call printLine(STDOUT,printstring)
        goto 9900
      endif

c     the total number of frames is nset + nset2
      nsetout = nset1 + nset2

c     but might not write every frame...
      nsetout = (nsetout / idelta)

c     write this information out to the header of the output file
      write(OFILE,err=9700) hdr,nsetout,strt,nsavc,(zeroes(i),i=1,5),natnfr,delta,(zeroes(i),i=6,14)

c     read in the title of file1
      read(FILE1,err=9700,end=9700) ntitle,(title(i),i=1,2*ntitle+1)
c     write it to the output file
      write(OFILE,err=9700) ntitle,(title(i),i=1,2*ntitle+1)
      if (debug) then
        call printLine(STDOUT,"Title lines from FILE1:")
        write(printstring,*) (title(i),i=1,2*ntitle+1)
        call printLine(STDOUT,printstring)
      endif

c     now read in the number of atoms in both files...
      read(FILE1,err=9700,end=9700) nats
      if (debug) then
        write(printstring,*) "FILE1: natoms = ",nats
        call printLine(STDOUT,printstring)
      endif

      if (nats.gt.MAXATOMS) then
        write(printstring,*) "Exceeded the maximum number of atoms, ",MAXATOMS,
     .                 ". Increase MAXATOMS to at least be equal to ",nats
        call printLine(STDOUT,printstring)
        goto 9900
      endif

c     write the number of atoms to the output file
      write(OFILE,err=9700) nats
      
c     read in all of the first DCD file, and write the coordinates if nframes / idelta is 
c     a whole number (e.g. write out every idelta frames)
      nframes = 0

      do i=1,nset1
c       read the x/y/z coordinates into a temporary buffer          
        read(FILE1,err=9700,end=9700) (tmpbuffer(j,1),j=1,nats)
        read(FILE1,err=9700,end=9700) (tmpbuffer(j,2),j=1,nats)
        read(FILE1,err=9700,end=9700) (tmpbuffer(j,3),j=1,nats)
        
c       increment the total number of frames that have been read in
        nframes = nframes + 1

        if (mod(nframes,idelta) .eq. 0) then
c         this is one of the frames to be written out, so get writing!
          write(OFILE,err=9700) (tmpbuffer(j,1),j=1,nats)
          write(OFILE,err=9700) (tmpbuffer(j,2),j=1,nats)
          write(OFILE,err=9700) (tmpbuffer(j,3),j=1,nats)
        endif
      enddo

c     we have finished with FILE1, so we can close it
      close(FILE1)

c     now go through file2 - reopen the file
      open(FILE2,file=ifile2,form="unformatted",status="old",err=9601)

c     read away the header
      read(FILE2,err=9700,end=9700) hdr,nset2,strt,nsavc,(zeroes(i),i=1,5),natnfr,delta,(zeroes(i),i=6,14)

c     now read away the title...
      read(FILE2,err=9700,end=9700) ntitle,(title(i),i=1,2*ntitle+1)
      if (debug) then
        call printLine(STDOUT,"Title lines from FILE2:")
        write(printstring,*) (title(i),i=1,2*ntitle+1)
        call printLine(STDOUT,printstring)
      endif

c     check that the numbers of atoms is in agreement
      read(FILE2,err=9700,end=9700) nats2
      if (debug) then
        write(printstring,*) "FILE2: natoms = ",nats2
        call printLine(STDOUT,printstring)
      endif

      if (nats .ne. nats2) then
        write(printstring,*) "The two DCD files have different numbers of atoms! ",
     .             nats," vs. ",nats2
        call printLine(STDOUT,printstring)
        goto 9900
      endif

      do i=1,nset2
c       read the x/y/z coordinates into a temporary buffer          
        read(FILE2,err=9700,end=9700) (tmpbuffer(j,1),j=1,nats)
        read(FILE2,err=9700,end=9700) (tmpbuffer(j,2),j=1,nats)
        read(FILE2,err=9700,end=9700) (tmpbuffer(j,3),j=1,nats)
        
c       increment the total number of frames that have been read in
        nframes = nframes + 1

        if (mod(nframes,idelta) .eq. 0) then
c         this is one of the frames to be written out, so get writing!
          write(OFILE,err=9700) (tmpbuffer(j,1),j=1,nats)
          write(OFILE,err=9700) (tmpbuffer(j,2),j=1,nats)
          write(OFILE,err=9700) (tmpbuffer(j,3),j=1,nats)
        endif
      enddo

c     everything has finished ok!
      close(FILE1)
      close(FILE2)
      close(OFILE)

      return

9600  continue
      write(printstring,*) "Problem in stitchDCD opening DCD file 1, ",ifile1(1:lgth1)
      call printLine(STDOUT,printstring)
      goto 9900

9601  continue
      write(printstring,*) "Problem in stitchDCD opening DCD file 2, ",ifile2(1:lgth2)
      call printLine(STDOUT,printstring)
      goto 9900

9602  continue
      write(printstring,*) "Problem in stitchDCD opening output DCD file ",ioutfile(1:olgth)
      call printLine(STDOUT,printstring)
      goto 9900

9700  continue
      call printLine(STDOUT,"There was an error reading one of the DCD files!")
      goto 9900

9900  continue
      call printLine(STDOUT,"Failed to stitch the files together!")
      close(FILE1)
      close(FILE2)
      close(OFILE)

      return
      end
