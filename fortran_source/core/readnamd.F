
      subroutine readNAMD(filename,typ,scl,markedonly)
      implicit none
      include 'core.inc'
      include 'splitter.inc'
c###############################################
c
c     Read in atomcoords or atomvels from the 
c     NAMD binary file 'filename'
c
c     (C) Christopher Woods, June 2005
c
c###############################################

      character*(*) filename
      integer lineLength,lgth
      integer nats
      integer typ
      integer i,j,idx
      double precision scl
      logical markedonly

c
c     The format of a namd binary file is as follows (according to the source code!)
c
c     integer  (number of vectors)
c     n * vector   (where each vector is 3 doubles)
c
c     Thus it should read as 1*integer followed by 3*n doubles
c

      call printLine(STDOUT,"The namdbin reader/writer does not work...")
      call exit(-1)
      
      lgth = lineLength(filename)
      if (debug) then
        write(printstring,*) "Reading NAMD binary file ",filename(1:lgth)
        call printLine(STDOUT,printstring)

        if (typ .ne. COORDS .and. scl.ne.ONE) then
          write(printstring,*) "Scaling velocities by ",scl
          call printLine(STDOUT,printstring)
        endif

      endif
            
c     open the NAMD binary file
      open(unit=FILE,file=filename,status='old',form='unformatted',err=9600)

c     read in the number of atoms
      read(FILE,err=9700,end=9700) nats

c     ensure that this is the right number of atoms
      if ( (markedonly .and. nats .ne. nmarked) .or. (.not. markedonly .and. nats .ne. natoms) ) then
        write(printstring,*) "There are the wrong number of atoms in this file! (",nats,")"
        call printLine(STDOUT,printstring)
        goto 9700
      endif

      if (debug) then
        write(printstring,*) "Reading in ",nats," atoms..."
        call printLine(STDOUT,printstring)
      endif

c     now read all of the data in to a temporary double precision buffer
      read(FILE,err=9700,end=9700) ( (dtmpbuffer(i,j),j=1,3), i=1,nats )

      do i=1,nats
        print *,(dtmpbuffer(i,j),j=1,3)
      enddo

c     copy the tmp buffer into the correct output...
      if (typ .eq. COORDS) then
        if (markedonly) then
          do i=1,nmarked
            idx = atomindex(i)
            do j=1,3
              atomcoords(idx,j) = dtmpbuffer(i,j)
            enddo
          enddo
        else
          do i=1,natoms
            do j=1,3
              atomcoords(i,j) = dtmpbuffer(i,j)
            enddo
          enddo
        endif
      else if (typ .eq. VELS) then
        if (markedonly) then
          do i=1,nmarked
            idx = atomindex(i)
            do j=1,3
              atomvels(idx,j) = scl * dtmpbuffer(i,j)
            enddo
          enddo
        else
          do i=1,natoms
            do j=1,3
              atomvels(i,j) = scl * dtmpbuffer(i,j)
            enddo
          enddo
        endif
      else if (typ .eq. AVGVELS) then
        if (markedonly) then
          do i=1,nmarked
            idx = atomindex(i)
            do j=1,3
              atomvels(idx,j) = HALF * (atomvels(idx,j) + scl*dtmpbuffer(i,j))
            enddo
          enddo
        else
          do i=1,natoms
            do j=1,3
              atomvels(i,j) = HALF * (atomvels(i,j) + scl*dtmpbuffer(i,j))
            enddo
          enddo
        endif
      else
        call printLine(STDOUT,"Unrecognised type to readnamd!")
      endif

      close(FILE)
      return
      
9600  continue
      write(printstring,*) "There was an error opening the NAMD binary file ",filename(1:lgth)
      call printLine(STDOUT,printstring)
      call exit(-1)      
      
9700  continue
c     there was an error while reading the file
      write(printstring,*) "There was an error while reading the NAMD binary file ",filename(1:lgth)
      call printLine(STDOUT,printstring)
      call exit(-1)
      
      return
      end
