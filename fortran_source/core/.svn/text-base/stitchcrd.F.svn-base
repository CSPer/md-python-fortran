
      subroutine stitchCRD(ifile1,ifile2,ioutfile,istep)
      implicit none
      include 'core.inc'
c#############################################
c
c     This routine stitches together two CRD files,
c     ifile1 and ifile2, writing the output to a new CRD
c     file, ioutfile. Every istep frames are written
c     to the output file.
c
c     (C) Christopher Woods, June 2005
c
c#############################################

      character*(*) ifile1,ifile2,ioutfile
      integer istep,idelta
      integer j,k,nframes
      character*1 hdr
      integer lgth1,lgth2,olgth,lineLength

      lgth1 = lineLength(ifile1)
      lgth2 = lineLength(ifile2)
      olgth = lineLength(ioutfile)

c     copy the variable so that we have pass by value...
      idelta = istep
      if (idelta .le. 0) idelta = 1

      if (debug) then
        write(printstring,*) "Stitching together CRD files ",ifile1(1:lgth1)," and ",
     .                       ifile2(1:lgth2)," into the new CRD file ",ioutfile(1:olgth)
        call printLine(STDOUT,printstring)
        
        if (idelta .eq. 1) then
          call printLine(STDOUT,"Including every frame of both CRD files")
        else
          write(printstring,*) "Including every ",idelta," frames"
          call printLine(STDOUT,printstring)
        endif
      endif

c     open up the output file for writing
      call unlink(ioutfile)
      open(OFILE,file=ioutfile,form="formatted",status="new",err=9602)

c     do the first file first!
      open(unit=FILE1,file=ifile1,form='formatted',status='old',err=9600)      

c     read away the blank line at the top of the file
      read(FILE1,9) hdr
9     format(a1)      
      
      write(OFILE,9) hdr

c     the formatted CRD file is arranged as a list of the coordinates/velocities
c     of each atom, with ten columns per list, and one frame per paragraph, e.g.
c
c     x01 y01 z01 x02 y02 z02 x03 y03 z03 x04
c     y04 z04 x05 y05 z05
c     x11 y11 z11 x12 y12 z12 x13 y13 z13 x14
c     y14 z14 x15 y15 z15
c     ... etc.      

      nframes = 0

c     read in the file, writing out the desired frames
100   continue
        read(FILE1,10,end=9500) ( (tmpbuffer(j,k), k=1,3), j=1,natoms)
10      format(10F8.3)

c       increment the number of read frames
        nframes = nframes + 1

c       see if this is a frame that should be written out
        if ( mod(nframes,idelta).eq.0 ) then
          write(OFILE,10) ( (tmpbuffer(j,k), k=1,3), j=1,natoms)
        endif            

        goto 100
9500  continue

c     get here when we have finished with the file
      
c     we can close the file
      close(FILE1)

c     now repeat the process with the second file...
c     do the first file first!
      open(unit=FILE2,file=ifile2,form='formatted',status='old',err=9601)      

c     read away the blank line at the top of the file
      read(FILE2,9) hdr

c     read in the second file, writing out the desired frames
101   continue
        read(FILE2,10,end=9501) ( (tmpbuffer(j,k), k=1,3), j=1,natoms)

c       increment the number of read frames
        nframes = nframes + 1

c       see if this is a frame that should be written out
        if ( mod(nframes,idelta).eq.0 ) then
          write(OFILE,10) ( (tmpbuffer(j,k), k=1,3), j=1,natoms)
        endif            

        goto 101
9501  continue
c     get here when we have finished with the file
      
c     we can close the file
      close(FILE2)

      
      return

9600  continue
      write(printstring,*) "Problem in stitchCRD opening CRD file 1, ",ifile1(1:lgth1)
      call printLine(STDOUT,printstring)
      goto 9900

9601  continue
      write(printstring,*) "Problem in stitchCRD opening CRD file 2, ",ifile2(1:lgth2)
      call printLine(STDOUT,printstring)
      goto 9900

9602  continue
      write(printstring,*) "Problem in stitchCRD opening output CRD file ",ioutfile(1:olgth)
      call printLine(STDOUT,printstring)
      goto 9900

9700  continue
      call printLine(STDOUT,"There was an error reading one of the CRD files!")
      goto 9900

9900  continue
      call printLine(STDOUT,"Failed to stitch the files together!")
      close(FILE1)
      close(FILE2)
      close(OFILE)
      
      return
      end
