
      double precision function ranGau(mean,stddev)
      implicit none
      include 'core.inc'
c##############################################################
c
c     This routine generates a random number on the 
c     gaussian distribution that is peaked at mean and 
c     has standard deviation stddev.
c
c     (C) Christopher Woods, December 2004
c
c##############################################################

      double precision mean,stddev
      double precision ran1,ran2,z
      double precision grnd
      double precision sqrt2
      integer i
      parameter(sqrt2=1.4142135623)
      
c####################################
c
c    This function works by generating a pair of random
c    numbers from a uniform distribution between 0 and 1.
c
c    This pair of uniform random numbers can be
c    converted to a pair of normal distributed
c    random numbers with mean 0.0 and stddev 1.0
c    using a Box-Muller transformation (see
c    'random.pdf' that accompanies this source code).
c
c    This pair of random numbers from a normal
c    distribution of mean 0.0 and stddev 1.0 can 
c    be converted to a pair of normally distributed
c    random numbers by using the equation;
c
c    ran' = stddev'*ran + mean'
c
c    where stddev' = sqrt(2) * stddev
c
c    See 'random.pl' for a simple perl script
c    that demonstrates this distribution
c    (run 'random.pl | xmgrace -pipe')

      if (oldrand) then
        rangau = sqrt2*stddev*randtemp + mean
        oldrand = .false.
        return
      endif

      do i=1,100
c       generate two uniform random numbers between -1.0 and 1.0 using the mersenne twister
c       uniform random number generator
        ran1 = 2.0D+00*grnd() - 1.0D+00
        ran2 = 2.0D+00*grnd() - 1.0D+00

c       convert them into normal random numbers using the
c       Box-Muller transformation - test the pair of numbers...
        z = ran1**2 + ran2**2

c       see if this pair of numbers pass...        
        if (z.le.1.0) goto 100
      enddo

      write(printstring,*) "Failed the Box-Muller test 100 times!"
      call printLine(STDOUT,printstring)
      call exit(-1)

100   continue

c     now form the gaussian numbers from those that pass      
      z = sqrt(-2.0*log(z)/z)
      
      ran1 = ran1*z
      ran2 = ran2*z

c     now convert one of these into a normally distributed number
      rangau = sqrt2*stddev*ran1 + mean

c     save the other number
      randtemp = ran2
      oldrand = .true.
      
      return      
      end

