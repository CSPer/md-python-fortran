      
      subroutine printLine(file,line)
      implicit none
c################################################
c
c     Print 'line' to filehandle 'file'
c
c     (C) Christopher Woods
c
c################################################

      integer file
      character*(*) line
      integer i
      integer maxlgth,lgth

c     left justify the line
      call leftJustify(line)
      
c     get the length of the line
      maxlgth = len(line)
      if (maxlgth.le.0) return
      
      lgth = 0
      do i=maxlgth,1,-1
        if (line(i:i).ne.' ') then
          lgth = i
          goto 100
        endif
      enddo
100   continue

      if (lgth.eq.0) return
      
      write(file,*) line(1:lgth)
      return
      end     
