
      subroutine writeAmber(filename, scl, markedonly)
      implicit none
      include 'core.inc'
c#############################################################
c
c     This function writes the coordinates and velocities
c     in atomcoords and atomvels to an amber restart file.
c     The atom velocities will be converted from internal
c     units (A ps-1) to external units by multiplying
c     by the factor 'convertVel2External'.
c
c     This function will not write the box size information, 
c     so this information will need to be appended to the resulting
c     file manually.
c
c     (C) Christopher Woods, March 2005
c
c#############################################################

      character*(*) filename
      integer lgth,lineLength
      integer i,j
      double precision scl
      logical markedonly
      
      if (debug) then
        lgth = lineLength(filename)
        write(printstring,*) "Writing AMBER restart file to ",filename(1:lgth)
        call printLine(STDOUT,printstring)
      endif
      
c     now open the file - remove it first to make sure that we
c     don't crash
      call unlink(filename)
      open(unit=FILE,file=filename,form='formatted',status='new',err=9900)      
      
c     write the header
      write(FILE,*) " Created by DigiFilter or RanVel"

c     don't write the current time as we don't know what it is!            
      write(FILE,10) natoms
10    format(i5)

      if (markedonly) then
c       now write out all of the coordinates
        write(FILE,11) ( (atomcoords(atomindex(i),j),j=1,3), i=1,nmarked)
c       now write out all of the velocities, converting them to amber velocities
        write(FILE,11) ( (scl * atomvels(atomindex(i),j),j=1,3), i=1,nmarked)
      else
c       now write out all of the coordinates
        write(FILE,11) ( (atomcoords(i,j),j=1,3), i=1,natoms)
c       now write out all of the velocities, converting them to amber velocities
        write(FILE,11) ( (scl * atomvels(i,j),j=1,3), i=1,natoms)
      endif
      
11    format(6F12.7)

      close(FILE)      
      
      return
      
9900  continue
      lgth = lineLength(filename)
      write(printstring,*) "There was a problem opening the AMBER restart file ",filename(1:lgth)
      call printLine(STDOUT,printstring)
      call printLine(STDOUT,"Check that you have write permissions to the directory!")
      call exit(-1)
      
      end
