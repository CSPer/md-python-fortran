
c     global variables used by libcore.a

c     storage for the random number generator
      double precision randtemp
      logical oldrand
      common /rangen/ randtemp,oldrand

c     Unit conversion parameters                  
c     convert J to kcal
      double precision J2KCAL
      parameter(J2KCAL=1.0/(4184.0))      
c     convert grams to kilograms
      double precision g2kg
      parameter(g2kg=0.001)
c     convert kilograms to grams
      double precision kg2g
      parameter(kg2g=1000.0)

c     Physical constants      
c     boltzmann constant in J K-1
      double precision BOLTZ
      parameter(BOLTZ=1.3806503e-23)
c     gas constant
      double precision GASR
      parameter(GASR=8.314472)
      
c     Numerical constants
      double precision FIFTH,QUARTER,THIRD,HALF,ZERO,ONE,TWO,THREE,FOUR,FIVE
      double precision THREESIXTY,ONEEIGHTY,ONE_OVER_ONEEIGHTY,ONE_OVER_THREESIXTY
      double precision PI,TWOPI,ONE_OVER_TWOPI,ONE_OVER_PI

      parameter(ZERO=0.D+00)
      parameter(ONE=1.D+00)
      parameter(TWO=2.D+00)
      parameter(THREE=3.D+00)
      parameter(FOUR=4.D+00)
      parameter(FIVE=5.D+00)
      parameter(FIFTH=ONE/FIVE)
      parameter(QUARTER=ONE/FOUR)
      parameter(THIRD=ONE/THREE)
      parameter(HALF=ONE/TWO)
      parameter(THREESIXTY=360.D+00)
      parameter(ONEEIGHTY=180.D+00)
      parameter(ONE_OVER_ONEEIGHTY=ONE/ONEEIGHTY)
      parameter(ONE_OVER_THREESIXTY=ONE/THREESIXTY)
      parameter(PI=3.1415926535897932384626433832795028841971693993751058209749445923)
      parameter(TWOPI=TWO*PI)
      parameter(ONE_OVER_PI=ONE/PI)
      parameter(ONE_OVER_TWOPI=ONE/TWOPI)

c     flags======
c     Coordinates or velocities or both or average-velocities
      integer COORDS,VELS,BOTH,AVGVELS
      parameter(COORDS=1)
      parameter(VELS=2)
      parameter(BOTH=3)
      parameter(AVGVELS=4)

c     the state of an energy/object...
      integer OLD,NEW,DONTCARE
      parameter(OLD=1)
      parameter(NEW=2)
      parameter(DONTCARE=3)

c     a string used for long line printing
      character*300 printstring
      
c     a generally used filehandle      
      integer FILE
      parameter(FILE=42)      
      integer FILE1
      parameter(FILE1=43)
      integer FILE2
      parameter(FILE2=44)
      integer OFILE
      parameter(OFILE=45)
c     a string used to read in files
      character*300 line
      
c     enum of different fileformats
      integer DCD,PDB,CRD
      parameter(DCD=1)
      parameter(PDB=2)
      parameter(CRD=3)      
      
c     file handles for STDOUT and STDERR - the values depend on your
c     operating system (6 and 0 for Linux)
      integer STDOUT,STDERR
      parameter(STDOUT=6)
      parameter(STDERR=0)

c     whether or not we are debugging
      logical debug

c     the maximum number of atoms that may be loaded into the core
      integer MAXATOMS
      parameter(MAXATOMS=150000)

c     an array to hold the (single set) of coordinates that may be held in the core
c     (coordinates in angstroms)
      double precision atomcoords(MAXATOMS,3)
c     an array to hold the (single set) of velocities that may be held in the core
c     (velocities in angstroms per picosecond)
      double precision atomvels(MAXATOMS,3)

c     the number of atoms loaded
      integer natoms

c     the names of each of the atoms
      character*4 atomnam(MAXATOMS)
c     the atom numbers of the atoms
      integer atomnum(MAXATOMS)
c     the residue numbers of the atoms
      integer resnum(MAXATOMS)
c     the residue names of the atoms
      character*4 resnam(MAXATOMS)
c     the masses of each of the atoms (in g mol-1)
      double precision atommass(MAXATOMS)
c     the proton numbers of each atom
      integer nprotons(MAXATOMS)

c     whether or not particular atoms are marked
c     (e.g. for filtering, or for random velocities)
      logical markatom(MAXATOMS)

c     array used to save the current marking of atoms
      logical savedmarks(MAXATOMS)

c     the number of marked atoms
      integer nmarked

c     the index of marked atom 'i'
      integer atomindex(MAXATOMS)

c     the number of degrees of freedom in the system
      integer ndof
c     the number of degrees of freedom of the marked atoms
      integer markedndof
c     the saved number of degrees of freedom
      integer savendof,savemarkedndof

c     whether or not to account for shake (on hydrogens) when 
c     calculating the number of degrees of freedom
      logical useshake

c     a temporary space to load up trajectories (used by DCD reader, stripper and writer)
      real tmpbuffer(MAXATOMS,3)
      real tmpbuffer2(MAXATOMS,3)

c     a double precision temporary buffer used to load up coordinates 
c     (used by the binary namd reader/writer)
      double precision dtmpbuffer(MAXATOMS,3)

      common /CORECOMMON/ atomcoords,atomvels,atommass,dtmpbuffer,tmpbuffer,tmpbuffer2,
     .           atomnum,resnum,natoms,ndof,markedndof,atomindex,savendof,savemarkedndof,
     .           atomnam,nprotons,nmarked,resnam,printstring,line,debug,markatom,useshake,
     .           savedmarks

      save /CORECOMMON/
      
