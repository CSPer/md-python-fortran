
      subroutine generateWeibull(tempk)
      implicit none
      include 'ranvel.inc'
c####################################################
c
c     Generates random velocities on the weibull
c     distribution appropriate for the temperature 
c     'tempk' kelvin.
c
c     (C) Christopher Woods, May 2005
c
c####################################################

      double precision tempk
      integer i,j
      double precision mss,mean,p,velToMomentum,momentumToVel
      double precision stddev,massToStdDev,rangau,meanfac,grnd

      if (debug) then
        write(printstring,*) "Generating random velocities using the weibull distribution ",
     .                       "and a temperature of ",tempK," K"
        call printLine(STDOUT,printstring)
      endif

      call exit(-1)

c     first decide if we are going to use +mean or -mean
      if (grnd().le.0.5) then
        meanfac = 1.0
      else
        meanfac = -1.0
      endif                  

      do i=1,natoms
c       there is no correlation between atoms, or the components of the atoms, so
c       generate each velocity component independently from a gaussian distribution

c       get the mass of the atom in kg mol-1
        mss = atommass(i) * g2kg

c       get the standard deviation for this atom mass at this temperature
        stddev = massToStdDev(mss,tempk)        
                
c       now loop over each component of the velocity and generate the random velocity
c       from the gaussian distribution with mean p and stddev 'stddev'
        do j=1,3
c         get the momentum for this atom along this component
          mean = meanfac * velToMomentum(biasvels(i,j),mss)

c         generate the random momentum                   
          p = rangau(mean,stddev)
                    
c         convert the momentum back a velocity
          atomvels(i,j) = momentumToVel(p,mss)
        enddo
                
      enddo
                            
      return
      end
      
