
      include '../core/core.inc'
      include '../core/splitter.inc'

c     flags...

c     the type of velocity
      integer INITIAL,FINAL,BIAS,OUTPUT
      parameter(INITIAL=1)
      parameter(FINAL=2)
      parameter(BIAS=3)
      parameter(OUTPUT=4)

c     the different random distributions
      integer BINORMAL,WEIBULL,GAU_WEIBULL
      parameter(BINORMAL=1)
      parameter(WEIBULL=2)
      parameter(GAU_WEIBULL=3)

c     the biasing velocities
      double precision biasvels(MAXATOMS,3)
c     the 'initial' velocities
      double precision initialvels(MAXATOMS,3)
c     the 'final' velocities
      double precision finalvels(MAXATOMS,3)

c     the input random number seed
      integer ranseed

      common /RANCOMMON/ biasvels,initialvels,finalvels,ranseed
