
      subroutine hmcout()
      implicit none
      include 'hmc.inc'

      integer frame

      call getFrame(frame)
      write(printstring,*) 'getFrame returns :', frame
      call printLine(STDOUT,printstring)

c     read the output.coor pdb and stores the coords in atomcoords
      call readhmc('output.coor', 'COORDS')
      
c     if the dcd file exists it will append the atomcoords
c     if it does not exists it will just write the atomcoords 
      call writeDCDcoords('hmccoords.dcd', frame)

      return
      end
