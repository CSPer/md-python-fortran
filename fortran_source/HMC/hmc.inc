      include '../core/splitter.inc'
      include '../core/core.inc'

c     boltzmann constant in au.Angstrom2.ps-2.K-1
      double precision BOLTZau
      parameter(BOLTZau=0.8314471D+00)
c     Boltzman constatntxAvagadros number
      double precision BoltzAvo
      parameter(BoltzAvo = 8.3144712D+00)

      double precision accept

c     the array to hold the single set of internal filter velocities
      double precision filtvel(MAXATOMS,3)

c     the array to hold the single set of randomly generated vels
      double precision vel(MAXATOMS,3)

c     The Total energy before and after the MD steps
      double precision Energy(2)

c     velocities before MD steps (oldvel) and after MD steps (newvel)
      double precision newvel(MAXATOMS,3)
      double precision oldvel(MAXATOMS, 3)

      real tmpcoords(10,MAXATOMS,3)     
      common /HMCCOMMON/ filtvel, accept, vel, Energy, newvel, oldvel, tmpcoords
