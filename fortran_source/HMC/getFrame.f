
      subroutine getFrame(frame)
      implicit none
      include 'hmc.inc'

      integer frame, lgth, LineLength, readInt
      character*10 keystr
     
      lgth = lineLength(words(2))
      keystr = words(2)(1:10)
      call lowercase(keystr)

      if (keystr(1:6) .eq. 'frames') then
          frame = readInt(words(2)(8:lgth))
      else
           call parseError()
           write(printstring,*) "Could not interpret'",words(2)(1:lgth),"'"
           call printLine(STDOUT,printstring)
      endif
    
      return
      end
