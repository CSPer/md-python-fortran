      
      subroutine writevel(filename)
      implicit none
      include 'hmc.inc'
c###############################################
c
c     This routine writes out the output
c     velocities or coordinates (typ) to a
c     PDB file (filename)
c
c     If velocities are being written, then
c     they are converted to external units
c     using the factor 'convertVel2External'.
c
c     If markedonly is true, then this only
c     writes the marked atoms.
c
c     (C) Christopher Woods, January 2005
c
c###############################################

      character*(*) filename
      integer lgth,lineLength
      integer i
        
c     try to delete the file - this prevents crashes if the file already exists
      call unlink(filename)      
            
c     try to open the new file
      open(unit=FILE,file=filename,status='new',form='formatted',err=9900)
      
c     write the PDB header
      write(FILE,10) "REMARK CREATED BY HMC"
      write(FILE,10) "REMARK OUTPUT RANDOMLY GENERATED VELOCITIES BY HMC"
10    format(100a)

c     now write out all of the atoms
       
      do i=1,natoms
      call writePDBLine(FILE,atomnum(i),atomnam(i),resnum(i),resnam(i),
     .                       vel(i,1),
     .                       vel(i,2),
     .                       vel(i,3))
         
      enddo
      
      write(FILE,10) "END"             
     
      close(FILE)
      
      return
      
9900  continue
      lgth = lineLength(filename)
      write(printstring,*) "There was a problem opening the PDB output file ", filename(1:lgth)
      call printLine(STDOUT,printstring)
      call printLine(STDOUT,"Check that you have write permissions to the directory!")
      call exit(-1)

      return
      end
