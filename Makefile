####
#### Makefile for the whole of rdfmd

# Build everything
all: fortran_source manual
        
# Build fortran_source
fortran_source: FORCE
	@echo -e "Compiling digifilter..."
	@cd fortran_source; make
        
# Build the manual
manual: FORCE
	@echo -e "Compiling the manual..."
	@cd manual; make

rebuild:
	@make clean
	@make fortran_source
	@make manual

#cleans everything!
clean:
	@echo -e "Making clean..."
	@cd fortran_source; make clean
	@cd manual; make clean

FORCE:
